﻿
using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.HitBoxSystem
{
    public class CollisionChecker
    {
        TimeStamp timeStamp;
        List<BoundingBox> active = new List<BoundingBox>();
        List<BoundingBox> inactive = new List<BoundingBox>();
        bool[,] colliding = new bool[1000,1000];

        public void CheckCollisions()
        {
            for (int i = 0; i < active.Count-1; i++)
            {
                for (int j = i+1; j < active.Count; j++)
                {
                    bool collided = false;
                    if ((active[i].CategoryMask & active[j].CollisionMask) != 0 &&
                        (active[j].CategoryMask & active[i].CollisionMask) != 0)
                    {
                        if (active[i].Max.X < active[j].Min.X
                        || active[i].Min.X > active[j].Max.X
                        || active[i].Min.Y > active[j].Max.Y
                        || active[i].Max.Y < active[j].Min.Y
                        || active[i].Min.Z > active[j].Max.Z
                        || active[i].Max.Z < active[j].Min.Z)
                        {

                        }
                        else
                        {
                            collided = true;
                        }
                        
                        
                    }
                    if (collided != colliding[i, j])
                    {
                        //collision changed, create event
                        int eventType = CollisionEvent.CollisionStart;
                        var ce = new CollisionEvent(eventType,timeStamp.Current,active[i].Id, active[j].Id);
                        
                        colliding[i, j] = collided;

                    }
                    

                }
            }
        }
    }

    public struct CollisionEvent
    {
        public const int CollisionStart = 1;
        public const int CollisionEnd = 2;

        public readonly int eventType;
        public readonly float timeStamp;
        public readonly int box1;
        public readonly int box2;

        public CollisionEvent(int eventType, float timeStamp, int box1, int box2)
        {
            this.eventType = eventType;
            this.timeStamp = timeStamp;
            this.box1 = box1;
            this.box2 = box2;
        }
    }


}
