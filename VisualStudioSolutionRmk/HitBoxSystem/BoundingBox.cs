﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.HitBoxSystem
{
    class BoundingBox
    {
        int Data1 = -1;
        int Data2 = -1;
        public Vector3D Min;
        public Vector3D Max;
        public UInt32 CategoryMask;
        public UInt32 CollisionMask;
        internal readonly int Id;
    }
}
