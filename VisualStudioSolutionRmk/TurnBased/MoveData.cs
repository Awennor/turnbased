﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurnBased
{



    public class MoveData
    {
        string label;
        internal List<Tick> units = new List<Tick>();
        internal List<string> tags = new List<string>();

        public MoveData(string label)
        {
            this.label = label;
        }

        public static int FindByLabel(List<MoveData> moveDatas, string label)
        {
            for (int i = 0; i < moveDatas.Count; i++)
            {
                if (moveDatas[i].label == label) return i;
            }
            return -1;
        }
    }

    public class Tick 
    {
        internal Condition condition;
        internal List<object> thingsToHappen = new List<object>();
    }
    public class Condition
    {
        internal readonly ConditionType type;
        internal readonly Target target;
        internal readonly Vector2D vector;

        public Condition(ConditionType type, Target target, Vector2D vector)
        {
            this.type = type;
            this.target = target;
            this.vector = vector;
        }
    }

    public enum ConditionType
    {
        CanMove
    }

    public class MoveAction
    {
        public readonly Target target;
        public readonly Vector2D distance;

        public MoveAction(Target target, Vector2D amount)
        {
            this.target = target;
            this.distance = amount;
        }
    }

    public enum Target
    {
        Self, ClosestTarget, ClosestTargetX,
        None
    }

}
