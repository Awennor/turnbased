﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurnBased
{
    class ECSBasicCall
    {
        public ECSManager Ecs;
        Action loop;

        public void Setup(List<MoveData> moveDatas, TimeStamp timeStamp)
        {
            
            BattleState bp = new BattleState();
            Ecs = ECSManager.Create();
            {
                Ecs.CreateEntity(out Entity e).AddComponent<GridUnit>();
                var ba = e.AddComponent<BattleActor>();
                var mvs = e.AddComponent<SelectableMoves>();
                
                for (int i = 0; i < moveDatas.Count; i++)
                {
                    if (moveDatas[i].tags.Contains(MoveDataTags.HeroInitial))
                    {
                        mvs.Moves.Add(i);   
                    }
                }
                ba.selectedMoves.Add(0);
                e.AddComponent<RenderInfo>().renderInfo = 5;
            }
            
            bp.phase = BattlePhase.ExecuteMove;
            var battlersInputable = Ecs.QuickAccessor2<SelectableMoves, BattleActor>();
            var moveExecuter = Ecs.QuickProcessorFlex<GridUnit, BattleActor>((a) =>
            {

                {
                    /*
                    #region setup
                    if (a.Length > bp.actingActor)
                    {
                        var movesS = a.Comp2(bp.actingActor).selectedMoves;
                        if (movesS.Count > bp.turn)
                        {
                            int moveId = movesS[bp.turn];
                            var md = moveDatas[moveId];
                            var units = md.units;
                            if (units.Count > bp.moveDataProgress)
                            {

                            }
                            else
                            {
                            }
                        }
                    }

                    #endregion
                    */
                }
                var movesS = a.Comp2(bp.actingActor).selectedMoves;
                int moveId = movesS[bp.turn];
                var md = moveDatas[moveId];
                var units = md.units;
                var currentUnit = units[bp.moveDataProgress];
                var things = currentUnit.thingsToHappen;
                foreach (var t in things)
                {
                    if (t is MoveAction)
                    {
                        MoveAction moveAction = (t as MoveAction);
                        var targetGridUnit = a.Comp1(bp.actingActor); //change to target resolving code
                        targetGridUnit.position += moveAction.distance;
                    }
                }


            });
            var inputReader = Ecs.QuickProcessorFlex<BattleInput, TimeStampSnap>((inputs)=> {
                for (int i = 0; i < inputs.Length; i++)
                {
                    if (inputs.Comp2(i).TimeSnap == timeStamp.CurrentSnap)
                    {
                        if (inputs.Comp1(i).type == BattleInputType.Move)
                        {
                            for (int j = 0; j < battlersInputable.Length; j++)
                            {
                                battlersInputable.Comp2(j).selectedMoves.Add(inputs.Comp1(i).move);
                            }
                        }
                        
                        
                    }
                }
            });
            moveExecuter.Run();
            loop = () =>
            {
                inputReader.Run();
            };
        }

        internal void Update(float delta)
        {
            loop();
        }
    }

    public class BattleState
    {
        public int turn = 0;
        public int turnsPerPhase = 3;

        public int moveDataProgress;


        public int actingActor = 0;
        public BattlePhase phase;
    }

    public enum BattlePhase
    {
        EnemyMoveChoice,
        HandRecharge,
        PickHands,
        ExecuteMove,
    }

    public enum BattleInputType
    {
        Move,
        Done,
        Redo,
    }

    public class GridUnit
    {
        public Vector2D position;
        public Vector2D min;
        public Vector2D max;
    }

    public class BattleInput
    {
        internal readonly float TimeStamp;
        internal readonly BattleInputType type;
        internal readonly int move;

        public BattleInput(BattleInputType type) : this(-1, type)
        {
        }

        public BattleInput(float currentSnap, BattleInput template)
        {
            TimeStamp = currentSnap;
            type = template.type;
            move = template.move;
        }

        public BattleInput(float timeStamp, BattleInputType type, int move = -1)
        {
            TimeStamp = timeStamp;
            this.type = type;
            this.move = move;
        }
    }

    public class SelectableMoves
    {
        public List<int> Moves = new List<int>();

    }

    public class RenderInfo
    {
        public int renderInfo;
    }

    public class BattleActor
    {
        //public int id;
        public List<int> selectedMoves = new List<int>();

    }


}
