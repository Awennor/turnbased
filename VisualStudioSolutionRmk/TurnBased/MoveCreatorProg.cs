﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace TurnBased
{
    public class MoveCreatorProg
    {
        internal List<MoveData> moveDatas = new List<MoveData>();
        internal List<MoveRenderData> moveRenders = new List<MoveRenderData>();

        public MoveCreatorProg()
        {
            Vector2D[] directions = new Vector2D[] {
                
                new Vector2D(0, 1),
                new Vector2D(-1, 0),
                new Vector2D(0, -1),
                new Vector2D(1, 0),
            };
            string[] moveLabels = new string[] {
                "Move Up",
                "Move Left",
                "Move Down",
                "Move Right",
            };
            string[] moveAbrev = new string[] {
                "^",
                "<",
                "v",
                ">",
            };
            for (int i = 0; i < directions.Length; i++)
            {
                NewMoveData(label:moveLabels[i], condition: new Condition(ConditionType.CanMove, Target.Self, directions[i]), action: new MoveAction(Target.Self, directions[i]), tags: TagArray("movement",  MoveDataTags.HeroInitial));
                NewMoveTextRenderData(name:moveLabels[i], abrev:moveAbrev[i]);
            }
            
        }

        private void NewMoveTextRenderData(string name, string abrev)
        {
            moveRenders.Add(new MoveRenderData(name, abrev));
        }

        private void NewMoveData(string label, Condition condition, object action, string[] tags)
        {
            var mv = new MoveData(label);
            Tick tick = new Tick();
            tick.condition = condition;
            tick.thingsToHappen.Add(action);
            mv.units.Add(tick);
            mv.tags.AddRange(tags);
            moveDatas.Add(mv);
        }

        private string[] TagArray(params string[] args)
        {
            return args;
        }
    }

    public class MoveRenderData
    {
        public string Label;
        public string Abrev;

        public MoveRenderData(string label, string abrev)
        {
            this.Label = label;
            this.Abrev = abrev;
        }
    }
}
