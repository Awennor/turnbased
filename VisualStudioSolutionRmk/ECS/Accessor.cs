﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ECS
{

    public class Accessor
    {
        public int Length { get { return SelectedEntities.Count; } }

        internal Type[] TypesProhibited { get; set; }

        internal Type[] TypesNecessary;
        internal List<Entity> SelectedEntities = new List<Entity>();

        public Accessor(params Type[] s)
        {
            TypesNecessary = s;
        }

        internal bool EntityAdded(Entity e)
        {
            return SelectedEntities.Contains(e);
        }

        public Entity Get(int i)
        {
            return SelectedEntities[i];
        }
    }

    public class QuickAccessorOne<T1>
    {

        public QuickAccessorOne()
        {
            accessor = new Accessor(typeof(T1));
        }

        internal Accessor accessor;
        public int Count { get { return accessor.Length; } }
        

        public T1 Comp1(int i)
        {
            return accessor.SelectedEntities[i].GetComponent<T1>();
        }

        public Entity Entity(int i)
        {
            return accessor.SelectedEntities[i];
        }
    }
    public class QuickAccessorTwo<T1, T2> 
    {

        internal Accessor accessor;
        public int Length { get { return accessor.Length; } }

        public T1 Comp1(int i)
        {
            return accessor.SelectedEntities[i].GetComponent<T1>();
        }

        public Entity Entity(int i)
        {
            return accessor.SelectedEntities[i];
        }

        public QuickAccessorTwo()
        {
            accessor = new Accessor(typeof(T1), typeof(T2));
        }

        

        public T2 Comp2(int i)
        {
            return accessor.SelectedEntities[i].GetComponent<T2>();
        }

        
    }
}
