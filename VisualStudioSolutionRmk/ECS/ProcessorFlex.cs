﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ECS
{
    public class ProcessorFlex<T1, T2>
    {
        private Action<QuickAccessorTwo<T1, T2>> p;
        internal QuickAccessorTwo<T1, T2> accessor;

        public ProcessorFlex(Action<QuickAccessorTwo<T1, T2>> p)
        {
            this.p = p;
            accessor = new QuickAccessorTwo<T1, T2>();
        }

        public void Run()
        {
            p(accessor);
        }
    }

    public class ProcessorAccessor
    {
        private Action<Accessor> p;

        Accessor a;

        public ProcessorAccessor(Action<Accessor> p, Accessor a)
        {
            this.p = p;
            this.a = a;
        }

        public void Run()
        {
            p(a);
        }
    }
}
