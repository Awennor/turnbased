﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ECS
{
    public struct Entity : IEquatable<Entity>
    {
        public static Entity None = new Entity(-1,-1);
        readonly internal int ecs;
        readonly internal int id;

        public Entity(int ecs, int id)
        {
            this.ecs = ecs;
            this.id = id;
        }

        public bool Exist()
        {
            return id != -1;
        }

        public bool Equals(Entity other)
        {
            return other.id == this.id && other.ecs == this.ecs;
        }


    }

    public static class ExtensionMethods
    {

        public static void RemoveComponent(this Entity e, object comp)
        {
            ECSManager.GetInstance(e).RemoveComponent(e, comp);
        }

        public static T AddComponent<T>(this Entity e) where T: new()
        {
            return ECSManager.GetInstance(e).AddComponent<T>(e);
        }
        public static void AddComponent(this Entity e, object comp)
        {
            ECSManager.GetInstance(e).AddComponent(e, comp);
        }
        public static T GetComponent<T>(this Entity e)
        {
            return ECSManager.GetInstance(e).GetComponent<T>(e);
        }
    }
}
