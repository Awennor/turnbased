﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ECS
{
    public class ECSManager
    {

        private static ECSManager[] managers = new ECSManager[8000];
        Dictionary<Type, object[]> comps = new Dictionary<Type, object[]>();
        private int ECSId;
        int entityIdMax = -1;
        List<Accessor> accessors = new List<Accessor>();

        Dictionary<Type, Action<Object, Object>> CopyMethods = new Dictionary<Type, Action<object, object>>();

        private ECSManager() { }

        public ProcessorAccessor CreateProcessor(Accessor accessor, Action<Accessor> action)
        {

            return new ProcessorAccessor(action, accessor);
        }

        public void AddCopyMethod(Type type, Action<object, object> copyMetho)
        {
            CopyMethods.Add(type, copyMetho);
        }

        public Accessor CreateAccessor(Type[] necessary, Type[] not)
        {
            var acc = new Accessor(necessary);
            acc.TypesProhibited = not;
            AddAccessor(acc);
            return acc;

        }

        public QuickAccessorTwo<T1, T2> QuickAccessor2<T1, T2>()
        {
            QuickAccessorTwo<T1, T2> accessor = new QuickAccessorTwo<T1, T2>();
            AddAccessor(accessor.accessor);
            return accessor;
        }

        public QuickAccessorOne<T1> QuickAccessor1<T1>()
        {
            QuickAccessorOne<T1> accessor = new QuickAccessorOne<T1>();
            AddAccessor(accessor.accessor);
            return accessor;
        }





        #region static methods


        internal static ECSManager GetInstance(Entity e)
        {
            return managers[e.ecs];
        }

        public static ECSManager Create()
        {

            for (int i = 0; i < managers.Length; i++)
            {
                if (managers[i] == null)
                {
                    managers[i] = new ECSManager();
                    managers[i].ECSId = i;
                    return managers[i];
                }

            }
            return null;
        }


        #endregion

        public Entity CreateEntityWithComponent(object v)
        {
            CreateEntity(out Entity e);
            AddComponent(e, v);
            return e;
        }

        public Entity CreateEntityWithComponent(object v, object v2)
        {
            CreateEntity(out Entity e);
            AddComponent(e, v);
            AddComponent(e, v2);
            return e;
        }

        public void DeleteAllEntitiesThatDoNotHave(Type[] componentTypes)
        {
            var acs = new Accessor();

        }

        public Entity CreateEntity(out Entity e)
        {
            entityIdMax++;
            Entity entity = new Entity(this.ECSId, entityIdMax);
            e = entity;
            return entity;
        }

        public Entity CreateEntity()
        {
            entityIdMax++;
            Entity entity = new Entity(this.ECSId, entityIdMax);
            return entity;
        }


        public ProcessorFlex<T1, T2> QuickProcessorFlex<T1, T2>(Action<QuickAccessorTwo<T1, T2>> p)
        {
            ProcessorFlex<T1, T2> processorFlex = new ProcessorFlex<T1, T2>(p);
            QuickAccessorTwo<T1, T2> accessor = processorFlex.accessor;
            Accessor accessor1 = accessor.accessor;
            AddAccessor(accessor1);
            return processorFlex;
        }

        private void AddAccessor(Accessor accessor1)
        {
            accessors.Add(accessor1);
            for (int i = 0; i <= entityIdMax; i++)
            {
                UpdateAccessorEntity(accessor1, i);
            }

        }

        private void UpdateAccessorEntity(Accessor accessor, int entityId)
        {
            Entity entity = new Entity(ECSId, entityId);
            bool belong = HasAllComps(accessor.TypesNecessary, entityId) && HasNoneOfTheseComps(accessor.TypesProhibited, entityId);
            bool member = accessor.EntityAdded(entity);

            if (belong != member)
            {
                if (belong)
                {
                    accessor.SelectedEntities.Add(entity);
                }
                else
                {
                    //Console.WriteLine("REMOVED ENTITY "+accessor.TypesNecessary[0]);
                    accessor.SelectedEntities.Remove(entity);
                    //Console.WriteLine(accessor.EntityAdded(entity)+" BELONG");
                }
            }

        }

        public void CloneState(ClonedState cs)
        {
            var comps = this.comps;
            Dictionary<Type, object[]> comps2 = cs.comps;
            Copy(comps, comps2);
        }

        public void RestoreState(ClonedState cs)
        {
            var comps = this.comps;
            Dictionary<Type, object[]> comps2 = cs.comps;
            Copy(comps2, comps);

            for (int i = 0; i <=


                entityIdMax; i++)
            {
                foreach (var item in accessors)
                {
                    UpdateAccessorEntity(item, i);
                }
            }

        }

        List<Type> aux = new List<Type>();

        private void Copy(Dictionary<Type, object[]> from, Dictionary<Type, object[]> to)
        {
            aux.Clear();

            foreach (var c in from)
            {
                Type type = c.Key;

                //UNCOMMENT THIS TO CHECK FOR PARAMETERLESS CONSTRUCTORS
                //if (type.GetConstructor(Type.EmptyTypes) == null)
                //{
                //    Console.WriteLine("NO PARAMETERLESS " + type);
                //}


                aux.Add(type);
                if (!to.ContainsKey(type))
                {
                    to.Add(type, new object[300]);
                }
                var toArray = to[type];
                var origin = c.Value;
                Copy(to, type, toArray, origin);
            }
            foreach (var c in to) //checks types in to, so it can be through
            {
                Type type = c.Key;
                if (!aux.Contains(type))
                {
                    aux.Add(type);
                    var toArray = c.Value; //access inverted when compared to previous
                    //var origin = from[type];
                    for (int i = 0; i < toArray.Length; i++)
                    {
                        toArray[i] = null;
                        //Console.WriteLine("Removing entity");
                    }
                }

            }
        }

        private void Copy(Dictionary<Type, object[]> to, Type type, object[] toArray, object[] origin)
        {
            Action<Object, Object> copyMethod = null;
            CopyMethods.TryGetValue(type, out copyMethod);

            for (int i = 0; i < origin.Length; i++)
            {
                if (origin[i] == null)
                {
                    if (toArray[i] != null)
                    {
                        //Console.WriteLine("Removing entity");
                        toArray[i] = null;
                    }

                }
                else
                {
                    //Console.WriteLine(type);
                    if (toArray[i] == null)
                        toArray[i] = Activator.CreateInstance(type);
                    if (copyMethod != null)
                        copyMethod(origin[i], toArray[i]);

                    //DeepCloneHelper.DeepCopyPartial(origin[i], toArray[i]);
                }
            }
        }

        internal T AddComponent<T>(Entity e) where T : new()
        {
            T t = new T();
            AddComponent(e, t);

            return t;
        }

        public void AddComponent(Entity e, object t)
        {
            Type type = t.GetType();
            if (!comps.ContainsKey(type))
            {
                comps.Add(type, new object[300]);
            }
            comps[type][e.id] = t;
            foreach (var item in accessors)
            {
                UpdateAccessorEntity(item, e.id);

            }
        }

        public void RemoveComponent(Entity e, object t)
        {
            Type type = t.GetType();
            if (!comps.ContainsKey(type))
            {
                comps.Add(type, new object[300]);
            }
            comps[type][e.id] = null;
            foreach (var item in accessors)
            {
                UpdateAccessorEntity(item, e.id);

            }
        }

        private bool HasAllComponents(Entity e, Type[] typesNecessary)
        {
            int id = e.id;
            return HasAllComps(typesNecessary, id);
        }

        private bool HasAllComps(Type[] typesNecessary, int id)
        {
            foreach (var type in typesNecessary)
            {
                if (!comps.ContainsKey(type))
                {
                    return false;
                }

                if (comps[type][id] == null)
                    return false;
            }
            return true;
        }

        private bool HasNoneOfTheseComps(Type[] typesProhibited, int id)
        {
            if (typesProhibited == null) return true;
            foreach (var type in typesProhibited)
            {
                if (comps.ContainsKey(type))
                {
                    if (comps[type][id] != null)
                        return false;
                }
            }
            return true;
        }

        internal T GetComponent<T>(Entity e)
        {
            Type type = typeof(T);
            if (!comps.ContainsKey(type))
            {
                //comps.Add(type, new object[300]);
                return default(T);
            }
            return (T)comps[type][e.id];
        }
    }
}
