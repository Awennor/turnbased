﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;
using TurnBased;

namespace Pidroh.TurnRenderText
{
    public class Game : ITextGame
    {
        private ECSManager ecs;
        private BattleRenderSetup battleRender;
        private TextScreenN tsn;
        TimeStamp timeStamp = new TimeStamp();
        private ECSBasicCall eCSBasicCall;

        public TextScreenHolder ScreenHolder { get; set; } = new TextScreenHolder();

        public Palette GetPalette()
        {
            return DefaultPalettes.C4KiroKaze;
        }

        public void Init(int w, int h)
        {
            
            TextWorld tw = new TextWorld();
            tw.Init(w, h);
            MoveCreatorProg moveCreatorProg = new MoveCreatorProg();
            var moveDatas = moveCreatorProg.moveDatas;
            eCSBasicCall = new ECSBasicCall();
            eCSBasicCall.Setup(moveDatas, timeStamp);
            ecs = eCSBasicCall.Ecs;
            battleRender = new BattleRenderSetup(ecs, tw, moveCreatorProg.moveRenders, moveDatas, timeStamp);
            tsn = new TextScreenN(tw);
            ScreenHolder.SetAll(tsn);
            tw.DrawChildren();
        }

        public void Update(float delta)
        {
            var unicode = tsn.InputUnicode;
            if (unicode > 0)
            {
                ecs.CreateEntityWithComponent(timeStamp.GetSnap(), new UnicodeInput(unicode));
            }
            battleRender.Update(delta);
            eCSBasicCall.Update(delta);
            timeStamp.Advance(delta);
        }
    }
}
