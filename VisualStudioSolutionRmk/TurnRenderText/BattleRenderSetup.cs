﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;
using TurnBased;

namespace Pidroh.TurnRenderText
{
    public class BattleRenderSetup
    {
        ECSManager ecs;
        Dictionary<BattleInput, int[]> moveKeys = new Dictionary<BattleInput, int[]>();
        List<InputMetaInfo> inputMetaInfos = new List<InputMetaInfo>();

        Action loop;

        public BattleRenderSetup(ECSManager ecs, TextWorld tw, List<MoveRenderData> renders, List<MoveData> mDatas, TimeStamp timeStamp)
        {
            this.ecs = ecs;
            MoveKeys(mDatas, renders);

            var inputRaw = ecs.QuickAccessor2<UnicodeInput, TimeStampSnap>();
            var renderCreator = ecs.CreateProcessor(ecs.CreateAccessor(necessary: new Type[] { typeof(RenderInfo) }, not: new Type[] { typeof(BattleRenderActorText) }), (a) =>
                 {
                     for (int i = 0; i < a.Length; i++)
                     {
                         //Console.Write("SADASs");
                         var brat = a.Get(i).AddComponent<BattleRenderActorText>();
                         brat.textEntity = tw.GetFreeEntity(3, 3);
                         brat.textEntity.Origin.DrawChar('c', 1, 1, 3, 0);
                     }
                 });
            var renderUpdate = ecs.QuickProcessorFlex<BattleRenderActorText, RenderInfo>((a) =>
            {
                for (int i = 0; i < a.Length; i++)
                {
                    a.Comp1(i).textEntity.Origin.DrawOneDigit(a.Comp2(i).renderInfo, 0, 0, 3);
                    //Console.Write("SADAS");
                }
            });
            var renderBattleInput = ecs.QuickProcessorFlex<SelectableMoves, GridUnit>((a) =>
            {
                for (int i = 0; i < a.Length; i++)
                {
                    var ms = a.Comp1(i).Moves;
                    for (int j = 0; j < ms.Count; j++)
                    {
                        var move = ms[j];
                        tw.mainBoard.Draw("[ ]", 0, j, 2);
                        foreach (var item in moveKeys)
                        {
                            if (item.Key.move == move)
                            {
                                tw.mainBoard.DrawUnicodeLabel(item.Value[0], 1, j, 2);
                                break;
                            }
                        }
                        
                        tw.mainBoard.Draw(renders[move].Abrev, 4, j, 2);
                        tw.mainBoard.Draw(renders[move].Label, 7, j, 2);
                    }
                    for (int j = 0; j < inputMetaInfos.Count; j++)
                    {
                        tw.mainBoard.SetCursorAt(0, ms.Count+j+1);
                        tw.mainBoard.Draw_Cursor('[', 2);
                        tw.mainBoard.Draw_Cursor_UnicodeLabel(inputMetaInfos[j].keys[0], 2);
                        tw.mainBoard.Draw_Cursor(']', 2);
                        tw.mainBoard.AdvanceCursor();
                        tw.mainBoard.Draw_Cursor(inputMetaInfos[j].label, 2);
                    }

                }
            });
            var renderSelectedMoves = ecs.QuickProcessorFlex<BattleActor, RenderInfo>((a) =>
            {
                int xOff = 0;
                for (int i = 0; i < a.Length; i++)
                {
                    xOff = i * 3;
                    var ms = a.Comp1(i).selectedMoves;
                    for (int j = 0; j < ms.Count; j++)
                    {
                        var move = ms[j];
                        tw.mainBoard.Draw(renders[move].Abrev, xOff, j+2, 2);

                    }

                }
            });
            Action inputRawProcessing = () =>
            {
                
                for (int i = 0; i < inputRaw.Length; i++)
                {
                    
                    if (inputRaw.Comp2(i).TimeSnap == timeStamp.CurrentSnap)
                    {
                        
                        var e = inputRaw.Entity(i);
                        foreach (var input in moveKeys)
                        {
                            
                            foreach (var inputKey in input.Value)
                            {
                                //Console.WriteLine("INPUT!!!s "+inputKey);
                                //Console.WriteLine("INPUT!!!z " + inputRaw.Comp1(i).input);
                                if (inputKey == inputRaw.Comp1(i).input)
                                {
                                    e.AddComponent(new BattleInput(timeStamp.CurrentSnap, input.Key));
                                    //Console.Write("INPUT!!!");
                                }
                            }
                        }
                        
                    }
                    

                }
            };
            
            
            loop = () =>
            {
                renderCreator.Run();
                renderUpdate.Run();
                renderBattleInput.Run();
                renderSelectedMoves.Run();
                inputRawProcessing();
            };
        }

        internal void Update(float delta)
        {
            loop();
        }

        public void MoveKeys(List<MoveData> moveDatas, List<MoveRenderData> moveRenders)
        {
            string moveLabel = "Move Up";
            moveKeys.Add(GetBattleInputMove(moveDatas, moveLabel), new int[] { 'w', 38 });
            moveKeys.Add(GetBattleInputMove(moveDatas, "Move Left"), new int[] { 'a', 37 });
            moveKeys.Add(GetBattleInputMove(moveDatas, "Move Down"), new int[] { 's', 40 });
            moveKeys.Add(GetBattleInputMove(moveDatas, "Move Right"), new int[] { 'd', 39 });
            

            inputMetaInfos.Add(new InputMetaInfo(new BattleInput(BattleInputType.Done), new int[] {32 }, "Done"));


        }

        private static BattleInput GetBattleInputMove(List<MoveData> moveDatas, string moveLabel)
        {
            return new BattleInput(-1, BattleInputType.Move, MoveData.FindByLabel(moveDatas, moveLabel));
        }
    }

    class InputMetaInfo
    {
        public BattleInput battleInputTemplate;
        public int[] keys;
        public string label;

        public InputMetaInfo(BattleInput battleInputTemplate, int[] keys, string label)
        {
            this.battleInputTemplate = battleInputTemplate;
            this.keys = keys;
            this.label = label;
        }
    }

    public class UnicodeInput
    {
        public readonly int input;

        public UnicodeInput(int input)
        {
            this.input = input;
        }
    }

    



    public class BattleRenderActorText
    {
        internal TextEntity textEntity;
    }
}
