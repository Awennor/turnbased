﻿using Pidroh.ReusableSunshine;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleBuild
{
    class Program
    {
        static void Main(string[] args)
        {
            TestGame testGame = new TestGame();
            new SunshineGameRun(testGame);
        }
    }
}
