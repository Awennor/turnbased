﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pidroh.BaseUtils;
using Pidroh.ReusableSunshine;
using Pidroh.TextRendering;
using Pidroh.TurnRenderText;
using SunshineConsole;
using TurnBased;

namespace BuildConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //new ECSBasicCall().Setup();
            //TestGame game = new TestGame();
            Game game = new Game();
            game.Init(20, 20);
            //game.Init(20,20);
            new SunshineGameRun(game);

        }
    }
}
