﻿using Bridge;
using Bridge.Html5;
//using ECS;
//using Newtonsoft.Json;
using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TurnBased.TextRendering;
//using Pidroh.ConsoleApp.Turnbased;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace BridgeBuild
{
    public class App
    {
        private static int buffer;
        private static bool bufferOn;
        //private static HTMLPreElement text;
        private static GameMain gr;
        private static TextBoard TextBoard;
        //private static StringBuilder sb;
        private static string[] colors;
        private static int bufferUnicode = -1;
        private static TextBoard aux;
        static DateTime last = DateTime.Now;
        private static bool CanDraw;

        private static void SetupGame(out GameMain gr, out TextBoard TextBoard)
        {

            //Console.WriteLine("Setup");
            Random rnd = new Random();
            RandomSupplier.Generate = () =>
            {
                float v = (float)rnd.NextDouble();
                //Console.WriteLine(v);
                return v;
            };

            gr = new GameMain();
            TextBoard = gr.GetBoard();
            aux = new TextBoard(300, 300);



        }

        public class Bla
        {
            public int i = 3;
            public Vector pos = new Vector();
        }
        public struct Vector
        {
            public float x;
            public float y;
        }

        static List<int> keysDown = new List<int>();

        public static void Main()
        {
            Debugger.ActiveStatic = false;
            //DeepCloneHelper.debug.Active(false);
            //new ReflectionTest();
            TestEntitySystem();
            //Console.WriteLine("Game Start");
            SetupGame(out gr, out TextBoard);
            colors = new string[30];
            for (int i = 0; i < ColorStuff.colors.Length; i++)
            {

                colors[i] = ColorStuff.colors[i];
            }



            var style = new HTMLStyleElement();
            style.InnerHTML = "html,body {font-family: Courier; background-color:#1f2526; height: 100%; color:#888;}" + "\n #canvas-container {width: 100%; height: 100%; text-align:center; vertical-align: middle; } ";
            Document.Head.AppendChild(style);
            buffer = 9;
            bufferOn = false;

            Document.OnKeyUp += (KeyboardEvent a) =>
            {
                int code = a.KeyCode;
                if (code == 0) code = a.CharCode;
                int unicode = code;
                if (unicode >= 65 && unicode <= 90)
                {
                    unicode = unicode - 65 + 97;
                }
                while (keysDown.Contains(unicode))
                {
                    //Console.WriteLine(unicode);
                    keysDown.Remove(unicode);
                }



            };

            Document.OnKeyDown += (KeyboardEvent a) =>
            {
                if (a.KeyCode >= 37 && a.KeyCode <= 40)
                {
                    Handle(a);
                    a.PreventDefault();
                }
                if (a.KeyCode == Unicode.Escape)
                {
                    Handle(a);
                    a.PreventDefault();
                }
                if (a.KeyCode == Unicode.Enter)
                {
                    Handle(a);
                    a.PreventDefault();
                }
                //Console.WriteLine("KD" + a.KeyCode);
                //Console.WriteLine("KD"+a.CharCode);

                //Console.Write(unicode);
                //buffer = a.CharCode;

            };


            Document.OnKeyPress += (KeyboardEvent a) =>
            {
                if (a.KeyCode >= 37 && a.KeyCode <= 40) return;
                //Console.WriteLine(a.KeyCode);
                Handle(a);
                a.PreventDefault();

                //Console.Write(unicode);
                //buffer = a.CharCode;

            };

            UpdateGame();

            // After building (Ctrl + Shift + B) this project, 
            // browse to the /bin/Debug or /bin/Release folder.

            // A new bridge/ folder has been created and
            // contains your projects JavaScript files. 

            // Open the bridge/index.html file in a browser by
            // Right-Click > Open With..., then choose a
            // web browser from the list

            // This application will then run in a browser.
        }

        private static void Handle(KeyboardEvent a)
        {
            int code = a.KeyCode;
            if (code == 0) code = a.CharCode;
            int unicode = code;
            bufferUnicode = unicode;
            keysDown.Add(unicode);

        }

        private static void TestEntitySystem()
        {

        }

        private static void UpdateGame()
        {
            if (CanDraw)
            {
                DateTime now = DateTime.Now;
                var secs = (now - last).TotalSeconds;
                if (secs > 0.08)
                {
                    //Console.WriteLine(secs);
                    secs = 0.08;
                }

                TextBoard = gr.GetBoard();
                last = now;
                gr.keyDownUnicode = bufferUnicode;
                bufferUnicode = -1;
                var mouseX = Script.Call<int>("getMouseX");
                var mouseY = Script.Call<int>("getMouseY");
                Point2D point2D = new Point2D(mouseX, mouseY);
                if (gr.Mouse.pos != point2D)
                {
                    //Console.WriteLine(mouseX+"sx");
                    //Console.WriteLine(mouseY + "sy");
                    gr.Mouse.pos = point2D;
                }
                gr.Keyboard.downUnicodes.Clear();
                gr.Keyboard.downUnicodes.AddRange(keysDown);

                gr.Draw((float)secs);




                //;;Script.Call("clear");
                for (int j = 0; j < TextBoard.Height; j++)
                {
                    for (int i = 0; i < TextBoard.Width; i++)
                    {
                        if (!aux.SameAs(TextBoard, x: i, y: j))
                        {
                            int tcI = TextBoard.TextColor[i, j];
                            string color = colors[0];
                            if (tcI < 0) { }
                            else
                                if (tcI >= colors.Length) { }
                            else
                            {
                                color = colors[tcI];
                            }



                            string backColor = colors[TextBoard.BackColor[i, j]];
                            char @char = TextBoard.CharAt(i, j);
                            Script.Call("draw", i, j, color, backColor, "" + @char);
                            aux.Copy(TextBoard, x: i, y: j);
                        }
                        else
                        {
                            //Script.Call("draw", i, j, colors[TextBoard.TextColor[i, j]], colors[TextBoard.BackColor[i, j]], "x");
                        }


                    }
                }
            }
            else
            {
                CanDraw = Script.Call<bool>("isReadyToDraw");
            }


            Window.SetTimeout(UpdateGame, 15);
        }
    }
}