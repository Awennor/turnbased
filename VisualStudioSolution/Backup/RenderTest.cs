﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using static Pidroh.ConsoleApp.Turnbased.TurnBaseTryValues;

namespace TurnBasedLogicTests
{
    [TestClass]
    public class RenderTest
    {
        [TestMethod]
        public void TestRendering()
        {
            var tb = new TurnBaseTryValues(0);
            

            const int X = 4;
            const int Y = 1;
            for (int i = 0; i < 2; i++)
            {
                var enemy1 = new BattleEntity();
                enemy1.life.Val = 3;
                enemy1.pos.Set(X, Y); //same position
                enemy1.minPos.Set(0, 0);
                enemy1.maxPos.Set(10, 10);
                enemy1.graphic = 1+i;
                enemy1.Type = EntityType.enemy;
                tb.entities.Add(enemy1);
            }
            var br = new BattleRender(tb);
            BattleEntity enemyA = tb.entities[1];
            var screePos = br.BattleEntityToScreenPosition(enemyA.PositionV2D);
            var board = br.GetBoard();
            br.DrawGraphics(0.1f);
            var drawnChar = board.CharAt((int)screePos.X, (int)screePos.Y);
            var enemyChar = br.GetChar(enemyA);
            
            enemyA.life.Val  = 0;
            br.DrawGraphics(0.1f);
            var drawnChar2 = board.CharAt((int)screePos.X, (int)screePos.Y);
            var enemyChar2 = br.GetChar(tb.entities[0]);
            var v = enemyChar2;
            //Console.WriteLine(c);


            Assert.IsTrue(enemyChar[0] != enemyChar2[0]);
            Assert.IsTrue(drawnChar == enemyChar[0]);
            Assert.IsTrue(drawnChar2 != enemyChar[0]);
            Assert.IsTrue(drawnChar2 != TextBoard.INVISIBLECHAR);
            Assert.IsTrue(drawnChar2 == v[0]);

        }

        [TestMethod]
        public void TableTextInvisibleCharWriteTest()
        {
            TextBoard tb = new TextBoard(1, 1);
            TextBoard tb2 = new TextBoard(1, 1);
            tb.DrawChar('a', 0,0,5);
            tb2.DrawChar(TextBoard.INVISIBLECHAR, 0, 0, 5);
            var c1 = tb.CharAt(0,0);
            tb.Insert(tb2);
            var c2 = tb.CharAt(0, 0);

            Assert.IsTrue(c1 == c2);


        }
    }
}
