﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pidroh.ConsoleApp.Turnbased;
using static Pidroh.ConsoleApp.Turnbased.TurnBaseTryValues;

namespace TurnBasedLogicTests
{
    [TestClass]
    public class TurnBasedBasicTests
    {
        [TestMethod]
        public void Movement_Occupied()
        {
            var tb = new TurnBaseTryValues(0);
            const int X = 4;
            const int Y = 1;
            for (int i = 0; i < 2; i++)
            {
                var enemy1 = new BattleEntity();
                enemy1.life.Val = 3;


                enemy1.pos.Set(X, Y + i);
                enemy1.minPos.Set(0, 0);
                enemy1.maxPos.Set(10, 10);
                tb.entities.Add(enemy1);
            }
            var e1 = tb.entities[0];
            for (int i = 0; i < e1.moves.Length; i++)
            {
                e1.moves[i] = new Value();
            }
            e1.moves[0].valAsEnum = MoveType.MoveRight;
            e1.moves[1].valAsEnum = MoveType.MoveLeft;
            e1.moves[2].valAsEnum = MoveType.MoveUp;

            //tb.Tick();
            Assert.IsTrue(e1.pos.x == X);
            Assert.IsTrue(e1.pos.y == Y);
            tb.ExecuteMove(e1, 0);
            //Assert.IsTrue(e1.pos.x == X-1);
            //Assert.IsTrue(e1.pos.x == X);
            Assert.IsTrue(e1.pos.x == X + 1);
            Assert.IsTrue(e1.pos.y == Y);
            tb.ExecuteMove(e1, 1);
            Assert.IsTrue(e1.pos.x == X);
            Assert.IsTrue(e1.pos.y == Y);
            tb.ExecuteMove(e1, 2);
            Assert.IsTrue(e1.pos.x == X);
            Assert.IsTrue(e1.pos.y == Y);
        }

        [TestMethod]
        public void Movement_Dead()
        {
            var tb = new TurnBaseTryValues(0);
            const int X = 4;
            const int Y = 1;
            for (int i = 0; i < 2; i++)
            {
                var enemy1 = new BattleEntity();
                enemy1.life.Val = 1 - i; //dead enemy


                enemy1.pos.Set(X + i, Y);
                enemy1.minPos.Set(0, 0);
                enemy1.maxPos.Set(10, 10);
                tb.entities.Add(enemy1);
            }
            var e1 = tb.entities[0];
            for (int i = 0; i < e1.moves.Length; i++)
            {
                e1.moves[i] = new Value();
            }
            e1.moves[0].valAsEnum = MoveType.MoveRight;

            //tb.Tick();
            Assert.IsTrue(e1.pos.x == X);
            Assert.IsTrue(e1.pos.y == Y);
            tb.ExecuteMove(e1, 0);
            Assert.IsTrue(e1.pos.x == X + 1);
            Assert.IsTrue(e1.pos.y == Y);
        }
    }
}
