﻿using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class ColorStuff
    {
        private static readonly string GoodMain;
        public static string neutralDark = "#19013b";
        public static string neutralStrong = "#2c3e43";
        private static readonly string GoodSub;
        private static readonly string EvilMain;
        public static string[] colors = new string[25];

        static ColorStuff()
        {
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = "#400020";
            }
            //colors[Colors.Hero] = "#009c8d";
            //const string heroSub = "#005f91";
            //colors[Colors.HeroTurn] = heroSub;
            //colors[Colors.Enemy] = "#ff0353";
            //colors[Colors.GridHero] = heroSub;
            //colors[BattleRender.Colors.GridEnemy] = "#8e0060";
            //colors[BattleRender.Colors.EnemyTurn] = "#8e0060";
            //colors[BattleRender.Colors.Board] = "#1e486e";
            //colors[BattleRender.Colors.inputKey] = "#688690";
            //colors[BattleRender.Colors.WindowLabel] = "#1e486e";
            //colors[BattleRender.Colors.FireAura] = "#793100";
            //colors[BattleRender.Colors.IceAura] = "#005590";
            //colors[BattleRender.Colors.ThunderAura] = "#00583d";

            colors[Colors.Hero] = "#8ad896";
            const string heroSub = "#4c6d50";
            colors[Colors.HeroTurn] = heroSub;
            colors[Colors.Enemy] = "#ff7694";
            colors[Colors.GridHero] = heroSub;
            const string enemysub = "#a7464f";
            colors[Colors.GridEnemy] = enemysub;
            colors[Colors.EnemyTurn] = enemysub;
            colors[BattleRender.Colors.Board] = "#1e486e";
            colors[BattleRender.Colors.inputKey] = "#688690";
            colors[BattleRender.Colors.WindowLabel] = "#1e486e";
            colors[BattleRender.Colors.FireAura] = "#793100";
            colors[BattleRender.Colors.IceAura] = "#005590";
            colors[BattleRender.Colors.ThunderAura] = "#00583d";
            colors[BattleRender.Colors.FireShot] = "#f82b36";
            colors[BattleRender.Colors.IceShot] = "#007eff";
            colors[BattleRender.Colors.ThunderShot] = "#a37c00";
            colors[BattleRender.Colors.BackgroundInput] = "#080808";


            colors[BattleRender.Colors.inputKey] = "#9E8664";
            colors[BattleRender.Colors.InputDescription] = "#808080";
            colors[BattleRender.Colors.BackBattle] = "#000000";
            colors[BattleRender.Colors.BackgroundInput] = "#1A1A1A";
            colors[Colors.HeroTurn] = "#00B2B2";
            colors[Colors.EnemyTurn] = "#FF0040";
            colors[Colors.GridHero] = "#00468C";
            colors[Colors.GridEnemy] = "#8C0023";
            colors[Colors.Hero] = "#66FFFF";
            colors[Colors.Enemy] = "#D90036";
            
            colors[Colors.Hero] = "#BFBFBF";
            colors[Colors.Enemy] = colors[Colors.Hero];
            colors[Colors.HeroTurn] = colors[Colors.Hero];
            colors[Colors.EnemyTurn] = colors[Colors.Hero];
            colors[BattleRender.Colors.WindowLabel] = "#666666";
            colors[BattleRender.Colors.BackCommand] = "#333333";

            colors[BattleRender.Colors.FireAura] = "#FF8C66";
            colors[BattleRender.Colors.IceAura] = "#66FFFF";
            colors[BattleRender.Colors.ThunderAura] = "#FFFF66";

            colors[BattleRender.Colors.BackStripe] = "#1A140D";
        }


    }
}
