﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.ECS;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    class AttackPreview
    {
        ECSManager ecs;
        private readonly BattleRender render;
        private QuickAccessorOne<MoveData> moveDatas;
        List<TextEntity> entities = new List<TextEntity>();
        private int currentMoveId;

        public AttackPreview(ECSManager ecs, BattleRender render)
        {
            this.ecs = ecs;
            this.render = render;
            this.render.attackPreview = this;
            moveDatas = ecs.QuickAccessor1<MoveData>();
            
        }

        public void ShowPreview(int user, int moveId)
        {
            if (moveId == currentMoveId) return;
            End();
            currentMoveId = moveId;
            //Console.WriteLine("START");

            //Console.WriteLine("move "+moveId);
            var moveData = moveDatas.Comp1(moveId);
            Vector2D pos = render.EntityScreenPosition(user);
            Rect gridRect = render.GetGridRect();

            foreach (var item in moveData.units)
            {
                var items = item.thingsToHappen;
                foreach (var thing in items)
                {
                    if (thing is DealDamageAction)
                    {
                        var da = thing as DealDamageAction;
                        var target = da.target;
                        switch (target)
                        {
                            case Target.None:
                                break;
                            case Target.Self:
                                break;
                            case Target.ClosestTarget:
                                break;
                            case Target.ClosestTargetX:
                                var freeEntity = render.textWorld.GetFreeEntity(gridRect.X+gridRect.Width - pos.XInt,1);
                                entities.Add(freeEntity);
                                freeEntity.SetPosition(pos);
                                freeEntity.Origin.SetAll(TextBoard.INVISIBLECHAR, TextBoard.INVISIBLECOLOR, 1);
                                break;
                            case Target.Area:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        internal void End()
        {
            
            if (currentMoveId >= 0) {
                //Console.WriteLine("END");
                currentMoveId = -1;
                render.textWorld.Free(entities);
            }
        }
    }
}
