﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.TurnBased.TextRendering
{
    public class NavigationKeyboard
    {
        List<Point2D> points = new List<Point2D>();
        public int currentPoint = 0;
        private TextWorld textWorld;
        private TextEntity cursor;
        public int chosenOption = -1;
        public bool showEscape = true;
        public bool wantToEscape = false;
        

        public NavigationKeyboard(TextWorld textWorld)
        {
            this.textWorld = textWorld;
            cursor = textWorld.GetFreeEntity(1, 1);
            cursor.Origin.DrawChar('@', 0, 0, Colors.Hero);
            //UpdatePoint();
        }

        //int inputUnicode;

        public void CheckInput(int inputUnicode)
        {

            //Console.WriteLine(inputUnicode);
            if (inputUnicode == Unicode.Space)
            {
                chosenOption = currentPoint;
            }
            if (inputUnicode == 's' || inputUnicode == 'd')
            {
                Move(1);
            }
            if (inputUnicode == 'w' || inputUnicode == 'a')
            {
                Move(-1);
            }
            if(inputUnicode == Unicode.Escape && showEscape)
            {
                wantToEscape = true;
            }
        }

        private void Move(int v)
        {
            currentPoint += v;
            if (currentPoint >= points.Count)
            {
                currentPoint = 0;
            }
            if (currentPoint < 0)
            {
                currentPoint = points.Count - 1;
            }
            UpdatePoint();
        }

        private void UpdatePoint()
        {
            if (currentPoint < points.Count)
                cursor.SetPosition(points[currentPoint]);
        }

        public void DrawControls()
        {
            int y = 30;
            UIReusable.DrawControlsLabel(textWorld, y);
            {
                var forceInputLabel = UIReusable.WASD;
                var forceCommandLabel = UIReusable.arrows;
                
                BattleRender.DrawInput(7, y+2, forceInputLabel, forceCommandLabel, textWorld.mainBoard);
                GameMain.DrawInput(7, y+4, Unicode.Space, "SELECT", textWorld.mainBoard);
                if(showEscape)
                    GameMain.DrawInput(7, y+6, Unicode.Escape, "RETURN", textWorld.mainBoard);
            }
            

        }

        internal void Clear()
        {
            points.Clear();
        }

        internal void ResetChoice()
        {
            //currentPoint = 0;
            chosenOption = -1;
            wantToEscape = false;
        }

        internal void Add(int x, int y)
        {
            points.Add(new Point2D(x, y));
            UpdatePoint();
        }

        internal void ShowCursor(bool v)
        {
            if (v)
            {
                cursor.Origin.DrawChar('@', 0, 0, Colors.Hero);
            }
            else
            {
                cursor.Origin.Reset();

            }
        }

        internal void ResetCursor()
        {
            Move(-currentPoint);
        }
    }
}
