﻿using Pidroh.ECS;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using Pidroh.TurnBased.TextRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class StageGroupSelectionScreen : ITextScreen_
    {
        private QuickAccessorOne<StageDataGroup> stageGroups;
        private TextWorld textWorld;
        public MouseIO Mouse { set; get; }
        public KeyboardIO Keyboard { set; get; }

        public int wantedGroup = -1;
        public Entity wantedGroupEntity = Entity.None;

        public StageGroupSelectionScreen()
        {
            
            textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            navKey = new NavigationKeyboard(textWorld);
            navKey.showEscape = false;
            textWorld.mainBoard.SetAll(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackgroundInput);

            textWorld.mainBoard.DrawOnCenterHorizontal("Stage Selection", BattleRender.Colors.Hero, 0, 1);

            textWorld.mainBoard.DrawOnCenterHorizontal("Controls", Colors.WindowLabel, 0, 6);
            

        }

        public void SetECS(ECSManager ecs)
        {
            stageGroups = ecs.QuickAccessor1<StageDataGroup>();

            
        }

        public int wannaLeave;
        private NavigationKeyboard navKey;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }

        public void Enter()
        {
            wannaLeave = 0;
        }

        public void Draw(float f)
        {
            navKey.Clear();
            textWorld.mainBoard.Reset();
            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            textWorld.DrawChildren();
            //textWorld.mainBoard.DrawOnCenterHorizontal("Controls", Colors.WindowLabel, 0, 6);
            textWorld.mainBoard.DrawOnCenterHorizontal("Stage Selection", BattleRender.Colors.Hero, 0, 1);

            for (int i = 0; i < stageGroups.Count; i++)
            {
                int x = 5;
                int y = 5 + i * 2;
                textWorld.mainBoard.Draw("World " + (i + 1), x, y, Colors.WindowLabel);
                navKey.Add(x - 2, y);
            }

            if (keyDownUnicode >= 0)
            {
                navKey.CheckInput(keyDownUnicode);
            }

            wantedGroup = navKey.chosenOption;
            if(wantedGroup >=0)
                wantedGroupEntity = stageGroups.Entity(wantedGroup);
            navKey.DrawControls();

            
        }

        internal void ResetChoice()
        {
            wantedGroup = -1;
            navKey.ResetChoice();
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }


    }
}
