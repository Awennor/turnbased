﻿using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.TurnBased.TextRendering
{
    public class ModeSelectionScreen : ITextScreen_
    {
        private TextWorld textWorld;
        public MouseIO Mouse { set; get; }
        int selection;
        public BattleResult battleResult;
        public KeyboardIO Keyboard { set; get; }
        public ModeSelectionScreen()
        {
            textWorld = new TextWorld();
            textWorld.Init(70, 25);
        }
        public int wannaLeave;
        public int mode;
        public bool timeAttack = false;
        public int screenStage;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }

        public void Enter()
        {
            wannaLeave = 0;
        }

        public void Draw(float f)
        {
            textWorld.mainBoard.Reset();
            InputKey ik = (InputKey) Input;
            mode = -1;
            textWorld.mainBoard.Draw("ProgBattle Prototype v0.3", 1, 1, Colors.GridHero);
            textWorld.mainBoard.Draw("A game by Pidroh", 1, 2, Colors.GridHero);
            if (screenStage == 0)
            {
                switch (ik)
                {

                    case InputKey.LEFT:
                        screenStage = 1;
                        timeAttack = false;
                        break;
                    case InputKey.RIGHT:
                        screenStage = 1;
                        timeAttack = true;
                        break;
                    case InputKey.DOWN:
                        timeAttack = true;
                        mode = 0;
                        break;
                    case InputKey.UP:
                        mode = 0;
                        timeAttack = false;
                        break;
                    default:
                        break;
                }
                textWorld.mainBoard.DrawOnCenter("[w] Vanilla", Colors.GridHero, yOff: 4, alignString: false);
                textWorld.mainBoard.DrawOnCenter("[a] Elemental", Colors.GridHero, yOff: 5, alignString: false);
                textWorld.mainBoard.DrawOnCenter("[s] Vanilla Time Attack", Colors.GridHero, yOff: 6, alignString: false);
                textWorld.mainBoard.DrawOnCenter("[d] Elemental Time Attack", Colors.GridHero, yOff: 7, alignString: false);
            }
            if (screenStage == 1)
            {
                if (ik == InputKey.UP)
                {
                    mode = 1;
                    
                }
                if (ik == InputKey.DOWN)
                {
                    screenStage = 0;
                }
                textWorld.mainBoard.DrawOnCenter("Elemental Mode", Colors.GridHero, yOff: -5);
                textWorld.mainBoard.DrawOnCenter("Fire beats Ice, Ice beats Thunder, Thunder beats fire", Colors.GridHero, yOff: -2);
                textWorld.mainBoard.DrawOnCenter("Same element = no damage", Colors.GridHero, yOff: 0);
                textWorld.mainBoard.DrawOnCenter("It is best to have had some experience with vanilla mode", Colors.GridHero, yOff: 1);
                textWorld.mainBoard.DrawOnCenter("[w] Start Elemental Mode", Colors.GridHero, yOff: 4, alignString: false);
                textWorld.mainBoard.DrawOnCenter("[s] Go back", Colors.GridHero, yOff: 5, alignString: false);
            }


            if (mode >= 0)
            {
                wannaLeave = 1;
            }

            
            

            //string message = youWin;
            //if (battleResult.result == 2) message = youLose;
            //textWorld.mainBoard.DrawOnCenter(message, Colors.Board);
        }

        internal void Reset()
        {
            mode = -1;
            wannaLeave = 0;
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

    }
}
