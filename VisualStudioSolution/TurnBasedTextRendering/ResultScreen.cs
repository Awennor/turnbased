﻿using Pidroh.BaseUtils;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class ResultScreen : ITextScreen_
    {
        private TextWorld textWorld;
        string youWin = "You Win";
        public MouseIO Mouse { set; get; }
        string youLose = "You lose";
        public BattleResult battleResult;
        public TrackBattle trackBattle;
        private TextEntity prizeEntity;

        public KeyboardIO Keyboard { set; get; }

        public List<int> movePrizes = new List<int>();

        public ResultScreen(TrackBattle trackBattle)
        {
            textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            this.trackBattle = trackBattle;
            prizeEntity = textWorld.GetFreeEntity(GameMain.Width, GameMain.Height);
            prizeEntity.Origin.ResetInvisible();
        }

        public int wannaLeave;
        private int mode;
        internal List<MoveMetaData> moveRenders;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }
        public List<MoveMetaData> MoveRenders { get; internal set; }
        public int currencyPrize = -1;

        int moveIndex = 0;

        public void Enter()
        {
            wannaLeave = 0;
            mode = 0;
        }

        public void Draw(float f)
        {
            if (keyDownUnicode > 0)
            {
                if (mode == 1)
                {
                    moveIndex++;

                }
                else
                {
                    if(mode == 2)
                    {
                        wannaLeave = 1;
                    }
                    if(mode == 0)
                        mode = 1;
                }
                if (movePrizes.Count <= moveIndex)
                {
                    if(currencyPrize > 0){
                        mode = 2;
                        DrawCurrencyGet(currencyPrize);
                    }
                    else{
                        wannaLeave = 1;
                    }
                    
                    //wannaLeave = 1;
                }
                else
                {
                    DrawMoveGet(movePrizes[moveIndex]);
                }


            }
            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            string message = youWin;
            if (battleResult.result == 2) message = youLose;
            textWorld.mainBoard.DrawOnCenter(message, Colors.ThunderShot);

            int x = 3;
            int y = 3;
            BattleRender.DrawStrippedText(x,y, "TURNS", textWorld.mainBoard);
            var metrics = this.trackBattle.metrics;
            BattleRender.DrawSubText(x+14, y, metrics.round+"", textWorld.mainBoard);
            BattleRender.DrawStrippedText(x, y+2, "TIMES HIT", textWorld.mainBoard);
            BattleRender.DrawSubText(x + 14, y+2, metrics.damageTimes + "", textWorld.mainBoard);

            BattleRender.DrawStrippedText(x, y + 10, "RANK", textWorld.mainBoard);
            BattleRender.DrawSubText(x + 14, y + 10, metrics.rank + "", textWorld.mainBoard);

            textWorld.mainBoard.DrawHorizontalLine(Unicode.AsciiGridHor, y + 7, Colors.InputDescription);
            textWorld.mainBoard.DrawHorizontalLine(Unicode.AsciiGridHor, y + 13, Colors.InputDescription);
            textWorld.DrawChildren();

        }

        public void SkipToShowMove()
        {
            mode = 1;
            DrawMoveGet(movePrizes[moveIndex]);
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

        public void DrawMoveGet(int move)
        {
            prizeEntity.Origin.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            prizeEntity.Origin.DrawOnCenterHorizontal("You received a new attack", Colors.InputDescription, y: 18);
            prizeEntity.Origin.DrawOnCenterHorizontal(moveRenders[move].Label, Colors.inputKey, y:20);
        }

        public void DrawCurrencyGet(int amount)
        {
            prizeEntity.Origin.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            prizeEntity.Origin.DrawOnCenterHorizontal("You received data fragments", Colors.InputDescription, y: 18);
            prizeEntity.Origin.DrawOnCenterHorizontal(""+amount, Colors.inputKey, y: 20);
        }

        internal void AddMovePrize(int v)
        {
            movePrizes.Add(v);
        }
    }
}
