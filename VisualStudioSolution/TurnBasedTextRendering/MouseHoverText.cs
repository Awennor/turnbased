﻿using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class MouseHoverText
    {
        public string[][] texts = new string[3][];
        public MouseHoverManager hoverManager;
        public TextEntity entity;

        public MouseHoverText(MouseHoverManager hoverManager, TextEntity entity, string[] moveDescriptions)
        {
            this.hoverManager = hoverManager;
            this.entity = entity;
            //texts[0] = new string[Enum.GetValues(typeof(BattleMain.MoveType)).Length];

            texts[0] = moveDescriptions;
            //Done,
        //    Redo,
        //Preview,
        //Confirm,
        //Cancel
            texts[1] = new string[] {
                "Starts command execution",
                "Removes the last inputted command",
                "Preview moves of the opponents",
                "Inputs move",
                "Returns",
                "Shows helpful information",
            };
            texts[2] = new string[] {
                "Moves in the corresponding direction",
            };
        }


        public void Update()
        {
            entity.ResetFull();
            hoverManager.Update();
            var active = hoverManager.mouseHoversActive;
            if (active.Count > 0)
            {
                //Console.Write("HOVER");
                int id = active[0].id;
                if(id >= 0)
                {
                    string text = texts[active[0].type][id];
                    //entity.Origin.Draw(text, 0, 0, 2, BattleRender.Colors.BackBattle);
                    entity.Origin.SetCursorAt(0,0);
                    entity.Origin.Draw_Cursor_SmartLineBreak(text, 2);
                    
                    int x = active[0].rect.X + 1 - text.Length/2;
                    if (x < 0) x = 1;
                    entity.SetPosition(x, active[0].rect.Y -2);
                }

                
            }
        }
    }
}
