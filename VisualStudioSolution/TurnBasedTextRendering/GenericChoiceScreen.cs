﻿using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class GenericChoiceScreen : ITextScreen_
    {

        private TextWorld textWorld;
        private NavigationKeyboard navKey;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }
        public MouseIO Mouse { set; get; }
        public KeyboardIO Keyboard { set; get; }
        int mode = 0;
        public int chosenOption = -1;

        public List<string> choices = new List<string>();
        public List<string> extraText = new List<string>();
        public List<bool> enabled = new List<bool>();
        public string title;

        public GenericChoiceScreen()
        {
            textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            navKey = new NavigationKeyboard(textWorld);
        }

        public void Draw(float f)
        {
            navKey.Clear();
            textWorld.mainBoard.Reset();
            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackgroundInput);
            textWorld.DrawChildren();

            if (title != null)
                textWorld.mainBoard.DrawOnCenterHorizontal(title, BattleRender.Colors.Hero, 0, 1);

            for (int i = 0; i < choices.Count; i++)
            {
                int y = i * 2 + 5;
                int x = 4;
                int color = BattleRender.Colors.InputDescription;
                if (enabled.Count > i)
                {
                    if(!enabled[i])
                        color = BattleRender.Colors.WindowLabel;
                }
                textWorld.mainBoard.Draw(choices[i], x, y, color);
                navKey.Add(x - 2, y);
                
            }
            for (int i = 0; i < extraText.Count; i++)
            {
                int y = i * 2 + 5 + 1;
                int x = 12;
                textWorld.mainBoard.Draw(extraText[i], x, y, BattleRender.Colors.WindowLabel);
            }

            if (keyDownUnicode >= 0)
            {
                navKey.CheckInput(keyDownUnicode);
            }


            navKey.DrawControls();

            chosenOption = navKey.chosenOption;
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

        internal void ResetChoice()
        {
            navKey.ResetChoice();
            chosenOption = -1;
        }

        internal bool WannaLeave()
        {
            return navKey.wantToEscape;
        }

        internal void SetExtraText(int id, string text)
        {
            while (this.extraText.Count <= id)
            {
                this.extraText.Add(String.Empty);
            }
            extraText[id] = text;
        }

        internal void SetEnabled(int id, bool enable)
        {
            while (this.enabled.Count <= id)
            {
                this.enabled.Add(true);
            }
            enabled[id] = enable;
        }

    }
}
