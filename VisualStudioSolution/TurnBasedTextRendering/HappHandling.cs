﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.ECS;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class HappHandling
    {
        BattleRender battleRender;
        ECSManager ecs;
        public Action Handle;
        List<HappHandler> handlers = new List<HappHandler>();
        private QuickAccessorTwo<HappTags, TimeStampSnap> happs;
        private float highestHandled
        {
            get { return handleState.highestHandled; }
            set { handleState.highestHandled = value; }
        }
        private HappHandleState handleState = new HappHandleState();

        public HappHandling(BattleRender battleRender, BattleSetup battleSetup)
        {
            this.battleRender = battleRender;
            var world = battleRender.textWorld;
            var posAnim = world.AddAnimation(new PositionAnimation());
            var blinkAnim = world.AddAnimation(new BlinkAnim());
            var delayAnim = world.AddAnimation(new DelaysAnimation());
            this.ecs = battleSetup.ecs;
            this.ecs.CreateEntityWithComponent(handleState);
            var battleMain = battleSetup.battleMain;
            var time = battleSetup.timeStamp;
            battleRender.HappHandling = this;
            happs = ecs.QuickAccessor2<HappTags, TimeStampSnap>();
            highestHandled = -1;


            handlers.Add(new HappHandler((e) =>
            {



                var damage = e.GetComponent<HappDamageData>();

                var amount = damage.amount;


                


                string message;
                if (damage.elementalBlock)
                {

                    {
                        string text = damage.damageE.ToString().ToUpper() + " BLOCK " + damage.targetE.ToString().ToUpper();
                        var message2 = battleRender.textWorld.GetTempEntity(text.Length, 1);
                        int colorE = BattleRender.ElementToProjColor(damage.damageE);
                        message2.Origin.Draw(text, 0, 0, colorE);

                        blinkAnim.Add(message2.AnimBase(0.6f), BlinkAnim.BlinkData.FrontColor(colorE, 0.2f));
                        int offset = (int)Math.Floor(-text.Length / 2f);
                        message2.SetPosition(battleRender.battlerRenders[damage.target].GetPosition() + new Vector2D(+1 + offset, -3));
                        delayAnim.Delay(0.65f);
                    }
                    {
                        var pos = battleRender.BattleEntityToScreenPosition(battleMain.entities[damage.target].pos);
                        var blast = battleRender.textWorld.GetTempEntity(5, 5);
                        blast.SetPosition(pos + new Vector2D(-2, -2));

                        blast.Origin.DrawRect(' ', 0, 0, 5, 5, TextBoard.INVISIBLECOLOR, BattleRender.ElementToProjColor(damage.damageE));
                        //blast.Origin.DrawRepeated(' ', 1, 1, 3, 3, TextBoard.INVISIBLECOLOR, BattleRender.ElementToProjColor(damage.damageE));
                        blinkAnim.Add(blast.AnimBase(0.2f), BlinkAnim.BlinkData.BackColor(BattleRender.Colors.inputKey, 0.05f, false));
                    }
                    message = damage.damageE + " absorbs " + damage.targetE + "\n";
                    message += battleRender.GetEntityName(damage.target) + " is unafectted.";
                }
                else
                {

                    //message = battleRender.GetEntityName(damage.target) + " gets hit!";
                    if (damage.superEffective)
                    {

                        {
                            string text = damage.damageE.ToString().ToUpper() + " BREAK " + damage.targetE.ToString().ToUpper();
                            var message2 = battleRender.textWorld.GetTempEntity(text.Length, 1);
                            int colorE = BattleRender.ElementToProjColor(damage.damageE);
                            message2.Origin.Draw(text, 0, 0, colorE);

                            blinkAnim.Add(message2.AnimBase(0.45f), BlinkAnim.BlinkData.FrontColor(colorE, 0.2f));
                            int offset = (int)Math.Floor(-text.Length / 2f);
                            message2.SetPosition(battleRender.battlerRenders[damage.target].GetPosition() + new Vector2D(+1 + offset, -2));
                            delayAnim.Delay(0.65f);
                        }

                        message = null;
                        //message = damage.damageE + " ravages " + damage.targetE + "\n";
                        //message += battleRender.GetEntityName(damage.target)+" takes a heavy hit!";
                        {
                            var pos = battleRender.BattleEntityToScreenPosition(battleMain.entities[damage.target].pos);
                            var blast = battleRender.textWorld.GetTempEntity(5, 5);
                            blast.SetPosition(pos + new Vector2D(-2, -2));

                            blast.Origin.DrawRepeated(' ', 1, 1, 3, 3, TextBoard.INVISIBLECOLOR, BattleRender.Colors.Enemy);
                            blinkAnim.Add(blast.AnimBase(0.2f), BlinkAnim.BlinkData.BackColor(BattleRender.ElementToProjColor(damage.damageE), 0.05f));
                        }
                    }
                    else
                    {
                        //message = battleRender.GetEntityName(damage.target) + " gets hurt";
                        message = null;
                    }
                }

                {
                    var pos = battleRender.BattleEntityToScreenPosition(battleMain.entities[damage.target].pos);
                    var number = battleRender.textWorld.GetTempEntity(2, 1);

                    Vector2D initialPos = pos + new Vector2D(0, 0);
                    number.SetPosition(initialPos);

                    //number.Origin.DrawOneDigit(amount, 0, 0, BattleRender.Colors.Hero);
                    if(amount >= 10)
                        number.Origin.DrawTwoDigits(amount, 0, 0, BattleRender.Colors.Hero);
                    else
                        number.Origin.DrawOneDigit(amount, 0, 0, BattleRender.Colors.Hero);
                    posAnim.Add(number.AnimBase(0.6f), new PositionAnimation.PositionData(initialPos, initialPos + new Vector2D(0, -3)));
                    //blinkAnim.Add(number.AnimBase(1f), BlinkAnim.BlinkData.Char(' ', 5f));
                }

                if (message != null)
                    battleRender.ShowMessage(message);

                var defender = battleRender.battlerRenders[damage.target];

                //var fe = battleRender.textWorld.GetTempEntity(defender.Width, defender.Height);
                if (!damage.superEffective && !damage.elementalBlock 
                //&& battleMain.entities[damage.target].Alive
                )
                {
                    var fe = battleRender.textWorld.GetTempEntity(3, 3);
                    var backColor = BattleRender.ElementToProjColor(damage.damageE);
                    backColor = 0;
                    int xColor = BattleRender.ElementToProjColor(damage.damageE);
                    const char damageChar = 'X';
                    fe.Origin.DrawChar(damageChar, 1, 0, xColor, backColor);
                    fe.Origin.DrawChar(damageChar, 1, 1, xColor, backColor);
                    fe.Origin.DrawChar(damageChar, 1, 2, xColor, backColor);
                    fe.Origin.DrawChar(damageChar, 0, 1, xColor, backColor);
                    fe.Origin.DrawChar(damageChar, 2, 1, xColor, backColor);
                    //fe.Origin.DrawChar(TextBoard.NOCHANGECHAR, 0, 0);
                    fe.Origin.Position = defender.GetPosition() + new Vector2D(-1, -1);

                    blinkAnim.Add(fe.AnimBase(0.35f), BlinkAnim.BlinkData.Char('Z', 0.05f));
                    //blinkAnim.Add(fe.AnimBase(0.35f), BlinkAnim.BlinkData.BackColor(BattleRender.Colors.Hero, 0.05f));
                }

                //Console.Write("CHANGE ELE");

            }, MiscHappTags.Damage));
            handlers.Add(new HappHandler((e) =>
            {
                var hmd = e.GetComponent<HappMoveData>();


                string text = hmd.element.ToString();
                var message = battleRender.textWorld.GetTempEntity(text.Length, 1);
                message.Origin.Draw(text, 0, 0, BattleRender.Colors.Hero);
                blinkAnim.Add(message.AnimBase(0.5f), BlinkAnim.BlinkData.FrontColor(BattleRender.Colors.InputDescription, 0.15f));
                int offset = (int)Math.Floor(-text.Length / 2f);
                message.SetPosition(battleRender.battlerRenders[hmd.user].GetPosition() + new Vector2D(+1 + offset, -1));

            }, MiscHappTags.ChangeElement));
            handlers.Add(new HappHandler((e) =>
            {
                var hmd = e.GetComponent<HappMoveData>();
                //var defender = battleRender.battlerRenders[hmd.target];
                var pos = battleRender.BattleEntityToScreenPosition(battleMain.entities[hmd.user].pos);
                var blast = battleRender.textWorld.GetTempEntity(3, 3);
                blast.SetPosition(pos + new Vector2D(-1, -1));

                blast.Origin.DrawRepeated(' ', 1, 1, 1, 1, TextBoard.INVISIBLECOLOR, BattleRender.Colors.Enemy);
                blinkAnim.Add(blast.AnimBase(0.2f), BlinkAnim.BlinkData.BackColor(BattleRender.Colors.Hero, 0.05f));
                //delayAnim.Delay(5);
                //Console.Write("DEATH");
                //Console.Write("CHANGE ELE");

            }, MiscHappTags.Death));
            Action<Entity> moveHandle = (e) =>
            {
                //Console.WriteLine("HANDLE!3");
                var hmd = e.GetComponent<HappMoveData>();
                var hmf = e.GetComponent<HappMovement>();
                int eId = hmd.user;
                var mover = battleMain.entities[eId];

                var pos = mover.PositionV2D;
                var pos2 = hmf.moveTo;
                var posF = (pos + pos2) / 2;

                var fe = battleRender.battlerRenders[eId];
                //Console.WriteLine("Move fail");
                //Console.WriteLine("HAPP MOVE HANDLE");
                if (hmf.success)
                {
                    Vector2D finalPosScreen = battleRender.BattleEntityToScreenPosition(hmf.moveTo);
                    posAnim.Add(fe.AnimBase(0.2f), new PositionAnimation.PositionData(
                    battleRender.BattleEntityToScreenPosition(hmf.moveFrom),
                    finalPosScreen));
                    fe.SetPosition(finalPosScreen);
                }
                else
                {
                    posAnim.Add(fe.AnimBase(0.2f), new PositionAnimation.PositionData(
                    battleRender.BattleEntityToScreenPosition(hmf.moveFrom),
                    battleRender.BattleEntityToScreenPosition(posF)));
                }
                
            };
            handlers.Add(new HappHandler(moveHandle, MoveDataTags.Movement));

            handlers.Add(new HappHandler((e) =>
            {
                var ha = e.GetComponent<HappArea>();
                var hmd = e.GetComponent<HappMoveData>();
                int eId = hmd.user;
                var mover = battleMain.entities[eId];
                //var userRender = battleRender.battlerEntities[eId];
                var area = ha.area;
                var points = area.points;

                var useEffect = world.GetTempEntity(1, 1);
                useEffect.SetPosition(battleRender.BattleEntityToScreenPosition(mover.pos));
                blinkAnim.Add(useEffect.AnimBase(0.5f), BlinkAnim.BlinkData.BackColor(BattleRender.ElementToProjColor(hmd.element), 0.15f));
                foreach (var item in points)
                {   
                    var entity = world.GetTempEntity(1, 1);
                    var finalPos = item * new Vector2D(ha.mirroringX, 1) + ha.offset;
                    if (finalPos.X < 0) continue;
                    if (finalPos.Y < 0) continue;
                    if (finalPos.X > 5) continue;
                    if (finalPos.Y > 2) continue;
                    //Console.Write(finalPos.XInt);
                    //Console.Write(finalPos.YInt);
                    var pos = battleRender.BattleEntityToScreenPosition(finalPos);
                    entity.SetPosition(pos.XInt, pos.YInt);
                    blinkAnim.Add(entity.AnimBase(0.5f), BlinkAnim.BlinkData.BackColor(BattleRender.ElementToProjColor(hmd.element), 0.15f));
                }
            }, MoveDataTags.Bomb));
            handlers.Add(new HappHandler((e) =>
            {
                var ha = e.GetComponent<HappShoot>();
                var md = e.GetComponent<HappMoveData>();
                if (ha != null)
                {
                    
                    BattleMain.Element element = md.element;
                    TextEntity fe = battleRender.GetProjTextEntity(element);
                    var pos = ha.start;
                    var pos2 = ha.end;
                    var xDis = Math.Abs(pos.X - pos2.X);    
                    float timeV = (float)xDis * 0.1f;
                    posAnim.Add(fe.AnimBase(timeV), new PositionAnimation.PositionData(
                            battleRender.BattleEntityToScreenPosition(pos),
                            battleRender.BattleEntityToScreenPosition(pos2)));


                }

            }, MoveDataTags.Shoot));

            Handle = () =>
            {
                //Console.WriteLine("HANDLE!");
                float newHighestHandled = highestHandled;
                for (int i = 0; i < happs.Length; i++)
                {
                    //Console.Write("ADV"+battleRender.CanAdvanceGraphics());
                    if (!battleRender.CanAdvanceGraphics()) break;
                    var tags = happs.Comp1(i);
                    //if (happs.Comp2(i).TimeSnap > highestHandled)
                    if (i > highestHandled)
                    {
                        //newHighestHandled = happs.Comp2(i).TimeSnap;
                        newHighestHandled = i;
                        //Console.Write("Handled"+i);
                        //Console.WriteLine("HANDLE!");
                        bool handled = false;
                        foreach (var han in handlers)
                        {
                            //Console.WriteLine("HANDLE!x");
                            if (han.CanHandle(tags.tags))
                            {
                                //Console.Write("HandledX" + i);
                                handled = true;
                                //Console.WriteLine(happs.Comp2(i).TimeSnap + " - " + time.CurrentSnap);
                                //Console.WriteLine("HANDLE!2");
                                han.Handler(happs.Entity(i));
                            }
                        }
                        if (!handled)
                        {
                            foreach (var t in tags.tags)
                            {
                                //Console.WriteLine("Not handled tag " + t);
                            }
                        }
                    }
                    else
                    {
                        //Console.WriteLine(happs.Comp2(i).TimeSnap+" - "+ time.CurrentSnap);
                    }
                }
                highestHandled = newHighestHandled;
            };

        }

        public class HappHandleState
        {
            public float highestHandled;
        }

        public class HappHandler
        {
            internal List<int> necessaryTags = new List<int>();
            internal Action<Entity> Handler;

            public HappHandler(Action<Entity> handler, params object[] tags)
            {
                foreach (var t in tags)
                {
                    necessaryTags.Add(Convert.ToInt32(t));
                }
                this.Handler = handler;
            }

            internal bool CanHandle(List<int> tags)
            {
                foreach (var item in necessaryTags)
                {
                    if (!tags.Contains(item))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        internal bool IsDone()
        {
            return highestHandled >= happs.Length - 1;
        }
    }
}
