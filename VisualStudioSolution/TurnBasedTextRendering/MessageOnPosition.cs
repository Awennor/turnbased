﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.TurnBased.TextRendering
{
    public class MessageOnPosition
    {
        BattleRender battleRender;
        private TextWorld textWorld;
        private BlinkAnim blinkAnim;

        public MessageOnPosition(BattleRender battleRender)
        {
            textWorld = battleRender.textWorld;
            blinkAnim = textWorld.AddAnimation(new BlinkAnim());
        }

        public void MessageOnPos(Vector2D pos, string v)
        {
            var entity = textWorld.GetTempEntity(v.Length + 2, 6);
            blinkAnim.Add(entity.AnimBase(2f), new BlinkAnim.BlinkData(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, TextBoard.NOCHANGECOLOR, 0.35f, 0.35f));
            var xOff = (v.Length - 3) / 2;
            if (xOff < 0) xOff = 0;
            var lineStart = new Vector2D(xOff, 0);
            entity.SetPosition(pos + new Vector2D(1 - xOff, 0));
            //Console.Write(pos);
            //entity.Origin.Draw(v, 1, 5);
            entity.Origin.DrawWithGrid(v, 0, 3, Colors.Hero, Colors.Hero);

            entity.Origin.DrawLines(Colors.Hero, lineStart, lineStart + new Vector2D(2, 0), lineStart + new Vector2D(2, 2));
            entity.Origin.AutoFixGridding();
        }
    }
}
