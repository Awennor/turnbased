﻿using Pidroh.ConsoleApp.Turnbased;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class HelpScreenIntegration
    {
        public bool showRequest;
        internal ITextScreen_ caller;

        internal void Show(ITextScreen_ screen)
        {
            showRequest = true;
            caller = screen;
        }
    }
}
