﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class InputHandling
    {
        int[] unfixedCommandKeys = {'1', '2','3','4', '5' };
        Dictionary<InputUnit, int> fixedMoveButtons = new Dictionary<InputUnit, int>()
        {
            { new InputUnit(InputType.Move, BattleMain.MoveType.NormalShot), 'g'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.Fire), 'f'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.Ice), 'i'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.IceBomb), 'b'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.ThunderBomb), 'y'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.Thunder), 't'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.MoveRight), 'd'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.MoveUp), 'w'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.MoveDown), 's'},
            { new InputUnit(InputType.Move, BattleMain.MoveType.MoveLeft), 'a'},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Done), Unicode.Space},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Redo), 'r'},
            //{ new InputUnit(InputType.MiscBattle, MiscBattleInput.Redo), Unicode.Backspace},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Preview), 'p'},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Help), 'h'},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Cancel), 'r'},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.Confirm), Unicode.Space},
            { new InputUnit(InputType.MiscBattle, MiscBattleInput.LeaveAttackDemonstration), Unicode.Escape},
        };

        List<KeyValuePair<InputUnit, int>> fixedMoveButtonAlternative = new List<KeyValuePair<InputUnit, int>>()
        {
            new KeyValuePair<InputUnit, int>(new InputUnit(InputType.MiscBattle, MiscBattleInput.Redo),Unicode.Backspace)
        };

        //List<InputUnit> buttonEquivalence = new List<InputUnit>();
        //List<int> buttonEquivalenceKey = new List<int>();

        public int GetFixedMoveUnicode(InputUnit input)
        {
            int value;
            if (fixedMoveButtons.TryGetValue(input, out value))
            {

            }
            else
            {
                value = -1;
            }
            return value;
        }

        public InputUnit Inputted(int unicodeKey, InputHolder input)
        {
            //Console.WriteLine(" input + "+(char)unicodeKey);
            foreach (var item in fixedMoveButtons)
            {
                if (item.Value == unicodeKey)
                {
                    if(input.Contains(item.Key))
                        return item.Key;
                }
            }
            foreach (var item in fixedMoveButtonAlternative)
            {
                if (item.Value == unicodeKey)
                {
                    if (input.Contains(item.Key))
                        return item.Key;
                }
            }
            for (int i = 0; i < unfixedCommandKeys.Length; i++)
            {
                if (unfixedCommandKeys[i] == unicodeKey)
                {
                    int unfixedCommandPos = 0;
                    for (int i2 = 0; i2 < input.inputs.Count; i2++)
                    {
                        if (input.TagIs(i2, InputTags.MOVEUNFIX))
                        {
                            if (unfixedCommandPos == i)
                            {
                                return input.inputs[i2];
                            }
                            unfixedCommandPos++;
                        }
                    }
                }
            }
            return default(InputUnit);
        }
    }
}
