﻿using Pidroh.BaseUtils;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using Pidroh.TurnBased.TextRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleMain;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class BattleRender : ITextScreen_
    {
        internal AttackPreview attackPreview;
        public KeyboardIO Keyboard { set; get; }
        private BattleMain turnBaseTry;
        private readonly StageData stageData;
        private readonly PreviewSystem previewSystem;
        private readonly List<MoveMetaData> moveRenders;
        private PositionAnimation posAnim;
        private CharByCharAnimation charByCharAnim;
        private DelaysAnimation delayAnim;

        public TextWorld textWorld;
        public TextBoard TextBoard { get; set; }
        List<BattlePhase> inputPhases = new List<BattlePhase>() { BattlePhase.PickHands, BattlePhase.ConfirmInput };

        internal Vector2D EntityScreenPosition(int user)
        {
            return BattleEntityToScreenPosition(turnBaseTry.entities[user].pos);
        }

        internal Rect GetGridRect()
        {
            return new Rect(gridOffsetx, gridOffsety, gridScale * 6, gridScale * 3);
        }

        int input;
        public int Input
        {
            get { return input; }
            set
            {
                input = value; //Console.WriteLine(value);
            }
        }

        public HappHandling HappHandling { get; internal set; }
        public int keyDownUnicode { set; get; } = -1;
        public MouseIO Mouse { set; get; }
        public MouseHoverManager mouseHover;

        //public List<DelayedActions> tasks = new List<DelayedActions>();
        //Dictionary<object, string> moveChars;
        //Dictionary<object, string> moveDescriptions = new Dictionary<object, string>();
        Dictionary<MiscBattleInput, string> miscDescriptions = new Dictionary<MiscBattleInput, string>() {
            {MiscBattleInput.Done ,"DONE" },
            {MiscBattleInput.Redo ,"REDO" },
            {MiscBattleInput.Help ,"HELP" },
            {MiscBattleInput.Preview ,"PREVIEW" },
            {MiscBattleInput.Confirm ,"CONFIRM" },
            {MiscBattleInput.Cancel ,"CANCEL" },
            {MiscBattleInput.LeaveAttackDemonstration ,"END PREVIEW" },
        };
        private Dictionary<InputUnit, string> moveButtons;

        private bool debugOn = true;
        private int gridScale = 5;
        private int gridOffsetx = 2;
        private int gridOffsety = 1;
        internal List<TextEntity> battlerRenders;

        char[][] entitiesChars;
        private bool MessageDoNotHide;
        string message = null;
        private bool waitingForMessageInput;
        private BattlePhase lastPhase;
        private TextEntity messageEnt;

        public InputHandling inputH = new InputHandling();
        //public bool helpVisualizeRequest;
        public HelpScreenIntegration help;
        private bool attackMenu = false;

        public BattleRender(BattleMain battleLogic, StageData stageData, PreviewSystem PreviewSystem, List<string> entityTexts, List<MoveMetaData> moveRenders)
        {


            //string[] entityTexts = { "@", "&", "%", "$", "O", "X", "J", "Y", "Z", "M","C", "D", "E" };
            entitiesChars = new char[entityTexts.Count][];
            for (int i = 0; i < entityTexts.Count; i++)
            {
                entitiesChars[i] = entityTexts[i].ToCharArray();
            }

            turnBaseTry = battleLogic;
            this.stageData = stageData;
            previewSystem = PreviewSystem;
            this.moveRenders = moveRenders;
            textWorld = new TextWorld();
            posAnim = textWorld.AddAnimation(new PositionAnimation());
            charByCharAnim = textWorld.AddAnimation(new CharByCharAnimation());
            delayAnim = textWorld.AddAnimation(new DelaysAnimation());
            textWorld.Init(GameMain.Width, GameMain.Height);
            TextBoard = textWorld.mainBoard;
            //TextBoard = new TextBoard(70, 25);

            //var posAnim = textWorld.AddAnimation(new PositionAnimation());
            var blinkAnim = textWorld.AddAnimation(new BlinkAnim());

            battlerRenders = new List<TextEntity>();
            UpdateBattleRenderCount();


            messageEnt = textWorld.GetFreeEntity(40, 4);

            turnBaseTry.happManager.AddHandler(new Happs.HappHandler(HappTag.AttackHit, (h) =>
            {
                var attacker = turnBaseTry.entities[h.GetAttribute_Int(1)];
                int defenderEID = h.GetAttribute_Int(0);
                BattleEntity defender = null;
                if (defenderEID >= 0)
                    defender = turnBaseTry.entities[defenderEID];
                Element element = (Element)h.GetAttribute_Int(2);
                TextEntity fe = GetProjTextEntity(element);

                if (defender != null)
                {
                    var pos = attacker.PositionV2D;
                    var pos2 = defender.PositionV2D;
                    var xDis = Math.Abs(pos.X - pos2.X);
                    float time = (float)xDis * 0.1f;


                    posAnim.Add(fe.AnimBase(time), new PositionAnimation.PositionData(
                        BattleEntityToScreenPosition(attacker.PositionV2D),
                        BattleEntityToScreenPosition(defender.PositionV2D)));
                }
                else
                {
                    var pos = attacker.PositionV2D;
                    var pos2 = pos;
                    if (attacker.Type == BattleMain.EntityType.enemy)
                        pos2.X = -1;
                    else
                        pos2.X = 6;
                    var xDis = Math.Abs(pos.X - pos2.X);
                    float time = (float)xDis * 0.1f;
                    posAnim.Add(fe.AnimBase(time), new PositionAnimation.PositionData(
                        BattleEntityToScreenPosition(pos),
                        BattleEntityToScreenPosition(pos2)));
                }




            }));

            turnBaseTry.happManager.AddHandler(new Happs.HappHandler(BattleMain.HappTag.AttackMiss, (h) =>
            {

                var attacker = turnBaseTry.entities[h.GetAttribute_Int(0)];
                Element element = (Element)h.GetAttribute_Int(1);
                TextEntity fe = GetProjTextEntity(element);
                var pos = attacker.PositionV2D;
                var pos2 = pos;
                if (attacker.Type == BattleMain.EntityType.enemy)
                    pos2.X = -1;
                else
                    pos2.X = 6;
                var xDis = Math.Abs(pos.X - pos2.X);
                float time = (float)xDis * 0.1f;
                posAnim.Add(fe.AnimBase(time), new PositionAnimation.PositionData(
                    BattleEntityToScreenPosition(pos),
                    BattleEntityToScreenPosition(pos2)));
            }));


            //moveChars = new Dictionary<object, string>()
            //{
            //    { BattleMain.MoveType.Fire, "F"},
            //    { BattleMain.MoveType.Ice, "I"},
            //    { BattleMain.MoveType.Thunder, "T"},
            //    { BattleMain.MoveType.NormalShot, "G"},
            //    { BattleMain.MoveType.MoveRight, Unicode.Rightarrow2+""},
            //    { BattleMain.MoveType.MoveUp, Unicode.Uparrow2+""},
            //    { BattleMain.MoveType.MoveDown, Unicode.Downarrow2+""},
            //    { BattleMain.MoveType.MoveLeft, Unicode.Leftarrow2+""},
            //    { BattleMain.MoveType.IceBomb, "IB"},
            //    { BattleMain.MoveType.ThunderBomb, "TB"},
            //    { BattleMain.MoveType.SummonEntity, "SU"},
            //    { BattleMain.MoveType.DoNothing, " "},
            //    { BattleMain.MoveType.DownFire, "FD"},
            //    { BattleMain.MoveType.TripleFire, "TF"},
            //};

            //moveDescriptions = new Dictionary<object, string>()
            //{

            //    { BattleMain.MoveType.Ice, "Ice Shot"},
            //    { BattleMain.MoveType.Fire, "Fire Shot"},
            //    { BattleMain.MoveType.Thunder, "Thunder Shot"},
            //    { BattleMain.MoveType.IceBomb, "Ice Bomb"},
            //    { BattleMain.MoveType.NormalShot, "Gun"},
            //    { BattleMain.MoveType.MoveRight, Unicode.Rightarrow2+""},
            //    { BattleMain.MoveType.MoveUp, Unicode.Uparrow2+""},
            //    { BattleMain.MoveType.MoveDown, Unicode.Downarrow2+""},
            //    { BattleMain.MoveType.MoveLeft, Unicode.Leftarrow2+""},
            //    { BattleMain.MoveType.ThunderBomb, "Thunder Bomb"},
            //    { BattleMain.MoveType.SummonEntity, "Summon"},
            //};

            var mesOnPos = new MessageOnPosition(this);

            for (int i = 0; i < turnBaseTry.entities.Count; i++)
            {
                var e = turnBaseTry.entities[i];
                if (e.Type == EntityType.hero)
                {
                    var pos = battlerRenders[i].GetPosition();
                    //mesOnPos.MessageOnPos(BattleEntityToScreenPosition(turnBaseTry.entities[i].pos), "YOU");
                    //MessageOnPos(Vector2D.Zero, "YOU");
                }
            }


            //Console.ReadLine();
        }



        private void UpdateBattleRenderCount()
        {
            while (battlerRenders.Count < this.turnBaseTry.entities.Count)
            {
                TextEntity item = textWorld.GetFreeEntity(2, 2);
                battlerRenders.Add(item);
                item.SetPosition(BattleEntityToScreenPosition(turnBaseTry.entities[battlerRenders.Count - 1].pos));

            }
            if (battlerRenders.Count > this.turnBaseTry.entities.Count)
            {
                for (int i = this.turnBaseTry.entities.Count; i < battlerRenders.Count; i++)
                {
                    battlerRenders[i].Origin.ResetInvisible();
                }
            }
        }

        internal string GetEntityName(int user)
        {
            BattleEntity gameEntity = turnBaseTry.entities[user];
            var chars = GetChar(gameEntity);
            string name = new string(chars);
            if (gameEntity.graphicRepeatedIndex > 0)
            {
                return name + (gameEntity.graphicRepeatedIndex + 1);
            }
            else
            {
                return name;
            }


        }

        public TextEntity GetProjTextEntity(Element element)
        {
            var fe = textWorld.GetTempEntity(1, 1);
            fe.Origin.DrawChar(TextBoard.INVISIBLECHAR, 0, 0);
            int elementColor = ElementToProjColor(element);
            fe.Origin.SetBackColor(elementColor, 0, 0);
            return fe;
        }

        public void Draw(float delta)
        {

            InputKey input = (InputKey)Input;
            if (input != InputKey.NONE && waitingForMessageInput)
            {
                waitingForMessageInput = false;
                message = null;
            }
            //if (input != InputKey.NONE) Console.WriteLine(input);
            //int inputNumber = input - '0';
            //if (debugOn && input == 'k')
            //{
            //    DebugExtra.DebugEx.Show();
            //}

            if (lastPhase != turnBaseTry.battleState.phase)
            {
                if (turnBaseTry.battleState.phase == BattlePhase.PickHands)
                {
                    //ShowMessage("Pick your commands", false, true);
                    //TextBoard.SetAll(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, Colors.FireAura);

                }
                if (lastPhase == BattlePhase.PickHands)
                {
                    //Console.Write("X__X");
                    HideMessage();
                    //TextBoard.SetAll(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, 0);
                }
            }
            lastPhase = turnBaseTry.battleState.phase;
            if (inputPhases.Contains(turnBaseTry.battleState.phase))
            {
                if (keyDownUnicode >= 0)
                {
                    if (keyDownUnicode == 'p')
                    {
                        //Console.WriteLine("PREVIEW");
                        previewSystem.StartPreview();
                        return;
                    }
                    if (keyDownUnicode == 'e' && HasAttacks())
                    {
                        attackMenu = !attackMenu;

                    }

                    if (attackMenu && keyDownUnicode == Unicode.Escape)
                    {
                        attackMenu = false;
                    }
                    var inputX = inputH.Inputted(keyDownUnicode, turnBaseTry.inputs);
                    if (inputX.type == InputType.MiscBattle && inputX.arg1 == (int)MiscBattleInput.Help)
                    {
                        //helpVisualizeRequest = true;
                        help.Show(this);
                    }

                    if (inputX.type != InputType.None)
                    {

                        turnBaseTry.InputDone(inputX);
                    }

                }
                if (Keyboard.downUnicodes.Contains('c'))
                {
                    attackMenu = false;
                }
                else
                {
                    attackMenu = true;
                }
                if (turnBaseTry.inputs.inputForConfirmation.type != InputType.None)
                {
                    if (false)
                    //if (turnBaseTry.inputs.inputForConfirmation.type == InputType.Move)
                    {
                        attackPreview.ShowPreview(0, turnBaseTry.inputs.inputForConfirmation.arg1);
                        turnBaseTry.ConfirmInputStart();
                    }
                    else
                    {
                        attackPreview.End();
                        turnBaseTry.InputConfirmed();
                    }

                }
                else
                {
                    attackPreview.End();
                }


                //foreach (var item in moveKeys)
                //{
                //    if (item.Value == input)
                //    {
                //        turnBaseTry.InputDone(item.Key);
                //    }
                //}
            }
            UpdateBattleRenderCount();
            DrawGraphics(delta);
            if (CanAdvance_Logic())
            {
                if (turnBaseTry.battleState.phase != BattlePhase.ExecuteMove && previewSystem.previewActive)
                {
                    previewSystem.EndPreview();
                }
                switch (turnBaseTry.battleState.phase)
                {
                    case BattleMain.BattlePhase.EnemyMoveChoice:
                        turnBaseTry.Tick();
                        break;
                    case BattleMain.BattlePhase.HandRecharge:
                        turnBaseTry.Tick();
                        break;
                    case BattleMain.BattlePhase.PickHands:

                        break;
                    case BattleMain.BattlePhase.ExecuteMove:
                        //System.Threading.Thread.Sleep(300);
                        turnBaseTry.Tick();
                        break;
                    default:

                        break;
                }
            }
            //UpdateBattleRenderCount();
            //DrawGraphics(delta);

        }

        public bool CanAdvanceGraphics()
        {
            return textWorld.IsDone() && !waitingForMessageInput;
        }

        private bool CanAdvance_Logic()
        {
            return CanAdvanceGraphics() && HappHandling.IsDone();
        }

        public void ShowMessage(string s, bool waitForInput = true, bool doNotHide = false)
        {
            this.MessageDoNotHide = doNotHide;
            message = s;
            messageEnt.Origin.ResetInvisible();
            float timeToWrite = message.Length * 0.015f;
            if (timeToWrite > 0.4f) timeToWrite = 0.4f;
            charByCharAnim.Add(messageEnt.AnimBase(timeToWrite), new CharByCharAnimation.CharByCharData(0, message.Length + 1));
            delayAnim.Delay(timeToWrite + 0.8f);

            //waitingForMessageInput = waitForInput;
            //Console.Write("M: "+s);
        }

        public void HideMessage()
        {
            message = null;
            waitingForMessageInput = false;
            //Console.Write("M: "+s);
        }

        public void ShowBattleMessage(string s)
        {
            if (!turnBaseTry.BattleDecided())
                ShowMessage(s);
            //Console.Write("M: "+s);
        }

        public void DrawGraphics(float delta)
        {
            mouseHover.mouseHovers.Clear();
            turnBaseTry.Update(delta);
            //clear grid
            TextBoard.Reset();

            if (inputPhases.Contains(turnBaseTry.battleState.phase))
            {
                TextBoard.SetAll(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            }

            int controlsY = gridScale * 3 + 10 + 3 + 2;

            int enemyGridOffX = gridScale * 3;
            bool drawDot = false;
            TextBoard.DrawRepeated(' ', gridOffsetx, gridOffsety, gridScale * 6, gridScale * 3, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
            for (int i = 0; i < 3 * gridScale; i++)
            {
                for (int j = 0; j < 3 * gridScale; j++)
                {
                    if (drawDot)
                    {
                        TextBoard.DrawChar(
                        '.',
                        gridOffsetx + i,
                        gridOffsety + j, Colors.GridHero);
                        TextBoard.DrawChar(
                            '.',
                            gridOffsetx + i + enemyGridOffX,
                            gridOffsety + j, Colors.GridEnemy);
                    }
                    if (i % gridScale == 0 && j % gridScale == 0)
                    {

                        TextBoard.DrawGrid(i + gridOffsetx + enemyGridOffX, j + gridOffsety, gridScale, gridScale, Colors.GridEnemy);
                        TextBoard.DrawGrid(i + gridOffsetx, j + gridOffsety, gridScale, gridScale, Colors.GridHero);
                    }
                }
            }

            for (int i = 0; i < turnBaseTry.entities.Count; i++)
            {

                BattleMain.BattleEntity gameEntity = turnBaseTry.entities[i];

                var ec = GetChar(gameEntity);

                var pos = gameEntity.PositionV2D;
                var screenPos = BattleEntityToScreenPosition((BaseUtils.Vector2D)pos);
                if (gameEntity.Type == EntityType.paneleffect)
                {
                    screenPos.Y = screenPos.Y + 1;
                    screenPos.X = screenPos.X - 1;
                }
                if (textWorld.IsDone() && HappHandling.IsDone())
                    battlerRenders[i].Origin.Position = screenPos;
                if (battlerRenders[i].Origin.Position != screenPos && textWorld.IsDone())
                {
                    //battlerRenders[i].Origin.Position = screenPos;
                    //float time = 0.15f;
                    ////time = 5;
                    //posAnim.Add(battlerRenders[i].AnimBase(time), new PositionAnimation.PositionData(battlerRenders[i].Origin.Position, screenPos, true));
                }

                var c = Colors.Hero;
                if (gameEntity.Type == EntityType.enemy) c = Colors.Enemy;
                if (gameEntity.Type == EntityType.pickup) c = Colors.inputKey;
                if (gameEntity.Dead)
                    c = TextBoard.INVISIBLECOLOR;
                int bc = TextBoard.INVISIBLECOLOR;

                if (gameEntity.Alive)
                {
                    Element element = gameEntity.element;
                    if (element != Element.Nothingness)
                        c = ElementToAuraColor(element);

                }
                if (gameEntity.Dead)
                {
                    for (int j = 0; j < ec.Length + 1; j++)
                    {
                        battlerRenders[i].Origin.DrawChar(TextBoard.INVISIBLECHAR, j, 0, c, bc);
                    }

                }
                else
                {
                    battlerRenders[i].Origin.Draw(ec, 0, 0, c, bc);
                    if (gameEntity.graphicRepeatedIndex > 0)
                        battlerRenders[i].Origin.DrawOneDigit(gameEntity.graphicRepeatedIndex + 1, 0 + ec.Length, 0, c, bc);
                }


            }


            int textBoardHeight = 3 * gridScale;

            {
                //int y = 2;
                //int x = 6 * gridScale + 20;

                int x = 3;

                if (inputPhases.Contains(turnBaseTry.battleState.phase))
                {
                    DrawControls(controlsY, 7);
                    if (turnBaseTry.timeToChoose > 0)
                    {
                        float ratio = turnBaseTry.timeToChoose / turnBaseTry.timeToChooseMax;
                        TextBoard.DrawRepeated(' ', x, controlsY + 16, (int)(ratio * 15), 1, Colors.Board, Colors.GridHero);
                    }
                }
                else
                {
                    TextBoard.DrawRepeated(' ', x - 1, controlsY - 1, 15, 15, Colors.Board);
                }
            }

            int turnOrderX = 6 * gridScale + 5;
            int turnOrderY = 2;
            turnOrderX = 2;
            turnOrderY = 3 * gridScale + 1;
            if (inputPhases.Contains(turnBaseTry.battleState.phase))
                turnOrderY += 3;

            DrawTurnOrder(turnOrderX, turnOrderY);
            if (!stageData.hideLifeUI)
            {
                DrawLife(turnOrderX + 14, turnOrderY);
            }

            {
                const int X = 2;
                //const int Y = 16;
                messageEnt.SetPosition(X, controlsY - 2);
                if (message != null && (!CanAdvanceGraphics()))
                {
                    //TextBoard.DrawGrid(
                    //    messageEnt.Origin.Position.XInt, messageEnt.Origin.Position.YInt, 
                    //    messageEnt.Width, messageEnt.Height, Colors.Board);
                    //messageEnt.Origin.DrawGrid(0, 0, 40, 4, Colors.Board);
                    messageEnt.Origin.DrawWithLinebreaks(message, 1, 0, 1, Colors.HeroTurn);
                }
                else
                {
                    if (!MessageDoNotHide)
                    {
                        message = null;
                        messageEnt.Origin.SetAll(TextBoard.INVISIBLECHAR);
                    }

                    //TextBoard.DrawRepeated(' ',X, Y, 40, 4, Colors.Board);
                }
            }
            TextBoard.CursorNewLine(1);
            TextBoard.CursorNewLine(1);
            //textBoard.Draw_Cursor(turnBaseTry.battleState.phase.ToString());


            textWorld.DrawChildren();
            textWorld.TryEndAnimations();
            textWorld.AdvanceTime(delta);

            if (CanAdvanceGraphics())
            {
                HappHandling.Handle();
                if (CanAdvanceGraphics())
                {
                    turnBaseTry.happManager.TryHandle();
                }
            }
            //if (CanAdvance())
            //{

            //}
        }

        public static int ElementToAuraColor(Element element)
        {
            int bc = TextBoard.INVISIBLECOLOR;
            if (element == Element.Fire)
            {
                bc = Colors.FireAura;
            }
            if (element == Element.Ice)
            {
                bc = Colors.IceAura;
            }
            if (element == Element.Thunder)
            {
                bc = Colors.ThunderAura;
            }

            return bc;
        }

        public static int ElementToProjColor(Element element)
        {
            int bc = Colors.inputKey;
            if (element == Element.Fire)
            {
                bc = Colors.FireShot;
            }
            if (element == Element.Ice)
            {
                bc = Colors.IceAura;
            }
            if (element == Element.Thunder)
            {
                bc = Colors.ThunderAura;
            }

            return bc;
        }

        public Vector2D BattleEntityToScreenPosition(BaseUtils.Vector2D pos)
        {
            var x = pos.X;
            var y = pos.Y;
            var screenPos = new BaseUtils.Vector2D(x * gridScale + gridScale / 2 + gridOffsetx, 2 * gridScale - y * gridScale + gridScale / 2 + gridOffsety);
            return screenPos;
        }

        private void DrawControls(int y, int x)
        {
            //TextBoard.DrawGrid(x - 2, y - 1, 20, 15, Colors.Board);
            //TextBoard.Draw("Controls", x, y, );
            TextBoard.SetCursorAt(x + 5, y);
            TextBoard.Draw_Cursor("Controls", Colors.WindowLabel);
            TextBoard.CursorNewLine(x);


            //InputTags inputTag = InputTags.MOVEFIX;
            int yOff = 0;

            if (!attackMenu)
            {
                yOff = DrawInputs_Fix(y, x, InputTags.MOVEFIX, yOff);

                //if (HasAttacks())
                //    GameMain.DrawInput(x, y + 2 + yOff, 'e', "ATTACK", TextBoard);
                //else
                //    DrawInput(x, y + 2 + yOff, "", "", TextBoard);
                //yOff += 2;


                //yOff++;
                yOff = DrawInputs_Fix(y, x, InputTags.MISC, yOff);
                //yOff++;
                //yOff = DrawInputs_Fix(y, x, InputTags.MOVEUNFIX, yOff);
                return;
            }




            int attackNumber = 1;
            for (int i = 0; i < turnBaseTry.inputs.inputs.Count; i++)
            {
                int x2 = x;
                int y2 = y + 2 + yOff;
                var input = turnBaseTry.inputs.inputs[i];

                if (turnBaseTry.inputs.TagIs(i, InputTags.MOVEUNFIX))
                {
                    yOff++;
                    yOff++;
                    int unicode = '0' + attackNumber;
                    attackNumber++;
                    mouseHover.mouseHovers.Add(new MouseHover(new Rect(x2 - 2, y2, 20, 1), 0, input.arg1));
                    //TextBoard.DrawChar('[', x2 - 1, y2, Colors.HeroTurn  );

                    //TextBoard.DrawChar(']', x2 + lengthBname, y2, Colors.HeroTurn);

                    string description = string.Empty;
                    if (input.type == InputType.Move)
                    {
                        if (input.arg1 >= 0)
                        {
                            MoveType m = (MoveType)input.arg1;

                            description = moveRenders[input.arg1].Label;
                            //moveDescriptions.TryGetValue(m, out description);
                            if (description == null)
                            {
                                description = m.ToString();
                            }
                        }


                    }
                    if (input.type == InputType.MiscBattle)
                    {
                        MiscBattleInput arg1 = (MiscBattleInput)input.arg1;
                        description = miscDescriptions[arg1];
                    }
                    var TextBoard = this.TextBoard;
                    GameMain.DrawInput(x2, y2, unicode, description, TextBoard);

                }

                //var c = moveChars[move];
                //DrawMove(move, Colors.HeroTurn);
                //TextBoard.Draw(c, x2 + 3, y2, Colors.HeroTurn);
                //TextBoard.DrawWithGrid(c+"", x2, y + 2, Colors.HeroTurn);
            }
            if (PreviewVisible())
                GameMain.DrawInput(x, y + 2 + yOff, 'p', "PREVIEW", TextBoard);
            else
                DrawInput(x, y + 2 + yOff, "", "", TextBoard);
            yOff += 2;
            GameMain.DrawInput(x, y + 2 + yOff, 'c', "CONTROLS", TextBoard);
        }

        private bool HasAttacks()
        {
            for (int i = 0; i < turnBaseTry.inputs.inputs.Count; i++)
            {
                if (turnBaseTry.inputs.TagIs(i, InputTags.MOVEUNFIX))
                {
                    return true;
                }
            }
            return false;
        }

        private bool PreviewVisible()
        {
            foreach (var e in turnBaseTry.entities)
            {
                if (e.Type == EntityType.enemy) return true;
            }
            return false;
        }

        private int DrawInputs_Fix(int y, int x, InputTags inputTag, int yOff)
        {

            for (int i = 0; i < turnBaseTry.inputs.inputs.Count; i++)
            {
                int x2 = x;
                int y2 = y + 2 + yOff;
                var input = turnBaseTry.inputs.inputs[i];

                if (turnBaseTry.inputs.TagIs(i, inputTag))
                {
                    var unicode = inputH.GetFixedMoveUnicode(input);
                    string forceInputLabel = null;
                    string forceCommandLabel = null;
                    bool movementCommand = unicode == 'w';
                    if (movementCommand)
                    {
                        forceInputLabel = UIReusable.WASD;
                        forceCommandLabel = UIReusable.arrows;
                    }
                    if (unicode == 'a' || unicode == 's' || unicode == 'd')
                    {
                        continue;
                    }
                    yOff++;
                    yOff++;



                    //TextBoard.DrawChar('[', x2 - 1, y2, Colors.HeroTurn);

                    //TextBoard.DrawChar(']', x2 + lengthBname, y2, Colors.HeroTurn);

                    string description = string.Empty;
                    if (input.type == InputType.Move)
                    {
                        if (forceCommandLabel != null)
                        {
                            description = forceCommandLabel;
                        }
                        else
                        {
                            description = moveRenders[input.arg1].Label;
                        }


                    }
                    if (input.type == InputType.MiscBattle)
                    {
                        MiscBattleInput arg1 = (MiscBattleInput)input.arg1;
                        description = miscDescriptions[arg1];
                        mouseHover.mouseHovers.Add(new MouseHover(new Rect(x2 - 2, y2, 20, 1), 1, input.arg1));
                    }
                    else
                    {
                        if (movementCommand)
                            mouseHover.mouseHovers.Add(new MouseHover(new Rect(x2 - 2, y2, 20, 1), 2, 0));
                        else
                            mouseHover.mouseHovers.Add(new MouseHover(new Rect(x2 - 2, y2, 20, 1), 0, input.arg1));
                    }
                    var TextBoard = this.TextBoard;
                    if (forceInputLabel == null)
                    {
                        DrawInput(x2, y2, unicode, description, TextBoard);
                    }
                    else
                    {
                        DrawInput(x2, y2, forceInputLabel, description, TextBoard);
                    }


                }

                //var c = moveChars[move];
                //DrawMove(move, Colors.HeroTurn);
                //TextBoard.Draw(c, x2 + 3, y2, Colors.HeroTurn);
                //TextBoard.DrawWithGrid(c+"", x2, y + 2, Colors.HeroTurn);
            }

            return yOff;
        }

        public static void DrawInput(int x2, int y2, int keyUnicode, string description, TextBoard TextBoard)
        {
            int x3 = DrawInpCommon(x2, y2, description, TextBoard);

            TextBoard.DrawUnicodeLabel(keyUnicode, x3, y2, Colors.inputKey);
            //TextBoard.DrawRect(TextBoard.NOCHANGECHAR, barx, y2, 6+x3-barx, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
            //TextBoard.DrawRect(TextBoard.NOCHANGECHAR, x2, y2, 6 + x3 - x2, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
        }

        private static int DrawInpCommon(int x2, int y2, string description, TextBoard TextBoard)
        {
            DrawBrownStripe(y2, TextBoard);
            TextBoard.Draw(description, x2, y2, Colors.InputDescription);
            int x3 = x2 + 14;

            //int offb = 7;
            int barx = x2 + description.Length;
            TextBoard.DrawRect(TextBoard.NOCHANGECHAR, x3, y2, 6, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
            return x3;
        }



        public static void DrawStrippedText(int x, int y, string description, TextBoard TextBoard)
        {
            DrawBrownStripe(y, TextBoard);
            TextBoard.Draw(description, x, y, Colors.InputDescription);
        }

        public static void DrawSubText(int x, int y, string description, TextBoard TextBoard)
        {
            TextBoard.DrawRect(TextBoard.NOCHANGECHAR, x, y, 6, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
            TextBoard.Draw(description, x + 1, y, Colors.inputKey);
        }

        public static void DrawInput(int x2, int y2, string keyLabel, string description, TextBoard TextBoard)
        {
            int x3 = DrawInpCommon(x2, y2, description, TextBoard);
            TextBoard.Draw(keyLabel, x3, y2, Colors.inputKey);
            //TextBoard.DrawRect(TextBoard.NOCHANGECHAR, barx, y2, 6 + x3 - barx, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);
            //TextBoard.DrawRect(TextBoard.NOCHANGECHAR, x2, y2, 6 + x3 - x2, 1, TextBoard.NOCHANGECOLOR, Colors.BackBattle);

        }

        private void DrawLife(int turnOrderX, int turnOrderY)
        {
            //TextBoard.DrawGrid(turnOrderX - 1, turnOrderY - 1, 20, 9, Colors.WindowLabel);
            TextBoard.SetCursorAt(turnOrderX + 1, turnOrderY);
            if (turnBaseTry.battleState.phase == BattlePhase.PickHands)
                TextBoard.Draw_Cursor("Life", Colors.WindowLabel);
            TextBoard.SetCursorAt(turnOrderX + 8, turnOrderY);
            if (turnBaseTry.battleState.phase == BattlePhase.PickHands)
                TextBoard.Draw_Cursor("Element", Colors.WindowLabel);
            int index = -1; //using this because not all units get drawn
            for (int i = 0; i < 4; i++)
            {
                int xOff = turnOrderX + 1;
                int yOff = turnOrderY + 2 + i * 2;
                TextBoard.DrawRect(TextBoard.NOCHANGECHAR, xOff, yOff, 4, 1, TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackBattle);
                TextBoard.DrawRect(TextBoard.NOCHANGECHAR, xOff + 7, yOff, 8, 1, TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackBattle);
            }
            for (int i = 0; i < turnBaseTry.entities.Count; i++)
            {
                if (i >= turnBaseTry.entities.Count)
                {
                    continue;
                }
                BattleMain.BattleEntity e = turnBaseTry.entities[i];
                if (!e.drawLife)
                {
                    continue;
                }
                if (!e.Dead)
                {
                    index++;
                    int xOff = turnOrderX + 1;
                    int yOff = turnOrderY + 2 + index * 2;
                    int color = Colors.HeroTurn;
                    if (e.Type == BattleMain.EntityType.enemy)
                    {
                        color = Colors.EnemyTurn;
                    }
                    if (e.element != Element.Nothingness)
                        color = ElementToAuraColor(e.element);
                    //TextBoard.DrawOneDigit_Cursor((int)e.life.Val);

                    //DrawEntityChar(e, color, xOff, yOff);
                    //TextBoard.DrawChar(GetChar(e), xOff, turnOrderY + 2, color);
                    TextBoard.DrawTwoDigits((int)e.life, xOff, yOff, color);
                    string element = string.Empty;
                    switch (e.element)
                    {
                        case Element.Fire:
                            element = "Fire";
                            break;
                        case Element.Ice:
                            element = "Ice";
                            break;
                        case Element.Thunder:
                            element = "Thunder";
                            break;
                        case Element.Nothingness:
                            break;
                        default:
                            break;
                    }
                    var eColor = ElementToAuraColor(e.element);

                    TextBoard.Draw(element, xOff + 7, yOff, eColor);
                }

                //TextBoard.DrawOneDigit_Cursor((int)e.life.Val);

                //TextBoard.CursorNewLine(x: 1);
            }
        }

        private void DrawBrownStripe(int yOff)
        {
            DrawBrownStripe(yOff, TextBoard);
        }

        static private void DrawBrownStripe(int yOff, TextBoard textBoard)
        {
            textBoard.DrawRect(TextBoard.NOCHANGECHAR, 0, yOff, textBoard.Width, 1, TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackStripe);
        }

        private void DrawTurnOrder(int turnOrderX, int turnOrderY, bool horizontal = true)
        {
            Value turnsPerPhase = turnBaseTry.battleState.turnsPerPhase;
            TextBoard.SetCursorAt(turnOrderX + 3, turnOrderY);
            if (turnBaseTry.battleState.phase == BattlePhase.PickHands)
                TextBoard.Draw_Cursor("Turn", Colors.WindowLabel);

            int drawingId = -1;
            for (int i = 0; i < 4; i++)
            {
                int yOff = turnOrderY + 2 + i * 2;
                DrawBrownStripe(yOff, TextBoard);
                for (int j = 0; j < 3; j++)
                {
                    int xOff = turnOrderX + j * 3 + 3;


                    TextBoard.DrawRect(TextBoard.NOCHANGECHAR, xOff, yOff, 2, 1, TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackBattle);
                }
            }
            for (int i = 0; i < turnBaseTry.entities.Count; i++)
            {

                BattleMain.BattleEntity e = turnBaseTry.entities[i];
                if (!e.drawTurn)
                {
                    continue;
                }
                if (!e.Dead)
                {
                    drawingId++;
                    int color = Colors.HeroTurn;
                    if (e.Type == BattleMain.EntityType.enemy)
                    {
                        color = Colors.EnemyTurn;
                    }
                    if (e.element != Element.Nothingness)
                        color = ElementToAuraColor(e.element);

                    //TextBoard.DrawOneDigit_Cursor((int)e.life.Val);
                    int xOff = turnOrderX + 1 + drawingId * 3;
                    int yEntity = turnOrderY + 2;
                    int yFirstMove = turnOrderY + 3;
                    int xFirstMove = xOff;
                    if (horizontal)
                    {
                        xOff = turnOrderX;
                        yEntity = turnOrderY + 2 + drawingId * 2;
                        yFirstMove = yEntity;
                        xFirstMove = turnOrderX + 3;
                    }
                    DrawEntityChar(e, color, xOff, yEntity);

                    TextBoard.SetCursorAt(xFirstMove, yFirstMove);

                    for (int i2 = 0; i2 < turnsPerPhase; i2++)
                    {
                        int color2 = color;
                        int backColor = Colors.BackCommand;
                        if (turnBaseTry.battleState.phase == BattleMain.BattlePhase.ExecuteMove)
                        {
                            if (drawingId == turnBaseTry.battleState.actingEntity && i2 == turnBaseTry.battleState.turn)
                            {
                            }
                            //color2 = Colors.Hero;
                            else
                            {
                                backColor = Colors.BackBattle;
                                color2 = Colors.InputDescription;
                            }

                        }

                        if (i2 < turnsPerPhase)
                        {
                            string c = GetCharOfMove(e, i2);
                            mouseHover.mouseHovers.Add(new MouseHover(new Rect(
                                TextBoard.CursorX,
                                TextBoard.CursorY,
                                c.Length,
                                1
                                ),
                                0, e.moves[i2])); //add here...? @_@

                            TextBoard.Draw_Cursor(c, color2);
                            if (horizontal)
                            {
                                for (int j = c.Length; j < 3; j++)
                                {
                                    TextBoard.AdvanceCursor();
                                }




                            }

                            //TextBoard.Draw_Cursor(' ');
                        }
                        else
                        {
                            TextBoard.Draw_Cursor(' ', color, Colors.BackCommand);
                        }
                        if (horizontal)
                        {

                        }
                        else
                        {
                            TextBoard.CursorNewLine(x: xFirstMove);
                        }
                    }
                }


                //TextBoard.CursorNewLine(x: 1);
            }
        }

        private void DrawEntityChar(BattleEntity e, int color, int x, int y)
        {
            char[] chars = GetChar(e);

            TextBoard.Draw(chars, x, y, color);
            if (e.graphicRepeatedIndex > 0)
            {
                TextBoard.DrawOneDigit(e.graphicRepeatedIndex + 1, x + chars.Length, y, color);
            }
        }

        private string GetCharOfMove(BattleMain.BattleEntity e, int i2)
        {


            int val = e.moves[i2];
            if (val >= 0)
                return moveRenders[val].Abrev;
            else
                return " ";
        }

        public char[] GetChar(BattleMain.BattleEntity gameEntity)
        {
            return entitiesChars[gameEntity.graphic];

        }

        private void DrawMove(Value move, int color)
        {
            if (move.Val >= 0)
            {
                BattleMain.MoveType m = (BattleMain.MoveType)move.Val;
                DrawMove(m, color);
            }
            else
            {
                TextBoard.Draw_Cursor(' ');
            }

        }

        private void DrawMove(BattleMain.MoveType move, int color)
        {
            var c = moveRenders[(int)move].Abrev;
            TextBoard.Draw_Cursor(c, color);
        }

        public TextBoard GetBoard()
        {
            return TextBoard;
        }

        public static class Colors
        {
            public const int GridHero = 1;
            public const int GridEnemy = 2;
            public const int Hero = 3;
            public const int Enemy = 4;
            public const int HeroTurn = 5;
            public const int EnemyTurn = 6;
            public const int inputKey = 7;
            public const int Board = 8;
            public const int WindowLabel = 9;
            internal static int FireAura = 10;
            internal static int IceAura = 11;
            internal static int ThunderAura = 12;
            internal static int FireShot = 13;
            internal static int IceShot = 14;
            internal static int ThunderShot = 15;
            public const int BackgroundInput = 16;
            public const int InputDescription = 17;
            internal static int BackBattle = 18;
            internal static int BackCommand = 19;
            internal const int BackStripe = 20;
        }

        public enum InputKey
        {
            NONE, LEFT, RIGHT, DOWN, UP, FIRE, REDO, DONE,
            ICE,
            THUNDER,
            NORMALSHOT
        }


    }

}
