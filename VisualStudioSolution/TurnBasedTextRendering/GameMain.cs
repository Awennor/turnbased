﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased.ContentCreator;
using Pidroh.ECS;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using Pidroh.TurnBased.TextRendering;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class GameMain : ITextScreen_
    {
        private BattleMain battleMain;
        private BattleRender battleRender;
        private ModeSelectionScreen modeSelectionScreen;
        ITextScreen_ mainDraw;
        private GenericChoiceScreen beforeLevelMenuScreen;
        private HelpScreen helpScreen;
        HelpScreenIntegration helpInt = new HelpScreenIntegration();
        private ResultScreen resultScreen;
        int stageId;

        private MouseHoverText mouseHover;
        public StageToBattle stageToBattle = new StageToBattle();
        List<StageProgressPersistence> stageProgressPersistence = new List<StageProgressPersistence>();
        public KeyboardIO Keyboard { set; get; } = new KeyboardIO();
        StageGroupNavigator stageGroupNav;
        int movePreviewCounter = -1;
        List<int> aux = new List<int>();

        public GameMain()
        {
            modeSelectionScreen = new ModeSelectionScreen();
            stageSelecScreen = new StageSelectionScreen();
            attackInventory = new AttackInventory();
            currencyHolder = new CurrencyHolder();
            currencyHolder.currency = 200;
            for (int i = (int)BattleMain.MoveType.Fire; i <= (int)BattleMain.MoveType.Thunder; i++)
            {
                attackInventory.attackInventory.Add(i);
            }
            attackInventory.equippedAttacks[0] = (int)BattleMain.MoveType.Fire;
            attackInventory.equippedAttacks[1] = (int)BattleMain.MoveType.Ice;
            attackInventory.equippedAttacks[2] = (int)BattleMain.MoveType.Thunder;
            //attackInventory.attackInventory.Add((int)BattleMain.MoveType.IceBomb);
            //attackInventory.attackInventory.Add((int)BattleMain.MoveType.ThunderBomb);
            //attackInventory.equippedAttacks[0] = (int)BattleMain.MoveType.Fire;
            //attackInventory.equippedAttacks[1] = (int)BattleMain.MoveType.Thunder;
            //attackInventory.equippedAttacks[2] = (int)BattleMain.MoveType.Ice;

            Reset();
            modeSelectionScreen.mode = 1;
            modeSelectionScreen.wannaLeave = 1;
            mainDraw = modeSelectionScreen;
            beforeLevelMenuScreen = new GenericChoiceScreen();
            beforeLevelMenuScreen.choices.Add("Begin Stage");
            beforeLevelMenuScreen.choices.Add("Change Attacks");
            beforeLevelMenuScreen.choices.Add("Acquire New Attack   Cost: 100");
            beforeLevelMenuScreen.title = "Stage Prelude";

        }

        private void Reset()
        {
            var wasInBeforeLevelMenu = mainDraw == beforeLevelMenuScreen;

            string[] moveDescriptions = new string[] {
                "",
                "Move up",
                "Move left",
                "Move down",
                "Move right",
                "Shoots forward",
                "Shoots fire forward",
                "Shoots ice forward",
                "Shoots bolt forward",
                "Throws ice bomb three squares forward",
                "Throws bolt bomb three squares forward",
                "Summons another enemy",
                "Shoot, down, shoot",
                "Shoots in three lines around user",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            };

            int mode = modeSelectionScreen.mode;
            bool timeAttack = modeSelectionScreen.timeAttack;

            var ecs = ECSManager.Create();


            StageDataCreator sdc = new StageDataCreator(ecs);
            //sdc.CreateDefaultStageData();

            new StageDataProcedural_A(sdc);

            //var stages = ecs.QuickAccessor1<StageData>();
            var groups = ecs.QuickAccessor1<StageDataGroup>();
            if (stageGroupNav == null)
            {
                stageGroupNav = new StageGroupNavigator(ecs);
            }

            var group = stageGroupNav.currentSDG;

            stageSelecScreen.stageGroup = group;

            stageId = stageSelecScreen.chosenStage;
            if (stageSelecScreen.chosenStage < 0)
            {
                stageId = 0;
            }
            int d = stageId;
            if (group.childIds.Count <= d)
            {
                mainDraw = modeSelectionScreen;
                modeSelectionScreen.Reset();
                stageId = 0;
                stageGroupNav.Backtrack();
                Reset();
                return;
            }

            List<string> entityRenderTexts = new List<string>();
            var moveCreator = new MoveCreatorProg(ecs);

            EnemyDataCreator enemyData = new EnemyDataCreator(entityRenderTexts, moveCreator);
            sdc.CalculateTrackInfo(enemyData.enemyDatas);
            sdc.InitializeProgress(stageProgressPersistence);

            StageData stageData = group.childIds[stageId].GetComponent<StageData>();
            if (stageSelecScreen.chosenStage < 0)
            {
                if (stageData == null)
                {
                    stageSelecScreen.ResetChoice();
                    stageSelecScreen.chosenStage = -1;
                    //Console.Write("STAGE GROUP DATA");
                }
            }
            else
            {
                if (stageData == null)
                {
                    stageSelecScreen.ResetChoice();
                    stageSelecScreen.chosenStage = -1;
                    StageDataGroup stageDataGroup = group.childIds[stageId].GetComponent<StageDataGroup>();
                    stageSelecScreen.stageGroup = stageDataGroup;
                    stageGroupNav.Enter(stageId);
                }

            }


            if (stageSelecScreen.chosenStage < 0)
            {
                mainDraw = stageSelecScreen;
                //Console.Write("STAGE SELEC SCREEN");
                stageSelecScreen.ResetChoice();
                stageSelecScreen.ResetCursor();
                return;

            }

            //battle code initialization
            //might return before coming here

            var battleConfig = stageData.battleConfig;

            stageEntity = group.childIds[stageId];
            if (group.childIds.Count > stageId + 1)
            {
                stageToBattle.stageToUnlock = group.childIds[stageId + 1];
            }
            else
            {
                stageToBattle.stageToUnlock = Entity.None;
            }
            stageToBattle.CurrentStage = stageEntity;

            //Find move for preview, if there is one
            int movePosition = -1;
            var spawns = stageData.spawnDatas;
            MoveData moveDataChosenPreview = null;
            int moveDataChosenPreviewIndex = -1;
            aux.Clear();
            if (false)
            {
                foreach (var s in spawns)
                {
                    if (s.entityType == (int)BattleMain.EntityType.enemy)
                    {
                        var enemy = enemyData.enemyDatas[s.id];
                        var ai = enemy.enemyAI;
                        var moves = ai.moves;
                        foreach (var move in moves)
                        {
                            if (move is MoveUse)
                            {


                                int moveIndex = ((MoveUse)move).move;
                                var moveData = moveCreator.moveDatas[moveIndex];
                                if (!moveData.tags.Contains((int)MoveDataTags.Movement))
                                {
                                    if (!aux.Contains(moveIndex))
                                    {
                                        aux.Add(moveIndex);
                                        movePosition++;
                                        if (movePosition == movePreviewCounter)
                                        {
                                            movePreviewCounter++;
                                            moveDataChosenPreview = moveData;
                                            moveDataChosenPreviewIndex = moveIndex;
                                            break;
                                        }
                                    }

                                }
                            }
                        }
                        if (moveDataChosenPreview != null)
                        {
                            break;
                        }
                    }
                }
            }


            BattleSetup battleSetup = new BattleSetup(mode, ecs, enemyData, moveCreator);
            if (moveDataChosenPreview == null)
            {

                battleSetup.StageInitialization(stageEntity, ecs);
                var fixedAttack = stageEntity.GetComponent<FixedAttackStage>();
                if (fixedAttack == null)
                {
                    for (int i = 0; i < attackInventory.equippedAttacks.Length; i++)
                    {
                        battleSetup.SetDefaultAttack(i, attackInventory.equippedAttacks[i]);
                    }
                }

            }
            else
            {
                battleSetup.AttackPreviewInitialization(moveDataChosenPreview, enemyData.startOfTutorialEnemy, moveDataChosenPreviewIndex);
            }



            battleMain = battleSetup.battleMain;
            List<MoveMetaData> moveRenders = moveCreator.moveRenders;
            //for (int i = 0; i < moveRenders.Count; i++)
            //{
            //    if (moveDescriptions.Length > i)
            //        moveRenders[i].Description = moveDescriptions[i];
            //}
            equipScreen = new EquipScreen(moveRenders);
            equipScreen.inventory = attackInventory;
            HelpScreenModel helpModel = new HelpScreenModel(battleMain);
            helpScreen = new HelpScreen(helpModel, moveRenders, moveCreator.moveDatas);

            var ps = new PreviewSystem(ecs, battleMain);


            float timeToChoose = -1;
            if (timeAttack)
            {
                timeToChoose = 30f;

            }
            battleMain.timeToChooseMax = timeToChoose;
            battleMain.Init();



            battleRender = new BattleRender(battleMain, stageData: stageData, PreviewSystem: ps, entityTexts: entityRenderTexts, moveRenders: moveRenders);
            battleRender.help = helpInt;
            new AttackPreview(ecs, battleRender);
            new HappHandling(battleRender, battleSetup);
            //

            if (helpScreen.IsWannaShowIntro())
            {
                helpScreen.Show();
                mainDraw = helpScreen;
                helpModel.battleIntroMode = true;
            }
            else
            {
                mainDraw = battleRender;
            }

            resultScreen = new ResultScreen(battleMain.trackBattle);

            resultScreen.battleResult = battleMain.battleResult;
            resultScreen.moveRenders = moveRenders;

            MouseHoverManager hoverManager = new MouseHoverManager(Mouse);
            hoverManager.mouseHovers.Add(new MouseHover(new BaseUtils.Rect(5, 5, 5, 5), 0, 0));


            mouseHover = new MouseHoverText(hoverManager, battleRender.textWorld.GetFreeEntity(battleRender.textWorld.mainBoard.Width - 2, 3), moveDescriptions);

            battleRender.mouseHover = hoverManager;

            //this.mainDraw = equipScreen;
            bool intermissionScreen = true;
            if (battleConfig != null)
                intermissionScreen = battleConfig.intermissionScreen;
            if (!wasInBeforeLevelMenu && intermissionScreen)
            {
                this.mainDraw = beforeLevelMenuScreen;
                beforeLevelMenuScreen.SetExtraText(2, "Fragments: " + currencyHolder.currency);
                beforeLevelMenuScreen.SetEnabled(2, currencyHolder.currency >= 100);
            }

        }

        public int Input { set { mainDraw.Input = value; } get { return 'c'; } }
        public int keyDownUnicode { set { value = remap.Remap(value); mainDraw.keyDownUnicode = value; } get { return 'c'; } }

        public MouseIO Mouse { set; get; } = new MouseIO();
        UnicodeRemap remap = new UnicodeRemap();
        //private StageGroupSelectionScreen stageGroupSelection;
        private StageSelectionScreen stageSelecScreen;
        private AttackInventory attackInventory;
        private CurrencyHolder currencyHolder;
        private EquipScreen equipScreen;
        private Entity stageEntity;
        public static readonly int Width = 35;
        public static readonly int Height = 46;

        public static void DrawInput(int x2, int y2, int unicode, string description, TextBoard textBoard)
        {
            BattleRender.DrawInput(x2, y2, unicode, description, textBoard);
        }

        public void Draw(float f)
        {
            if (mouseHover != null)
                mouseHover.Update();
            mainDraw.Draw(f);
            mainDraw.Mouse = Mouse;
            mainDraw.Keyboard = Keyboard;
            if (mainDraw == stageSelecScreen && stageSelecScreen.chosenStage >= 0)
            {
                movePreviewCounter = 0;
                Reset();
            }
            if (mainDraw == beforeLevelMenuScreen)
            {
                if (beforeLevelMenuScreen.WannaLeave())
                {
                    stageSelecScreen.ResetChoice();
                    mainDraw = stageSelecScreen;
                    beforeLevelMenuScreen.ResetChoice();
                }
                if (beforeLevelMenuScreen.chosenOption >= 0)
                {
                    if (beforeLevelMenuScreen.chosenOption == 0)
                    {
                        Reset();
                    }
                    if (beforeLevelMenuScreen.chosenOption == 1)
                    {
                        mainDraw = equipScreen;
                    }
                    if (beforeLevelMenuScreen.chosenOption == 2)
                    {
                        if (currencyHolder.currency >= 100)
                        {
                            currencyHolder.currency = currencyHolder.currency - 100;
                            aux.Clear();
                            for (int i = 14; i < 38; i++)
                            {
                                if (!attackInventory.attackInventory.Contains(i))
                                {
                                    aux.Add(i);
                                }
                            }
                            int attack = aux.RandomElement();
                            attackInventory.attackInventory.Add(attack);
                            //newAttackResultScreenMode = true;
                            mainDraw = resultScreen;
                            resultScreen.AddMovePrize(attack);
                            resultScreen.SkipToShowMove();
                        }
                    }

                    beforeLevelMenuScreen.ResetChoice();
                }
            }
            if (mainDraw == equipScreen)
            {
                if (equipScreen.wantToReturn)
                {
                    mainDraw = beforeLevelMenuScreen;
                    equipScreen.ResetChoice();
                }
            }
            if (stageSelecScreen.wantToReturn)
            {

                stageSelecScreen.ResetChoice();

                //stageGroupSelection.ResetChoice();
                //stageGroupSelection.wantedGroup = -1;
                //stageGroupSelection.wantedGroupEntity = Entity.None;

                stageSelecScreen.chosenStage = -1;
                stageGroupNav.Backtrack();

                Reset();
            }
            if (helpScreen != null)
            {
                if (helpScreen.wannaLeave == true)
                {
                    helpScreen.wannaLeave = false;
                    mainDraw = battleRender;

                }
            }

            if (helpInt.showRequest)
            {
                helpScreen.Show();
                helpInt.showRequest = false;
                mainDraw = helpScreen;
                helpScreen.HelpMode();
            }
            if (mainDraw == battleRender)
            {
                if (battleMain.IsOver())
                {
                    if (battleMain.BattleConfig.attackDemonstrationMode)
                    {
                        Reset();
                    }
                    else
                    {
                        if (battleMain.IsVictory())
                        {
                            stageId++;
                            stageToBattle.Victory();
                            stageSelecScreen.ResetChoice();
                            stageToBattle.BattleMetricsUpdate(battleMain.trackBattle.metrics);
                            var attackPrize = stageToBattle.CurrentStage.GetComponent<AttackPrize>();
                            if (attackPrize != null)
                            {
                                if (attackPrize.prizes != null)
                                {
                                    attackInventory.attackInventory.AddRange(attackPrize.prizes);
                                    resultScreen.AddMovePrize(attackPrize.prizes[0]);
                                }

                            }
                            var currencyPrize = stageToBattle.CurrentStage.GetComponent<CurrencyPrize>();
                            if (currencyPrize != null)
                            {
                                if (currencyPrize.currency != null)
                                {
                                    currencyHolder.currency += currencyPrize.currency[0];
                                    resultScreen.currencyPrize = currencyPrize.currency[0];
                                }
                            }
                        }
                        resultScreen.Enter();
                        mainDraw = resultScreen;
                    }

                }
            }
            if (mainDraw == resultScreen)
            {
                if (resultScreen.wannaLeave == 1)
                {
                    Reset();
                }
            }
            if (mainDraw == modeSelectionScreen)
            {
                if (modeSelectionScreen.wannaLeave == 1)
                {
                    Reset();
                }
            }

        }

        public TextBoard GetBoard()
        {
            return mainDraw.GetBoard();
        }
    }
}
