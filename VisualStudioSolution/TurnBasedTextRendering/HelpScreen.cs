﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.TurnBased.TextRendering
{
    class HelpScreen : ITextScreen_
    {
        public int Input { set; get; }
        public int keyDownUnicode { set; get; }
        public KeyboardIO Keyboard { set; get; }
        public MouseIO Mouse { set; get; } = new MouseIO();
        TextWorld textWorld;
        private readonly HelpScreenModel model;
        private readonly List<MoveMetaData> moveRenders;
        private readonly List<MoveData> moveDatas;
        char LeaveButton;
        public bool wannaLeave;
        private TextEntity explanationEntity;

        public HelpScreen(HelpScreenModel helpModel, List<MoveMetaData> moveRenders, List<MoveData> moveDatas)
        {
            this.textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            model = helpModel;
            this.moveRenders = moveRenders;
            this.moveDatas = moveDatas;
            textWorld.mainBoard.SetAll(TextBoard.INVISIBLECHAR, BattleRender.Colors.BackCommand, BattleRender.Colors.BackCommand);
            explanationEntity = textWorld.GetFreeEntity(GameMain.Width - 4, 35);
            explanationEntity.SetPosition(2, 4);
            //textWorld.GetFreeEntity(GameMain.Width - 4);
            //explanationEntity.Origin.Draw("SSS__SSSSDASDASDAS", 0,0, BattleRender.Colors.InputDescription);
        }

        public void Draw(float f)
        {
            var input = keyDownUnicode;
            if (input == LeaveButton) wannaLeave = true;
            if (input == 'H') wannaLeave = true;
            if (input == 'h') wannaLeave = true;
            if (input == Unicode.Escape) wannaLeave = true;



            //textWorld.mainBoard.Reset();
            int pos = 0;
            //textWorld.mainBoard.DrawWithLinebreaks("Input your commands and watch the turn play out. You can see everything your enemies will do\n\nAttacks have three elements, Fire, Thunder and Ice. Fire beats Ice, Ice beats Thunder, Thunder beats Fire.\nThe element of the attacker changes upon attacking. Attackers are immune to attacks of the same element!", 2, pos, 0, BattleRender.Colors.inputKey, TextBoard.INVISIBLECOLOR);
            explanationEntity.Origin.SetCursorAt(0, 0);
            //if (!model.battleIntroMode){
            if (false)
            {
                explanationEntity.Origin.Draw_Cursor_SmartLineBreak("Input your commands and watch the turn play out. Plan your moves based on what the enemy will do!", BattleRender.Colors.InputDescription);
                explanationEntity.Origin.CursorNewLine(0);
                explanationEntity.Origin.CursorNewLine(0);
                explanationEntity.Origin.Draw_Cursor_SmartLineBreak("Attacks have three elements, Fire, Thunder and Ice. Fire beats Ice, Ice beats Thunder, Thunder beats Fire.", BattleRender.Colors.InputDescription);
                explanationEntity.Origin.CursorNewLine(0);
                explanationEntity.Origin.CursorNewLine(0);

                explanationEntity.Origin.Draw_Cursor_SmartLineBreak("The element of the attacker changes upon attacking. Attackers are immune to attacks of the same element!", BattleRender.Colors.InputDescription);
                pos += 18;
            }
            else
            {
                pos = 5;
            }

            if (model.attackDemo)
            {
                explanationEntity.Origin.Draw_Cursor_SmartLineBreak("A battle simulation so you can examine enemy attacks beforehand", BattleRender.Colors.InputDescription);
            }
            else
            {
                explanationEntity.Origin.Draw_Cursor_SmartLineBreak("Pick your moves and execute them!", BattleRender.Colors.InputDescription);

                pos += 6;
                explanationEntity.Origin.CursorNewLine(0);
                explanationEntity.Origin.CursorNewLine(0);
                if (model.killAllEnemies)
                {
                    
                    explanationEntity.Origin.Draw_Cursor_SmartLineBreak("Defeat all enemies!", BattleRender.Colors.InputDescription);
                }
                else
                {
                    explanationEntity.Origin.Draw_Cursor_SmartLineBreak("Collect all objects", BattleRender.Colors.InputDescription);
                }

                
            }

            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            textWorld.Draw();
            

            //textWorld.mainBoard.DrawWithLinebreaks("Input your commands and watch the turn play out. You can see everything your enemies will do\n", 2, pos, 2, BattleRender.Colors.InputDescription);


            string menuTitle = "HELP";
            string leaveLabel = "EXIT";
            if (model.battleIntroMode)
            {
                leaveLabel = "START";
                menuTitle = "BATTLE INTRO";
                LeaveButton = (char)Unicode.Space;
            }
            else
            {
                LeaveButton = 'x';
            }
            if (model.attackDemo)
            {
                menuTitle = "ATTACK SIMULATION";
            }
            textWorld.mainBoard.DrawOnCenterHorizontal(menuTitle, BattleRender.Colors.WindowLabel, 0, 1);
            bool heroCommandExist = false;
            for (int i = 0; i < model.commandsHero.Count; i++)
            {
                if (CheckDrawCommand(model.commandsHero[i]))
                {
                    heroCommandExist = true;
                    break;
                }
            }
            bool enemyCommandExist = false;
            for (int i = 0; i < model.commandsEnemy.Count; i++)
            {
                if (CheckDrawCommand(model.commandsEnemy[i]))
                {
                    enemyCommandExist = true;
                    break;
                }
            }
            if (model.attackDemo)
            {
                //textWorld.mainBoard.Draw_Cursor_SmartLineBreak("A battle simulation where you can preview enemy attacks beforehand", 2, pos, 2, BattleRender.Colors.WindowLabel);
                pos += 4;
                textWorld.mainBoard.Draw("SIMULATED ATTACK", 2, pos, BattleRender.Colors.WindowLabel);
            }
            else
            {
                if (heroCommandExist && false)
                {
                    textWorld.mainBoard.Draw("YOUR COMMANDS", 2, pos - 2, BattleRender.Colors.WindowLabel);
                    List<int> commandList = model.commandsHero;
                    pos = ShowCommands(pos, commandList);
                    pos += 4;
                }
                
                if(enemyCommandExist)
                    textWorld.mainBoard.Draw("ENEMY COMMANDS", 2, pos, BattleRender.Colors.WindowLabel);
            }


            if (enemyCommandExist)
            {
                pos += 2;
                pos = ShowCommands(pos, model.commandsEnemy);
            }
                

            GameMain.DrawInput(1, pos + 3, LeaveButton, leaveLabel, textWorld.mainBoard);


        }

        private int ShowCommands(int pos, List<int> commandList)
        {
            for (int i = 0; i < commandList.Count; i++)
            {
                //Console.Write("DRAWWW");

                int command = commandList[i];
                bool drawFlag = CheckDrawCommand(command);
                if (drawFlag)
                {
                    textWorld.mainBoard.Draw(moveRenders[command].Abrev, 2, pos, BattleRender.Colors.HeroTurn);
                    textWorld.mainBoard.Draw(moveRenders[command].Label.ToUpper(), 5, pos, BattleRender.Colors.HeroTurn);
                    textWorld.mainBoard.SetCursorAt(3, pos + 1);
                    textWorld.mainBoard.Draw_Cursor_SmartLineBreak(moveRenders[command].Description, BattleRender.Colors.InputDescription, 0, moveRenders[command].Description.Length - 1, 3);
                    //textWorld.mainBoard.Draw(moveRenders[command].Description, 3, pos + 1, BattleRender.Colors.InputDescription);
                    pos += 3;
                }

            }

            return pos;
        }

        private bool IsShowingCommandInList(List<int> commandList)
        {
            for (int i = 0; i < commandList.Count; i++)
            {
                //Console.Write("DRAWWW");

                int command = commandList[i];
                if (CheckDrawCommand(command)) return true;


            }

            return false;
        }

        private bool CheckDrawCommand(int command)
        {
            bool drawFlag = false;
            if (command >= 0)
            {
                //Console.WriteLine(command);
                //Console.WriteLine();
                var md = moveDatas[command];
                if (!md.HasTag((int)MoveDataTags.Movement))
                {
                    drawFlag = moveRenders[command].Label.Length != 0;
                }


            }

            return drawFlag;
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

        internal void Show()
        {
            model.RefreshData();
        }

        internal void HelpMode()
        {
            model.battleIntroMode = false;
        }

        internal bool IsWannaShowIntro()
        {
            model.RefreshData();
            return IsShowingCommandInList(model.commandsEnemy) || IsShowingCommandInList(model.commandsHero);
            //return model.commandsEnemy.Count != 0 || model.commandsHero.Count != 0;
        }
    }

    public class HelpScreenModel
    {
        BattleMain battleMain;
        public List<int> commandsHero = new List<int>();
        public List<int> commandsEnemy = new List<int>();
        public bool battleIntroMode = false;
        public bool attackDemo;
        public bool killAllEnemies;

        public HelpScreenModel(BattleMain battleMain)
        {
            this.battleMain = battleMain;

        }

        public void RefreshData()
        {
            commandsHero.Clear();
            AddAll(battleMain.playerHandFixed, commandsHero);
            AddAll(battleMain.playerHandUnfixed, commandsHero);
            AddAll(battleMain.playerHandPool, commandsHero);
            foreach (var item in battleMain.entities)
            {
                AddAllArray(item.moves, commandsEnemy);
            }
            attackDemo = battleMain.BattleConfig.attackDemonstrationMode;
            killAllEnemies = battleMain.BattleConfig.needKillAllEnemies;
        }

        private void AddAll(List<BattleMain.MoveType> moves, List<int> commands)
        {
            foreach (var m in moves)
            {
                int a = (int)m;
                if (a < 0) continue;
                if (!commands.Contains(a))
                {
                    commands.Add(a);
                }
            }
        }

        private void AddAllArray(int[] moves, List<int> commands)
        {
            foreach (var a in moves)
            {
                if (a < 0) continue;
                if (!commands.Contains(a))
                {
                    commands.Add(a);
                }
            }
        }
    }


}
