﻿using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class EquipScreen : ITextScreen_
    {
        public AttackInventory inventory;
        private TextWorld textWorld;
        private NavigationKeyboard navKey_Inventory;
        private NavigationKeyboard navKey_Equip;
        private readonly List<MoveMetaData> moveMetaDatas;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }
        public MouseIO Mouse { set; get; }
        public KeyboardIO Keyboard { set; get; }
        int mode = 0;
        internal bool wantToReturn;

        public EquipScreen(List<MoveMetaData> moveMetaDatas)
        {
            textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            navKey_Inventory = new NavigationKeyboard(textWorld);
            navKey_Equip = new NavigationKeyboard(textWorld);
            this.moveMetaDatas = moveMetaDatas;
        }

        public void Draw(float f)
        {
            navKey_Inventory.Clear();
            navKey_Equip.Clear();
            textWorld.mainBoard.Reset();
            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, BattleRender.Colors.BackgroundInput);
            textWorld.DrawChildren();

            textWorld.mainBoard.DrawOnCenterHorizontal("ATTACK EQUIPMENT", BattleRender.Colors.Hero, 0, 1);

            for (int i = 0; i < inventory.equippedAttacks.Length; i++)
            {
                int x = 4;
                int y = i*2 + 5;
                navKey_Equip.Add(x - 2, y);
                textWorld.mainBoard.DrawOneDigit(i+1, x, y, BattleRender.Colors.InputDescription);
                textWorld.mainBoard.DrawChar('.', x+1, y, BattleRender.Colors.InputDescription);
                if (inventory.equippedAttacks[i] >= 0) { 

                    
                    textWorld.mainBoard.Draw(moveMetaDatas[inventory.equippedAttacks[i]].Label, x+3, y, BattleRender.Colors.InputDescription);
                }
                else
                {
                    textWorld.mainBoard.Draw("_____", x+3, y, BattleRender.Colors.InputDescription);
                }
            }

            int yOffset = 5 + inventory.equippedAttacks.Length * 2 + 2;

            textWorld.mainBoard.DrawHorizontalLine('_',yOffset-2, BattleRender.Colors.WindowLabel);

            for (int i = 0; i < inventory.attackInventory.Count; i++)
            {
                int x = 4;
                int y = i + yOffset;
                navKey_Inventory.Add(x - 2, y);
                textWorld.mainBoard.Draw(moveMetaDatas[inventory.attackInventory[i]].Label, x, y, BattleRender.Colors.InputDescription);
            }
            if (keyDownUnicode >= 0)
            {
                if(mode == 1)
                    navKey_Inventory.CheckInput(keyDownUnicode);
                if(mode == 0)
                    navKey_Equip.CheckInput(keyDownUnicode);
            }

            string attackDescription = "";
            int move = -1;
            if(mode == 1)
            {
                int cp = navKey_Inventory.currentPoint;
                if(cp >= 0)
                    move = inventory.attackInventory[cp];
            }
            if (mode == 0)
            {
                int cp = navKey_Equip.currentPoint;
                if(cp >= 0)
                    move = inventory.equippedAttacks[cp];
            }
            if(move >= 0){
                textWorld.mainBoard.Draw(moveMetaDatas[move].Description, 4, yOffset + 13, BattleRender.Colors.InputDescription);
                textWorld.mainBoard.Draw("Power: ", 4, yOffset + 14, BattleRender.Colors.InputDescription);
                textWorld.mainBoard.DrawTwoDigits(moveMetaDatas[move].damage, 4+"Power: ".Length, yOffset + 14, BattleRender.Colors.InputDescription);
            }
                

            navKey_Inventory.DrawControls();


            if (navKey_Inventory.chosenOption >= 0)
            {
                int attack = inventory.attackInventory[navKey_Inventory.chosenOption];
                inventory.equippedAttacks[navKey_Equip.chosenOption] = attack;
                navKey_Equip.ResetChoice();
                navKey_Inventory.ResetChoice();
                mode = 0;
                
            }
            if (navKey_Equip.chosenOption >= 0)
            {
                mode = 1;
            }
            if (navKey_Inventory.wantToEscape)
            {
                navKey_Equip.ResetChoice();
                navKey_Inventory.ResetChoice();
                mode = 0;
            }
            navKey_Inventory.ShowCursor(mode == 1);
            navKey_Equip.ShowCursor(mode == 0);
            wantToReturn = navKey_Equip.wantToEscape;
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

        internal void ResetChoice()
        {
            navKey_Equip.ResetChoice();
            wantToReturn = false;
        }
    }
}
