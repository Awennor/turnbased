﻿using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    interface ITextScreen_
    {
        void Draw(float f);
        TextBoard GetBoard();
        int Input { set; }
        int keyDownUnicode { set; }
        MouseIO Mouse { set; }
        KeyboardIO Keyboard { set; }


    }
}
