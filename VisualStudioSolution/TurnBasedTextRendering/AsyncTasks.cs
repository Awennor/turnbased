﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class AsyncTasks
    {

    }

    public class AsyncTrack
    {

    }

    public abstract class DelayedActions
    {
        List<float> times = new List<float>();
        List<IList> lists = new List<IList>();

        public void Update(float delta)
        {
            for (int i = 0; i < times.Count; i++)
            {
                times[i] -= delta;
                if (times[i] <= 0)
                {
                    Execute(i);
                    EndTask(i);
                }
            }
        }

        internal abstract void Execute(int i);

        internal void Add(float time)
        {
            times.Add(time);
        }

        public bool IsDone()
        {
            return times.Count == 0;
        }

        internal void EndTask(int i)
        {
            times.RemoveAt(i);
            foreach (var l in lists)
            {
                l.RemoveAt(i);
            }
        }
    }

    public class AsyncTaskSetter<T> : DelayedActions
    {
        List<T> ToValue = new List<T>();
        List<Action<T>> setters = new List<Action<T>>();

        public void Add(T e, Action<T> setter, float time)
        {
            ToValue.Add(e);
            setters.Add(setter);
            base.Add(time);
        }

        internal override void Execute(int i)
        {
            setters[i](ToValue[i]);
            ToValue.RemoveAt(i);
            setters.RemoveAt(i);
            
        }
    }
}
