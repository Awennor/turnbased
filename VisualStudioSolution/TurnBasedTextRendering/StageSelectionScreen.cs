﻿using Pidroh.ConsoleApp.Turnbased;
using Pidroh.ECS;
using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;

namespace Pidroh.TurnBased.TextRendering
{
    public class StageSelectionScreen : ITextScreen_
    {
        public StageDataGroup stageGroup;

        public int Input { set; get; }
        public int keyDownUnicode { set; get; }
        public MouseIO Mouse { set; get; }
        public KeyboardIO Keyboard { set; get; }
        TextWorld textWorld;
        private NavigationKeyboard navKey;
        public int chosenStage = -1;
        public bool wantToReturn;
        public string[] groupLabels = { "Sector ", "World " };

        public StageSelectionScreen()
        {
            textWorld = new TextWorld();
            textWorld.Init(GameMain.Width, GameMain.Height);
            navKey = new NavigationKeyboard(textWorld);
        }

        public void Draw(float f)
        {
            navKey.Clear();
            textWorld.mainBoard.Reset();
            textWorld.mainBoard.SetAll(' ', TextBoard.NOCHANGECOLOR, Colors.BackgroundInput);
            textWorld.DrawChildren();
            //textWorld.mainBoard.DrawOnCenterHorizontal("Controls", Colors.WindowLabel, 0, 6);
            textWorld.mainBoard.DrawOnCenterHorizontal("Stage Selection", BattleRender.Colors.Hero, 0, 1);

            int stageN = 1;
            for (int i = 0; i < stageGroup.childIds.Count; i++)
            {
                int x = 5;
                int y = 5 + i * 3;
                Entity e = stageGroup.childIds[i];
                StageProgressPersistence stageProgressPersistence = e.GetComponent<StageProgressPersistence>();
                var group = e.GetComponent<StageDataGroup>();
                string stageLabelPrefix = "Stage ";
                var tags = e.GetComponent<TagComponent>();
                if (group != null)
                {
                    stageLabelPrefix = groupLabels[group.level];
                }
                string finalLabel = "";

                if (tags != null && tags.Contains("tutorial"))
                {
                    finalLabel = "Tutorial";
                }
                else
                {
                    finalLabel = stageLabelPrefix + stageN;
                    stageN++;
                }
                if (stageProgressPersistence != null)
                    if (stageProgressPersistence.available)
                    {
                        textWorld.mainBoard.Draw(finalLabel, x, y, Colors.inputKey);

                        int rank = stageProgressPersistence.bestBattleMetrics.rank;
                        if (rank >= 0)
                        {
                            textWorld.mainBoard.Draw("Best Rank: " + rank, x + 2, y + 1, Colors.WindowLabel);
                        }
                    }
                    else
                        textWorld.mainBoard.Draw("???", x, y, Colors.WindowLabel);
                else
                {
                    textWorld.mainBoard.Draw(finalLabel, x, y, Colors.inputKey);
                }

                navKey.Add(x - 2, y);
            }
            if (keyDownUnicode >= 0)
            {
                navKey.CheckInput(keyDownUnicode);
            }

            chosenStage = navKey.chosenOption;
            if (chosenStage >= 0)
            {
                StageProgressPersistence stageProgressPersistence1 = stageGroup.childIds[chosenStage].GetComponent<StageProgressPersistence>();
                if (stageProgressPersistence1 != null)
                {
                    if (!stageProgressPersistence1.available)
                    {
                        chosenStage = -1;
                        navKey.chosenOption = -1;
                    }
                }
                else
                {

                }
            }
            navKey.DrawControls();
            wantToReturn = navKey.wantToEscape;
        }

        public TextBoard GetBoard()
        {
            return textWorld.mainBoard;
        }

        internal void ResetChoice()
        {
            chosenStage = -1;
            navKey.ResetChoice();
            wantToReturn = false;
        }

        internal void ResetCursor()
        {
            navKey.ResetCursor();
        }
    }
}
