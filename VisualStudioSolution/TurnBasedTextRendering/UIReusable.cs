﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{

    

    public class UIReusable
    {
        public static string WASD = "WASD";
        public static string arrows = "" + Unicode.Uparrow2 + Unicode.Leftarrow2 + Unicode.Downarrow2 + Unicode.Rightarrow2;
        public static void DrawControlsLabel(TextWorld textWorld, int y)
        {
            textWorld.mainBoard.DrawOnCenterHorizontal("Controls", BattleRender.Colors.WindowLabel, 0, y);

        }
    }
}
