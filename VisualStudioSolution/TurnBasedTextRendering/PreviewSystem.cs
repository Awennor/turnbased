﻿using System;
using System.Collections.Generic;
using System.Text;
using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.ECS;

namespace Pidroh.TurnBased.TextRendering
{
    public class PreviewSystem
    {
        private ECSManager ecs;
        private BattleMain battleMain;
        public bool previewActive = false;
        private ClonedState clonedState;
        private QuickAccessorOne<BattleMain.BattleEntity> battleEntity;
        Debugger debug = new Debugger(true);

        public PreviewSystem(ECSManager ecs, BattleMain battleMain)
        {
            this.ecs = ecs;
            ecs.AddCopyMethod(typeof(BattleMain.BattleEntity), (o1, o2)=> {
                var to = o2 as BattleMain.BattleEntity;
                var from = o1 as BattleMain.BattleEntity;
                to.pos = from.pos;
                to.life = from.life;
                to.maxLife = from.maxLife;
                to.element = from.element;
                for (int i = 0; i < to.moves.Length; i++)
                {
                    to.moves[i] = from.moves[i];
                }
            });
            ecs.AddCopyMethod(typeof(BattleMain.BattleState), (o1, o2) => {
                var to = o2 as BattleMain.BattleState;
                var from = o1 as BattleMain.BattleState;
                to.actingEntity = from.actingEntity;
                to.BattleEndEnable = from.BattleEndEnable;
                to.moveTick_Now = from.moveTick_Now;
                to.moveTick_Total = from.moveTick_Total;
                to.phase = from.phase;
                to.turn = from.turn;
                to.turnsPerPhase = from.turnsPerPhase;
                to.totalTurns = from.totalTurns;
            });
            ecs.AddCopyMethod(typeof(TimeStamp), (o1, o2) => {
                var to = o2 as TimeStamp;
                var from = o1 as TimeStamp;
                to.CurrentSnap = from.CurrentSnap;
            });
            ecs.AddCopyMethod(typeof(HappHandling.HappHandleState), (o1, o2) => {
                var to = o2 as HappHandling.HappHandleState;
                var from = o1 as HappHandling.HappHandleState;
                to.highestHandled = from.highestHandled;
            });
            this.battleMain = battleMain;
            clonedState = new ClonedState();
            battleEntity = ecs.QuickAccessor1<BattleMain.BattleEntity>();
        }

        internal void StartPreview()
        {
            battleMain.trackBattle.Preview();
            foreach (var item in battleMain.entities)
            {
                
                debug.Print("ALL ENTITIES BEFORE PREVIEW");
                debug.Print(item);
                debug.Print(item.randomPosition + " RANDOM POS");
                debug.Print(item.Type + " type");
                debug.Print(item.drawTurn + " draw turn");
                debug.Print("END");
            }
            ecs.CloneState(clonedState);
            battleMain.battleState.BattleEndEnable = false;
            previewActive = true;
            
            foreach (var item in battleMain.entities)
            {
                if (item.Type == BattleMain.EntityType.hero)
                {
                    item.life = 0;
                    for (int i = 0; i < item.moves.Length; i++)
                    {
                        item.moves[i] = -1;
                    }
                }
            }
            battleMain.Tick();

        }

        internal void EndPreview()
        {
            //Console.Write("End preview");
            //   Console.ReadKey();
            previewActive = false;
            //Console.WriteLine(battleMain.entities.Contains(battleEntity.Comp1(0))+"XXXS");
            
            ecs.RestoreState(clonedState);
            battleMain.battleState.phase = BattleMain.BattlePhase.PickHands;

            for (int j = 0; j < battleMain.entities.Count; j++)
            {
                BattleMain.BattleEntity item = battleMain.entities[j];
                bool exist = false;
                for (int i = 0; i < battleEntity.Count; i++)
                {
                    if (item == battleEntity.Comp1(i))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    battleMain.entities.Remove(item);
                    j--;
                }
                debug.Print("ALL ENTITIES AFTER PREVIEW");
                debug.Print(item);
                debug.Print(item.randomPosition+" RANDOM POS");
                debug.Print(item.Type + " type");
                debug.Print(item.drawTurn + " draw turn");
                debug.Print("END");
            }
        }
    }
}
