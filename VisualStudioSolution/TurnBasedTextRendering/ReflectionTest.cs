﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TurnBased.TextRendering
{
    public class ReflectionTest
    {

        public ReflectionTest() {
            var debug = new Debugger(true);
            
            BattleMain.BattleEntity be = new ConsoleApp.Turnbased.BattleMain.BattleEntity();
            var type = be.GetType();
            BattleMain.BattleEntity be2 = new BattleMain.BattleEntity();
            be2.randomPosition = true;
            debug.Print(type.GetField("randomPosition").GetValue(be2).ToString());
            debug.Print(be2.randomPosition + "");
            debug.Print(be);
            DeepCloneHelper.DeepCopyPartial(be, be2);
            DeepCloneHelper.DeepCopyPartial(be2, be);
            debug.Print(be);
            debug.Print(be2);
            

            debug.Print(type.GetField("randomPosition").GetValue(be2).ToString());
            debug.Print(be2.randomPosition+"");

        }
    }
}
