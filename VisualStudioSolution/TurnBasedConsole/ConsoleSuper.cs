﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class ConsoleSuper
{
    static public void WriteLine(string line)
    {
        Console.Write(line);
        while (true)
        {

            Console.Write(' ');
            if (Console.CursorLeft == 0) return;
        }
    }

    internal static void WriteLine_FixedWidth(string v1, int v2)
    {
        Console.Write(v1);
        int pad = v2 - v1.Length;
        for (int i = 0; i < pad; i++)
        {
            Console.Write(' ');
        }
        Console.CursorTop = Console.CursorTop + 1;
        Console.CursorLeft = 0;
    }

    internal static char GetChar_NonBlocking()
    {
        if (Console.KeyAvailable)
        {
            //Console.Clear();
            char latestKey = Console.ReadKey(true).KeyChar;
            return latestKey;
        }
        return char.MinValue;
    }

    public static bool Input(string prompt, char keyNecessary, char key_Pressed)
    {
        return Input(prompt, keyNecessary, key_Pressed, true);
    }

    public static bool Input(string prompt, char keyNecessary, char key_Pressed, bool available)
    {
        var previous = Console.ForegroundColor;
        if (!available) Console.ForegroundColor = ConsoleColor.DarkGray;
        Console.Write('[');
        Console.Write(keyNecessary);
        Console.Write(']');
        Console.Write(prompt);
        Console.ForegroundColor = previous;
        return keyNecessary == key_Pressed && available;
    }
}

public struct Point
{
    int x, y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int X { get => x; }
    public int Y { get => y; }
}

