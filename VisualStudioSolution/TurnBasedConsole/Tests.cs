﻿
using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased.Happs;
using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnBasedConsole
{
    public static class Tests
    {

        public static void TestEvent()
        {
            HappManager hm = new HappManager();
            object o = new object();
            HappHandler hh = new HappHandler(o, (h) => { Console.Write('c'); });
            hm.AddHandler(hh);
            Happ h1 = new Happ(o);
            hm.Add(h1);
            hm.TryHandle();
            hm.TryHandle();
            hm.Tick();
            hm.TryHandle();
            hm.Add(new Happ(o));
            hm.Add(new Happ(o));
            hm.TryHandle();
            hm.Tick();
            hm.TryHandle();
        }

        public static void TestAnimation()
        {
            Console.SetWindowSize(120, 40);
            ConsoleColor[] colors = Program.GetColors();

            TextWorld t = new TextWorld();
            t.Init(120, 35);
            TextEntity te = t.GetFreeEntity(2, 2);
            te.Origin.DrawChar('c', 0, 0);
            te.Origin.Position = new Vector2D(2, 2);
            var posAnim = t.AddAnimation(new PositionAnimation());
            t.Draw();
            Program.DrawTextBoard(t.mainBoard, colors);
            posAnim.Add(te.AnimBase(2f), new PositionAnimation.PositionData(new Vector2D(5, 5), new Vector2D(8, 8)));
            Console.ReadKey();
            t.Draw();
            Program.DrawTextBoard(t.mainBoard, colors);
            Console.ReadKey();
            t.AdvanceTime(1.5f);
            t.Draw();
            Program.DrawTextBoard(t.mainBoard, colors);
            Console.ReadKey();
            t.AdvanceTime(1.5f);
            t.Draw();
            Program.DrawTextBoard(t.mainBoard, colors);
        }
    }
}
