﻿using OpenTK.Graphics;
using Pidroh.ConsoleApp.Turnbased;
using SunshineConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;
using Pidroh.TextRendering;
using Pidroh.ConsoleApp.Turnbased.DebugExtra;
using static Pidroh.ConsoleApp.Turnbased.BattleRender;
using System.Drawing;
using Pidroh.BaseUtils;

namespace TurnBasedConsole
{
    class Program
    {
        private static DateTime last;

        static void Main(string[] args)
        {
            //Tests.TestEvent();
            //GameMain();
            //for (int i = 0; i < 256; i++)
            //{
            //    Console.WriteLine(i + " - " + "[" +(char)i+"]");
            //}
            SunshineMain();

            //Console.OutputEncoding = System.Text.Encoding.UTF8;
            //Colors(gr, TextBoard);
        }

        private static void SunshineMain()
        {

            GameMain gr;
            TextBoard TextBoard;
            SetupGame(out gr, out TextBoard);
            

            

            Color4[] colors = new Color4[20];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = new Color4(ColorTranslator.FromHtml(ColorStuff.colors[i]));
                
            }
            



            ConsoleWindow console = new ConsoleWindow(40, 120, "Sunshine Console Hello World");
            console.MouseDown += (b, m) =>
            {
                var pos = m.Position;
                Console.WriteLine(pos.X);
                Console.WriteLine(pos.Y);
                Console.WriteLine(pos.X * console.Cols / console.Width);
                Console.WriteLine(pos.Y * console.Rows / console.Height);
                
                //Console.WriteLine(pos.Y/40);
            };
            


            // Write to the window at row 6, column 14:
            console.Write(6, 14, "Hello World!", Color4.Lime);
            //for (int i = 0; i < 40*120-1; i++)
            //{
            //    console.Write(i/120, i % 120,(char)i, Color4.Lime);
            //}
            console.KeyDown += (o, e) =>
            {
                int scanCode = (int)e.ScanCode;

                if (e.Key <= Key.Z && e.Key >= Key.A)
                {

                    scanCode += 'a' - ((int)Key.A);
                    //Console.WriteLine(scanCode);
                }
                if (e.Key >= Key.Number0 && e.Key <= Key.Number9) 
                {
                    scanCode += '0' - ((int)Key.Number0);
                }
                if (e.Key >= Key.Keypad0 && e.Key <= Key.Keypad9)
                {
                    scanCode += '0' - ((int)Key.Keypad0);
                }
                if (e.Key == Key.Space)
                {
                    scanCode = Unicode.Space;
                }
                gr.keyDownUnicode = scanCode;
                Console.WriteLine(e.Key+"SCAN "+(char)scanCode);
            };
            while (console.WindowUpdate())
            {
                float deltaSec = UpdateGame(gr);
                gr.Input = (int)InputKey.NONE;
                gr.keyDownUnicode = -1;
                TextBoard = gr.GetBoard();

                int mouseX = console.Mouse.X* console.Cols / console.Width;
                int mouseY = console.Mouse.Y * console.Rows / console.Height;

                gr.Mouse.pos = new Point2D(mouseX, mouseY);
                //Console.WriteLine(mouseX + " "+mouseY);

                //draw board
                for (int i = 0; i < TextBoard.Width; i++)
                {
                    for (int j = 0; j < TextBoard.Height; j++)
                    {
                        console.Write(j, i, TextBoard.CharAt(i, j), colors[TextBoard.TextColor[i, j]], colors[TextBoard.BackColor[i, j]]); //UNCOMMENT

                        //Console.SetCursorPosition(i, j);
                        //Console.Write(TextBoard.CharAt(i, j));
                    }
                }

                /* WindowUpdate() does a few very important things:
                ** It renders the console to the screen;
                ** It checks for input events from the OS, such as keypresses, so that they
                **   can reach the program;
                ** It returns false if the console has been closed, so that the program
                **   can be properly ended. */
            }
        }

        private static void GameMain()
        {
            Console.SetWindowSize(120, 40);

            GameMain gr;
            TextBoard TextBoard;
            SetupGame(out gr, out TextBoard);

            ConsoleColor[] colors = GetColors();

            while (true)
            {
                //console.
                //for (char i = (char)0; i < 500; i++)
                //{
                //    console.Write(i/30, i%30, i, Color4.DodgerBlue);
                //}
                //continue;
                float deltaSec = UpdateGame(gr);
                TextBoard = gr.GetBoard();
                DrawTextBoard(TextBoard, colors);
                Console.WriteLine(deltaSec);

                //Console.Read();

                /* WindowUpdate() does a few very important things:
                ** It renders the console to the screen;
                ** It checks for input events from the OS, such as keypresses, so that they
                **   can reach the program;
                ** It returns false if the console has been closed, so that the program
                **   can be properly ended. */
            }
        }

        private static float UpdateGame(GameMain gr)
        {
            //gr.Input = ConsoleSuper.GetChar_NonBlocking();
            int milliseconds = (DateTime.Now - last).Milliseconds;
            float deltaSec = milliseconds / 1000f;
            //deltaSec = 0.001f;
            last = DateTime.Now;

            gr.Draw(deltaSec);
            return deltaSec;
        }

        private static void SetupGame(out GameMain gr, out TextBoard TextBoard)
        {
            Random rnd = new Random();
            RandomSupplier.Generate = () =>
            {
                return (float)rnd.NextDouble();
            };
            gr = new GameMain();
            TextBoard = gr.GetBoard();
        }

        public static ConsoleColor[] GetColors()
        {



            ConsoleColor[] colors = new ConsoleColor[10];
            for (int i = 0; i < colors.Length; i++)
            {
                colors[i] = ConsoleColor.Gray;
            }
            colors[BattleRender.Colors.GridHero] = ConsoleColor.DarkCyan;
            colors[BattleRender.Colors.GridEnemy] = ConsoleColor.DarkRed;
            colors[BattleRender.Colors.Hero] = ConsoleColor.Cyan;
            colors[BattleRender.Colors.Enemy] = ConsoleColor.Red;
            colors[BattleRender.Colors.HeroTurn] = ConsoleColor.DarkCyan;
            colors[BattleRender.Colors.EnemyTurn] = ConsoleColor.DarkRed;
            colors[BattleRender.Colors.Board] = ConsoleColor.DarkBlue;
            colors[BattleRender.Colors.inputKey] = ConsoleColor.DarkYellow;
            colors[BattleRender.Colors.WindowLabel] = ConsoleColor.DarkMagenta;
            colors[0] = ConsoleColor.Gray;
            return colors;
        }

        public static void DrawTextBoard(TextBoard TextBoard, ConsoleColor[] colors)
        {
            Console.SetCursorPosition(0, 0);
            //draw board0

            for (int j = 0; j < TextBoard.Height; j++)
            {
                for (int i = 0; i < TextBoard.Width; i++)
                {
                    Console.SetCursorPosition(i, j);
                    Console.ForegroundColor = colors[TextBoard.TextColor[i, j]];
                    int backColor = TextBoard.BackColor[i, j];
                    ConsoleColor consoleColor = colors[backColor];
                    if (backColor == 0) consoleColor = ConsoleColor.Black;
                    else DebugEx.Log("BACKCOLOR NOT ZERO " + backColor);
                    Console.BackgroundColor = consoleColor;
                    Console.Write(TextBoard.CharAt(i, j));
                    //console.Write(j, i, TextBoard.CharAt(i, j), colors[TextBoard.TextColor[i, j]]);
                    //Console.SetCursorPosition(i, j);
                    //Console.Write(TextBoard.CharAt(i, j));
                }
            }
        }



    }
}
