
using SadConsole;
using Console = SadConsole.Console;
using Microsoft.Xna.Framework;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.TextRendering;
using System;
using Pidroh.BaseUtils;
using System.Globalization;
using System.Threading;
using Pidroh.TurnBased.TextRendering;
using System.Collections.Generic;

namespace MyProject
{
    class Program
    {

        public const int Height = 48;
        private static Console startingConsole;
        private static GameMain gr;
        

        static void Main(string[] args)
        {
            Microsoft.Xna.Framework.Input.Keys[] specKeys = new Microsoft.Xna.Framework.Input.Keys[] {
                Microsoft.Xna.Framework.Input.Keys.Enter, Microsoft.Xna.Framework.Input.Keys.Escape,
                Microsoft.Xna.Framework.Input.Keys.Back
            };
            List<Microsoft.Xna.Framework.Input.Keys> specialKeys = new List<Microsoft.Xna.Framework.Input.Keys>(specKeys);
            Debugger.ActiveStatic = false;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
            Microsoft.Xna.Framework.Color[] colors = new Microsoft.Xna.Framework.Color[25];
            for (int i = 0; i < colors.Length; i++)
            {
                System.Drawing.Color color = System.Drawing.ColorTranslator.FromHtml(ColorStuff.colors[i]);
                colors[i] = new Microsoft.Xna.Framework.Color(color.R, color.G, color.B);

            }

            new ReflectionTest();

            
            TextBoard TextBoard;
            SetupGame(out gr, out TextBoard);
            int Width = GameMain.Width;
            // Setup the engine and creat the main window.
            SadConsole.Game.Create("C64.font", Width, Height);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            BattleMain.BattleEntity from = new BattleMain.BattleEntity();
            from.maxLife = 12;
            BattleMain.BattleEntity to = new BattleMain.BattleEntity();
            DeepCloneHelper.DeepCopyPartial(from, to);
            System.Console.WriteLine(to.maxLife);
            System.Console.WriteLine(from.maxLife);
            System.Console.WriteLine(to == from);
            to.maxLife = 8;
            DeepCloneHelper.DeepCopyPartial(to, from);
            System.Console.WriteLine(from.maxLife);

            // Hook the update event that happens each frame so we can trap keys and respond.
            //SadConsole.Game.OnUpdate = Update;



            SadConsole.Game.OnUpdate += (GameTime t) => 
            {
                gr.Draw((float)t.ElapsedGameTime.TotalSeconds);
                TextBoard = gr.GetBoard();
                //System.Console.WriteLine((float)t.ElapsedGameTime.TotalSeconds);
                gr.Keyboard.downUnicodes.Clear();
                foreach (var item in SadConsole.Global.KeyboardState.KeysDown)
                {
                    var key = item.Character;
                    if (specialKeys.Contains(item.Key))
                    {
                        key = (char)item.Key;
                    }
                    gr.Keyboard.downUnicodes.Add(key);
                }
                if (SadConsole.Global.KeyboardState.KeysPressed.Count > 0)
                {
                    var key = SadConsole.Global.KeyboardState.KeysPressed[0].Character;
                    if (specialKeys.Contains(SadConsole.Global.KeyboardState.KeysPressed[0].Key)){
                        key = (char)SadConsole.Global.KeyboardState.KeysPressed[0].Key;
                    }
                    //if (SadConsole.Global.KeyboardState.KeysPressed[0].Key == Microsoft.Xna.Framework.Input.Keys.Escape)
                    //{
                    //    key = (char)SadConsole.Global.KeyboardState.KeysPressed[0].Key;
                    //}
                    //if (SadConsole.Global.KeyboardState.KeysPressed[0].Key == Microsoft.Xna.Framework.Input.Keys.Enter)
                    //{
                    //    key = (char)SadConsole.Global.KeyboardState.KeysPressed[0].Key;
                    //}
                    gr.keyDownUnicode = key;
                    System.Console.WriteLine("cjarrrx "+ key);
                    System.Console.WriteLine("cjarrr " + (int)key);
                }
                else
                {
                    gr.keyDownUnicode = -1;
                }
                
                //var t2 = t;
                
                //gr.Draw(t.ElapsedGameTime.);
                if (SadConsole.Global.KeyboardState.IsKeyReleased(Microsoft.Xna.Framework.Input.Keys.F5))
                {
                    SadConsole.Settings.ToggleFullScreen();
                }

                for (int i = 0; i < TextBoard.Width; i++)
                {
                    for (int j = 0; j < TextBoard.Height; j++)
                    {
                        if (startingConsole.Width <= i)
                        {
                            System.Console.WriteLine("overflow");
                            continue;
                        }
                        if (startingConsole.Height <= j)
                        {
                            System.Console.WriteLine("overflow");
                            continue;
                        }
                        string text = TextBoard.CharAt(i, j) + "";
                        Color foreground = colors[TextBoard.TextColor[i, j]];
                        Color background = colors[TextBoard.BackColor[i, j]];
                        startingConsole.Print(i, j, text,
                            foreground,
                            background);
                        
                        //console.Write(j, i, TextBoard.CharAt(i, j), colors[TextBoard.TextColor[i, j]], colors[TextBoard.BackColor[i, j]]); //UNCOMMENT

                        //Console.SetCursorPosition(i, j);
                        //Console.Write(TextBoard.CharAt(i, j));
                    }
                }
            };


            // Start the game.
            SadConsole.Game.Instance.Run();

            //
            // Code here will not run until the game window closes.
            //

            SadConsole.Game.Instance.Dispose();
        }
        
        private static void Update(GameTime time)
        {
            // Called each logic update.

            // As an example, we'll use the F5 key to make the game full screen
            if (SadConsole.Global.KeyboardState.IsKeyReleased(Microsoft.Xna.Framework.Input.Keys.F5))
            {
                SadConsole.Settings.ToggleFullScreen();
            }



        }

        private static void Init()
        {
            // Any custom loading and prep. We will use a sample console for now

            startingConsole = new Console(GameMain.Width, Height);
            //startingConsole.FillWithRandomGarbage();
            startingConsole.Fill(new Rectangle(3, 3, 27, 5), null, Color.Black, 0);
            startingConsole.Print(6, 5, "Hello from SadConsole", ColorAnsi.CyanBright);
            

            // Set our new console as the thing to render and process
            SadConsole.Global.CurrentScreen = startingConsole;

            startingConsole.MouseMove += (sender, argus) =>
            {
                var consolePos = argus.MouseState.ConsolePosition;
                gr.Mouse.pos = new Point2D(consolePos.X, consolePos.Y);
            };
        }

        private static void SetupGame(out GameMain gr, out TextBoard TextBoard)
        {
            Random rnd = new Random();
            RandomSupplier.Generate = () =>
            {
                return (float)rnd.NextDouble();
            };
            gr = new GameMain();
            TextBoard = gr.GetBoard();
        }
    }
}
