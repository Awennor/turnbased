﻿using System;
using System.Collections.Generic;

namespace Pidroh.ConsoleApp.Turnbased.DebugExtra
{
    public static class DebugEx
    {
        static List<string> messages = new List<string>();

        public static void Log(string v)
        {
            messages.Add(v);
        }

        public static void Show()
        {
            Console.Clear();
            foreach (var item in messages)
            {
                Console.WriteLine(item);
                
            }
            Console.Read();
        }
    }
}
