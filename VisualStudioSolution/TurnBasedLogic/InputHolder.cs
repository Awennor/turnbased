﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class InputHolder
    {
        public List<InputUnit> inputs = new List<InputUnit>();
        List<InputTags> tags = new List<InputTags>();
        public InputUnit inputForConfirmation;

        internal void Clear()
        {
            inputs.Clear();
            tags.Clear();
        }

        internal void Add(InputUnit input, InputTags tag)
        {
            inputs.Add(input);
            tags.Add(tag);

        }

        public bool TagIs(int i2, InputTags tag)
        {
            if (tags.Count <= i2) return false;
            return tags[i2] == tag;
        }

        public bool Contains(InputUnit key)
        {
            foreach (var i in inputs)
            {
                if (i.arg1 == key.arg1 && i.type == key.type)
                    return true;
            }
            return false;
        }
    }

    public enum InputTags{
        NONE, MOVEFIX, MOVEUNFIX, MISC
    }
}
