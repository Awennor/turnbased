﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{

    public class StageDataCreator
    {
        //public List<StageData> stages = new List<StageData>();
        private readonly ECSManager ecs;
        public StageDataGroup currentGroup;

        public StageDataCreator(ECSManager ecs)
        {
            this.ecs = ecs;

            StageData.ResetIdCreation();

        }

        public void CreateDefaultStageData()
        {
            TutorialGroup();

            currentGroup = new StageDataGroup();
            AddGroup(currentGroup);

            Add(

                new StageData(
                Enemy(1, new BaseUtils.Vector2D(3, 2)),
                Enemy(2, new BaseUtils.Vector2D(5, 1))
                )
                ,
                 new StageData(
                Enemy(0, new BaseUtils.Vector2D(4, 1)),
                Enemy(1, new BaseUtils.Vector2D(5, 1))
                )
                ,
                new StageData(
                Enemy(0, new BaseUtils.Vector2D(3, 2)),
                Enemy(2, new BaseUtils.Vector2D(3, 1)),
                Enemy(2, new BaseUtils.Vector2D(5, 1))
                )
                ,
                new StageData(
                    new BattleConfig(new int[] { 1 }),
                    Enemy(3, new BaseUtils.Vector2D(4, 1))
                )

                //,

                //,
                //new StageData(
                //new EnemySpawnData(1, new BaseUtils.Vector2D(4, 1)),
                //new EnemySpawnData(1, new BaseUtils.Vector2D(5, 1)))
                );

            currentGroup = new StageDataGroup();
            AddGroup(currentGroup);

            Add(


                 new StageData(
                Enemy(8, new BaseUtils.Vector2D(3, 1))
                )
                ,
                new StageData(
                Enemy(8, new BaseUtils.Vector2D(4, 1)),
                Enemy(5, new BaseUtils.Vector2D(5, 1))
                )
                ,
                new StageData(
                Enemy(8, new BaseUtils.Vector2D(4, 2)),
                Enemy(2, new BaseUtils.Vector2D(3, 0))
                )
                ,
                new StageData(
                Enemy(8, new BaseUtils.Vector2D(3, 0)),
                Enemy(1, new BaseUtils.Vector2D(4, 2)),
                Enemy(1, new BaseUtils.Vector2D(5, 1))
                )
                ,
                new StageData(
                    this.Neutral(0, new BaseUtils.Vector2D(1, 2)),
                Enemy(9, new BaseUtils.Vector2D(3, 0)),
                Enemy(10, new BaseUtils.Vector2D(5, 2))
                )


                );
        }

        public Entity TutorialGroup()
        {
            StageDataGroup trainingGroup = new StageDataGroup();
            currentGroup = trainingGroup;
            var eTuto = AddGroup(trainingGroup);
            eTuto.AddComponent(new TagComponent("tutorial"));

            AddStage(new StageData(
                //Enemy(0, new BaseUtils.Vector2D(4, 0)),
                new BattleConfig(needKillAllEnemies: false).NoIntermission(),
                Pickup(0, new BaseUtils.Vector2D(0, 0)),
                Pickup(0, new BaseUtils.Vector2D(2, 2))
                ).HideLifeUI(), new FixedAttackStage());

            AddStage(new StageData(
                //Enemy(0, new BaseUtils.Vector2D(4, 0)),
                new BattleConfig(needKillAllEnemies: false).NoIntermission(),
                Pickup(0, new BaseUtils.Vector2D(2, 1)),
                Pickup(0, new BaseUtils.Vector2D(0, 2)),
                Enemy(4, new BaseUtils.Vector2D(5, 1))
                ).HideLifeUI(), new FixedAttackStage());
            AddStage(new StageData(
                //Enemy(0, new BaseUtils.Vector2D(4, 0)),
                new BattleConfig(needKillAllEnemies: false).NoIntermission(),
                Pickup(0, new BaseUtils.Vector2D(2, 2)),
                Pickup(0, new BaseUtils.Vector2D(1, 2)),
                Pickup(0, new BaseUtils.Vector2D(0, 2)),
                Enemy(5, new BaseUtils.Vector2D(5, 2))
                ), new FixedAttackStage());
            AddStage(new StageData(
                new BattleConfig(needKillAllEnemies: true).NoIntermission(),
                //Enemy(0, new BaseUtils.Vector2D(4, 0)),
                Enemy(6, new BaseUtils.Vector2D(5, 0))
                ), new FixedAttackStage(
                    (int)BattleMain.MoveType.Fire), new TrackInfoStage(2));
            AddStage(new StageData(
                new BattleConfig(needKillAllEnemies: true).NoIntermission(),
               //Enemy(0, new BaseUtils.Vector2D(4, 0)),
               Enemy(4, new BaseUtils.Vector2D(4, 1))
               ), new FixedAttackStage(
                   (int)BattleMain.MoveType.Fire), new TrackInfoStage(2));
            AddStage(new StageData(
                new BattleConfig(needKillAllEnemies: true).NoIntermission(),
               //Enemy(0, new BaseUtils.Vector2D(4, 0)),
               Enemy(5, new BaseUtils.Vector2D(4, 1))
               ), new FixedAttackStage(
                   (int)BattleMain.MoveType.Fire));
            AddStage(new StageData(
                new BattleConfig(needKillAllEnemies: true).NoIntermission(),
              //Enemy(0, new BaseUtils.Vector2D(4, 0)),
              Enemy(5, new BaseUtils.Vector2D(4, 1)),
              Enemy(7, new BaseUtils.Vector2D(3, 0))
              ), new FixedAttackStage(
                  (int)BattleMain.MoveType.Fire, (int)BattleMain.MoveType.Thunder));

            Add(new StageData(
                new BattleConfig(needKillAllEnemies: true).NoIntermission(),
                Enemy(0, new BaseUtils.Vector2D(3, 0)),
                Enemy(0, new BaseUtils.Vector2D(5, 2))
                ));
            return eTuto;
        }

        public void InitializeProgress(List<StageProgressPersistence> persistentList)
        {
            var stageDatas = ecs.QuickAccessor1<StageData>();
            var stageGroups = ecs.QuickAccessor1<StageDataGroup>();

            for (int i = 0; i < stageDatas.Count; i++)
            {
                bool locked = true;
                for (int j = 0; j < stageGroups.Count; j++)
                {
                    if (stageGroups.Comp1(j).childIds[0].Equals(stageDatas.Entity(i)))
                    {
                        locked = false;
                        break;
                    }

                }

                StageProgressPersistence comp = null;
                int id = stageDatas.Comp1(i).id;
                foreach (var per in persistentList)
                {
                    if (per.stageId == id)
                    {
                        comp = per;
                        break;
                    }
                }

                if (comp == null)
                {
                    //if (true) { 
                    comp = new StageProgressPersistence(id);
                    comp.available = !locked;
                    persistentList.Add(comp);
                }
                else
                {
                    //Console.Write("s");
                }
                stageDatas.Entity(i).AddComponent(comp);
            }
        }

        public void CalculateTrackInfo(List<EnemyData> enemies)
        {
            var stageDatas = ecs.QuickAccessor1<StageData>();
            for (int i = 0; i < stageDatas.Count; i++)
            {
                var sd = stageDatas.Comp1(i);

                var spawns = sd.spawnDatas;
                int totalHP = 0;
                foreach (var item in spawns)
                {
                    if (item.entityType == (int)BattleMain.EntityType.enemy)
                    {
                        var enemyId = item.id;
                        int hp = enemies[enemyId].hp;
                        totalHP += hp;
                    }
                }
                int target = 1;
                if (totalHP == 0)
                {
                    target = 2;
                }
                if (totalHP > 3)
                {
                    //target = ((totalHP - 1) / 2) + 1;
                    target = (int)Math.Ceiling(totalHP / 3f);
                }

                if (sd.battleConfig != null && !sd.battleConfig.needKillAllEnemies)
                {
                    target = 2;
                }

                Entity entity = stageDatas.Entity(i);
                var trackInfoStae = entity.GetComponent<TrackInfoStage>();
                if (trackInfoStae == null)
                    entity.AddComponent(new TrackInfoStage(target));
            }
        }

        public void AddStage(params object[] comps)
        {

            var e = ecs.CreateEntity();
            foreach (var item in comps)
            {
                e.AddComponent(item);
            }

            currentGroup.childIds.Add(e);
        }

        public Entity AddGroup(StageDataGroup group)
        {

            var e = ecs.CreateEntity();
            e.AddComponent(group);
            return e;
        }


        public SpawnData Pickup(int v, Vector2D vector2D)
        {
            return new SpawnData(v, vector2D, (int)BattleMain.EntityType.pickup);
        }

        public SpawnData Neutral(int v, Vector2D vector2D)
        {
            return new SpawnData(v, vector2D, (int)BattleMain.EntityType.neutral);
        }

        public SpawnData Enemy(int v, Vector2D vector2D)
        {
            return new SpawnData(v, vector2D, (int)BattleMain.EntityType.enemy);
        }

        public void Add(params StageData[] stageData1)
        {
            foreach (var item in stageData1)
            {

                currentGroup.childIds.Add(ecs.CreateEntityWithComponent(item));
            }
            //stages.AddRange(stageData1);
        }
    }

    public class StageData
    {
        public List<SpawnData> spawnDatas = new List<SpawnData>();
        public BattleConfig battleConfig;
        public bool hideLifeUI = false;
        static int staticIdHelper = -1;
        public int id = -1;

        public StageData()
        {
            staticIdHelper++;
            id = staticIdHelper;
        }

        public StageData(params SpawnData[] spawns) : this()
        {
            this.spawnDatas.AddRange(spawns);
        }

        public StageData(BattleConfig battleConfig, params SpawnData[] spawns) : this()
        {
            this.spawnDatas.AddRange(spawns);
            this.battleConfig = battleConfig;
        }

        public StageData HideLifeUI()
        {
            hideLifeUI = true;
            return this;
        }

        internal static void ResetIdCreation()
        {
            staticIdHelper = -1;
        }
    }

    /*
     * Has been expanded to also be able to contain other stage data groups
     * 
     */
    public class StageDataGroup
    {
        //can be stage data but also be stage data group
        public List<Entity> childIds = new List<Entity>();
        public int level;
    }

    public class TrackInfoStage
    {
        public readonly int targetRounds = 0;

        public TrackInfoStage()
        {

        }

        public TrackInfoStage(int targetTurns)
        {
            this.targetRounds = targetTurns;
        }
    }

    public class FixedAttackStage
    {
        public List<int> moves = new List<int>();


        public FixedAttackStage(int move)
        {
            moves.Add(move);
        }

        public FixedAttackStage(params int[] move)
        {
            moves.AddRange(move);
        }

        public FixedAttackStage()
        {
        }
    }

    public class AttackPrize
    {
        public int[] prizes;

        public AttackPrize(int attack)
        {
            prizes = new int[1];
            prizes[0] = attack;
        }

        public AttackPrize()
        {
        }
    }

    public class CurrencyPrize
    {
        public int[] currency;

        public CurrencyPrize(int attack)
        {
            currency = new int[1];
            currency[0] = attack;
        }

        public CurrencyPrize()
        {
        }
    }


    public class SpawnData
    {
        public readonly int id;
        public readonly int entityType;
        public readonly BaseUtils.Vector2D position;

        public SpawnData()
        {
        }

        public SpawnData(int id, Vector2D position, int type)
        {
            this.id = id;
            this.position = position;
            this.entityType = type;
        }
    }
}
