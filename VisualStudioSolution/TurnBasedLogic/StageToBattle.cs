﻿using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class StageToBattle
    {
        public Entity stageToUnlock;
        public Entity CurrentStage;

        public void Victory()
        {
            if(stageToUnlock.Exist())
                stageToUnlock.GetComponent<StageProgressPersistence>().available = true;
        }

        public void BattleMetricsUpdate(StageBattleMetrics metrics)
        {
            StageProgressPersistence stageProgressPersistence = CurrentStage.GetComponent<StageProgressPersistence>();
            stageProgressPersistence.bestBattleMetrics.CopyValues(metrics);
        }
    }
}
