﻿using Pidroh.ConsoleApp.Turnbased.Happs;
using System;
using System.Collections.Generic;
using System.Text;
using Pidroh.ConsoleApp.Turnbased;
using Pidroh.BaseUtils;
using Pidroh.ECS;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class MoveDataExecuter
    {
        BattleMain battleMain;
        List<MoveData> moveDatas;
        private HappManager happManager;
        private List<BattleMain.BattleEntity> entities;
        private readonly ECSManager ecs;
        TimeStamp timeStamp;
        List<Vector2D> aux = new List<Vector2D>();
        internal TrackBattle trackBattle;

        public MoveDataExecuter(BattleMain turnBase, List<MoveData> moveDatas, ECSManager ecs, TimeStamp timeStamp)
        {
            this.battleMain = turnBase;
            this.moveDatas = moveDatas;
            this.ecs = ecs;
            this.timeStamp = timeStamp;
        }

        public void ExecuteMove(BattleMain.BattleEntity actor, int turn)
        {


            var battleState = this.battleMain.battleState;
            entities = this.battleMain.entities;
            int userId = entities.IndexOf(actor);

            var moveId = actor.moves[turn];
            if (moveId < 0) return;
            var md = moveDatas[moveId];
            if (md == null) return;
            if (md.units.Count == 0) return;
            battleState.moveTick_Total = md.units.Count;
            int moveTick = battleState.moveTick_Now;
            var actions = md.units[moveTick].thingsToHappen;
            happManager = battleMain.happManager;



            foreach (var a in actions)
            {

                if (a is MoveAction)
                {
                    MoveAction ma = a as MoveAction;
                    var p = ma.distance;
                    actor.pos += p;
                    bool invalidMove =
                        actor.pos.X < actor.minPos.X
                        || actor.pos.Y < actor.minPos.Y
                        || actor.pos.Y > actor.maxPos.Y
                        || actor.pos.X > actor.maxPos.X;
                    foreach (var e in entities)
                    {
                        if (e != actor && e.Alive)
                        {
                            if (actor.pos == e.pos)
                            {
                                invalidMove = true;
                                if (e.Type == BattleMain.EntityType.pickup)
                                {
                                    e.life = 0;
                                    actor.damageMultiplier = 2;
                                    invalidMove = false;
                                }
                                if (e.Type == BattleMain.EntityType.paneleffect)
                                {
                                    invalidMove = false;
                                }
                                if (invalidMove) break;


                            }
                        }
                    }
                    if (invalidMove)
                    {
                        //Console.WriteLine("Invalid move generate" + battleState.moveTick_Now.Val);

                        int actorId = entities.IndexOf(actor);
                        Vector2D moveTo = actor.pos;
                        Vector2D moveFrom = actor.pos - p;
                        CreateHapp((int)MoveDataTags.Movement, new HappMoveData(actorId), new HappMovement(moveFrom, moveTo, false));


                        //battleMain.happManager
                        //    .Add(new Happ(BattleMain.HappTag.MovementFail))
                        //    .AddAttribute(new Happ.Attribute().SetValue(actorId))
                        //    .AddAttribute(new Happ.Attribute().SetValue(actor.pos.X))
                        //    .AddAttribute(new Happ.Attribute().SetValue(actor.pos.Y));

                        //battleState.moveTick_Total = 1;
                        actor.pos -= p;
                    }
                    else
                    {
                        int actorId = entities.IndexOf(actor);
                        Vector2D moveTo = actor.pos;
                        Vector2D moveFrom = actor.pos - p;
                        CreateHapp((int)MoveDataTags.Movement, new HappMoveData(actorId), new HappMovement(moveFrom, moveTo, true));
                        //Console.WriteLine("MOVE HAPP SUCCESS");
                    }
                }
                if (a is DealDamageAction)
                {
                    var dda = a as DealDamageAction;
                    var attackElement = dda.element;
                    
                    if (dda.target == Target.Area)
                    {
                        var area = dda.area;
                        var referenceUserOfArea = ResolveTarget(actor, entities, area.target, new Vector2D(0,0));
                        int mirroringX = 1;
                        if (actor.Type == BattleMain.EntityType.enemy) //enemies act on opposite side
                        {
                            mirroringX = -1;
                        }
                        foreach (var point in area.points)
                        {
                            var searchPos = point * new BaseUtils.Vector2D(mirroringX, 1) + referenceUserOfArea.pos;
                            //Console.WriteLine("Search point "+searchPos);
                            for (int i = 0; i < entities.Count; i++)
                            {
                                if (entities[i].pos == searchPos && entities[i].Alive)
                                {
                                    
                                    DealDamage(actor, dda, entities[i]);
                                }
                            }
                        }

                    }
                    else
                    {
                        //find target
                        BattleMain.BattleEntity target = ResolveTarget(actor, entities, dda.target, dda.userRefOffset);
                        if (target != null)
                        {
                            DealDamage(actor, dda, target);

                        }
                    }
                }
                if (a is SummonEntity)
                {
                    var entities = battleMain.entities;
                    int battlers = 0;
                    foreach (var item in entities)
                    {
                        if (item.drawLife && item.life >0)
                        {
                            battlers++;
                        }
                    }
                    if (battlers < 4) {
                        var se = a as SummonEntity;
                        var enemyWhich = se.enemyWhich;
                        var enemyId = battleMain.BattleConfig.enemiesToSummon[enemyWhich];

                        var positions = GetEmptySpots(side: 1);
                        if (positions.Count == 0) return;

                        Vector2D summonPos = se.preferentialRowColumn;
                        if (!positions.Contains(summonPos))
                        {
                            summonPos = positions[0];
                        }
                        ecs.CreateEntityWithComponent(new SpawnData(enemyId, summonPos, (int)BattleMain.EntityType.enemy));
                    }
                    
                    
                }
                if (a is Animation)
                {
                    var anim = a as Animation;
                    BattleMain.BattleEntity target = ResolveTarget(actor, entities, anim.target, anim.userRefOffset);
                    var area = anim.area;
                    HappArea happArea = null;
                    if (area != null)
                    {
                        var referenceUserOfArea = ResolveTarget(actor, entities, area.target, Vector2D.Zero);

                        int mirroringX = 1;
                        if (actor.Type == BattleMain.EntityType.enemy) //enemies act on opposite side
                        {
                            mirroringX = -1;
                        }
                        happArea = new HappArea(area, referenceUserOfArea.pos, mirroringX);
                    }
                    int targetId = -1;
                    if (target != null) 
                        targetId = entities.IndexOf(target);
                    CreateHapp(md, happArea, new HappMoveData(userId, targetId, anim.element));
                    if (anim.target != Target.None)
                    {
                        Vector2D initialPos = actor.pos + anim.userRefOffset;
                        Vector2D finalPos;
                        if (target != null)
                        {
                            finalPos = target.pos;
                        }
                        else{
                            finalPos = initialPos;
                            finalPos.X = 7;
                            if (actor.Type == BattleMain.EntityType.enemy) //enemies act on opposite side
                            {
                                finalPos.X = -1;
                            }
                        }
                        CreateHapp((int)MoveDataTags.Shoot, new HappShoot(initialPos, finalPos), new HappMoveData(userId, targetId, anim.element));
                    }

                        

                    //if (anim.target != Target.None)
                    //{
                    //    happManager
                    //    .Add(new Happ(BattleMain.HappTag.AttackHit))
                    //    .AddAttribute(new Happ.Attribute().SetValue(entities.IndexOf(target)))
                    //    .AddAttribute(new Happ.Attribute().SetValue(userId))
                    //    .AddAttribute(new Happ.Attribute().SetValue((int)anim.element));
                    //}


                }
            }

            if (moveTick == md.units.Count - 1)
            {
                foreach (var item in md.units)
                {
                    foreach (var act in item.thingsToHappen)
                    {
                        if (act is DealDamageAction)
                        {
                            ChangeElement(actor, (act as DealDamageAction).element);
                        }
                    }
                }

            }
        }

        private List<Vector2D> GetEmptySpots(int side = -1)
        {
            aux.Clear();
            int offX = 0;
            if (side == 1) offX = 3;
            int width = battleMain.BoardWidth / 2;
            if (side == -1)
                width = battleMain.BoardWidth;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < battleMain.BoardHeight; j++)
                {

                    aux.Add(new Vector2D(i+offX,j));
                }
            }
            var entities = battleMain.entities;
            foreach (var e in entities)
            {
                if (e.Alive && aux.Contains(e.pos))
                {
                    aux.Remove(e.pos);
                }
            }
            return aux;
            
        }

        private void ChangeElement(BattleMain.BattleEntity actor, BattleMain.Element element)
        {
            if (actor.element == element) return;
            actor.element = element;
            var th = new HappTags((int)MiscHappTags.ChangeElement);
            ecs.CreateEntityWithComponent(th, new HappMoveData(entities.IndexOf(actor), -1, element)).AddComponent(timeStamp.GetSnap());
        }

        private void CreateHapp(MoveData md, object comp1, object comp2)
        {
            var th = new HappTags(md.tags);
            var e = ecs.CreateEntityWithComponent(th, timeStamp.GetSnap());
            if (comp1 != null) e.AddComponent(comp1);
            if (comp2 != null) e.AddComponent(comp2);
        }

        private void CreateHapp(int tag, object comp1, object comp2)
        {
            var th = new HappTags(tag);
            var e = ecs.CreateEntityWithComponent(th, timeStamp.GetSnap());
            if (comp1 != null) e.AddComponent(comp1);
            if (comp2 != null) e.AddComponent(comp2);
        }

        private void DealDamage(BattleMain.BattleEntity actor, DealDamageAction dda, BattleMain.BattleEntity target)
        {
            BattleMain.Element attackElement = dda.element;
            bool elementalBlock = attackElement == target.element && attackElement != BattleMain.Element.Nothingness;
            bool superEffective = false;
            int damage = 0;
            int targetId = entities.IndexOf(target);
            if (elementalBlock)
            {
            }
            {
                if (!elementalBlock)
                {
                    var mul = battleMain.CalculateAttackMultiplier(actor);
                    mul *= battleMain.CalculateDefenderMultiplier(target);
                    if (attackElement == BattleMain.Element.Fire && target.element == BattleMain.Element.Ice
                        || attackElement == BattleMain.Element.Thunder && target.element == BattleMain.Element.Fire
                        || attackElement == BattleMain.Element.Ice && target.element == BattleMain.Element.Thunder)
                    {
                        mul *= 3;
                        superEffective = true;
                    }

                    

                    damage = dda.damage * (int)mul;
                    target.life -= damage;
                    if (target.Type == BattleMain.EntityType.hero)
                        trackBattle.HeroDamage(damage);
                    
                    actor.damageMultiplier = 1;
                    
                    happManager.Add(new Happ(BattleMain.HappTag.DamageTaken))
                    .AddAttribute(new Happ.Attribute().SetValue(targetId));
                }
            }
            this.CreateHapp((int)MiscHappTags.Damage, new HappDamageData(target.element, dda.element, entities.IndexOf(target), damage, superEffective, elementalBlock), null);
            if (target.life <= 0 && !superEffective)
            {
                CreateHapp((int)MiscHappTags.Death, new HappMoveData(targetId), null);
            }
        }

        private static BattleMain.BattleEntity ResolveTarget(BattleMain.BattleEntity actor, List<BattleMain.BattleEntity> entities, Target targetType, Vector2D offsetReferenceUserPosition)
        {
            if (targetType == Target.Self) return actor;
            BattleMain.BattleEntity target = null;
            float minDis = 10;
            Vector2D referenceUserPosition = actor.pos;
            referenceUserPosition += offsetReferenceUserPosition;
            foreach (var e2 in entities)
            {

                if (e2.Dead) continue;
                if (actor.Type != e2.Type
                    && e2.Type != BattleMain.EntityType.paneleffect
                    && e2.Type != BattleMain.EntityType.pickup)
                {
                    
                    bool sameHeight = referenceUserPosition.Y == e2.pos.Y;

                    if (sameHeight)
                    {
                        float dis = referenceUserPosition.X - e2.pos.X;
                        if (dis < 0) dis *= -1;
                        if (dis < minDis)
                        {
                            minDis = dis;
                            target = e2;
                        }

                    }
                }
            }

            return target;
        }
    }

    public class HappTags
    {
        public List<int> tags = new List<int>();

        public HappTags(List<int> tags)
        {
            this.tags.AddRange(tags);
        }

        public HappTags(int i)
        {
            tags.Add(i);
        }

        public HappTags()
        {
            
        }
    }

    public enum MiscHappTags{
        ChangeElement = 500,
        Damage = 501,
        Death = 502
    }

    public class HappDamageData
    {
        public readonly BattleMain.Element targetE, damageE;
        public readonly int target;
        public readonly int amount;
        public readonly bool superEffective;
        public readonly bool elementalBlock;

        public HappDamageData()
        {
        }

        public HappDamageData(BattleMain.Element targetE, BattleMain.Element damageE, int target, int amount, bool superEffective, bool elementalBlock)
        {
            this.targetE = targetE;
            this.damageE = damageE;
            this.target = target;
            this.amount = amount;
            this.superEffective = superEffective;
            this.elementalBlock = elementalBlock;
        }
    }

    public class HappMoveData
    {
        public readonly int user;
        public readonly int target = -1;
        public readonly BattleMain.Element element = BattleMain.Element.Nothingness;

        public HappMoveData()
        {
        }

        public HappMoveData(int user)
        {
            this.user = user;
        }

        public HappMoveData(int user, int target, BattleMain.Element element)
        {
            this.user = user;
            this.target = target;
            this.element = element;
        }
    }

    

    public class HappMovement
    {
        public readonly Vector2D moveFrom;
        public readonly Vector2D moveTo;
        public readonly bool success;


        public HappMovement()
        {
        }

        public HappMovement(Vector2D moveFrom, Vector2D moveTo, bool success)
        {
            this.moveFrom = moveFrom;
            this.moveTo = moveTo;
            this.success = success;
        }
    }

    public class HappArea
    {
        public readonly Area area;
        public Vector2D offset = new Vector2D();
        public readonly int mirroringX;

        public HappArea(Area area)
        {
            this.area = area;
        }

        public HappArea()
        {
        }

        public HappArea(Area area, Vector2D offset, int mirroringX)
        {
            this.area = area;
            this.offset = offset;
            this.mirroringX = mirroringX;
        }
    }

    public class HappShoot
    {
        public readonly Vector2D start;
        public readonly Vector2D end;

        public HappShoot()
        {
        }

        public HappShoot(Vector2D start, Vector2D end)
        {
            this.start = start;
            this.end = end;
        }
    }

}
