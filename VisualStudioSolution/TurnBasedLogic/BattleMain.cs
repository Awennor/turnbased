﻿using Pidroh.BaseUtils;
using Pidroh.ConsoleApp.Turnbased.Happs;
using Pidroh.ECS;
using System;
using System.Collections.Generic;

namespace Pidroh.ConsoleApp.Turnbased
{


    public class BattleMain
    {
        public DeckManager deckManager;
        public List<BattleEntity> entities = new List<BattleEntity>();
        public BattleState battleState = new BattleState();
        public HappManager happManager = new HappManager();
        Dictionary<MoveType, Vector2D> movementMoves = new Dictionary<MoveType, Vector2D>();
        //Dictionary<MoveType, Point> attackMoves = new Dictionary<MoveType, Point>();
        MoveType[] enemyMoves;
        //public List<Input> inputs = new List<Turnbased.Input>();
        public InputHolder inputs = new InputHolder();
        public List<MoveType> playerHandFixed = new List<MoveType>();
        public List<MoveType> playerHandUnfixed = new List<MoveType>();
        public List<MoveType> playerHandPool = new List<MoveType>();

        public float timeToChooseMax = 15f;
        public float timeToChoose = -1;

        public BattleResult battleResult = new BattleResult();

        internal void BattleConfigure(BattleConfig battleConfig)
        {
            if (battleConfig == null)
            {
                battleConfig = new BattleConfig(needKillAllEnemies: true);
            }
            this.BattleConfig = battleConfig;
            battleState.turnsPerPhase.Val = 3;

        }
        public MoveDataExecuter MoveDataExecuter;
        private readonly TimeStamp timeStamp;
        private QuickAccessorTwo<BattleEntity, PickupInfo> pickupAccessor;
        internal ECSIntegration ecsInteg;

        public Action EnemyGenerateMoves;
        public TrackBattle trackBattle;

        public BattleConfig BattleConfig { get; private set; }
        public int BoardWidth { get; internal set; } = 6;
        public int BoardHeight { get; internal set; } = 3;

        public BattleMain(int mode, ECSManager ecs, TimeStamp timeStamp)
        {
            //this.ecs = ecs;
            this.timeStamp = timeStamp;
            pickupAccessor = ecs.QuickAccessor2<BattleEntity, PickupInfo>();
            movementMoves.Add(MoveType.MoveUp, Vector2D.UnitY);
            movementMoves.Add(MoveType.MoveDown, -Vector2D.UnitY);
            movementMoves.Add(MoveType.MoveLeft, -Vector2D.UnitX);
            movementMoves.Add(MoveType.MoveRight, Vector2D.UnitX);

            ecs.CreateEntityWithComponent(battleState);

            playerHandFixed.Clear();
            playerHandFixed.Add(MoveType.MoveRight);
            playerHandFixed.Add(MoveType.MoveLeft);
            playerHandFixed.Add(MoveType.MoveDown);
            playerHandFixed.Add(MoveType.MoveUp);

            if (mode == 0)
            {
                playerHandUnfixed.Add(MoveType.NormalShot);
                enemyMoves = new MoveType[] {
                    MoveType.MoveUp,
                    MoveType.MoveLeft,
                    MoveType.MoveDown,
                    MoveType.MoveRight,
                    MoveType.NormalShot,
                };
            }
            else
            {
                //playerHandUnfixed.Add(MoveType.Fire);
                //playerHandUnfixed.Add(MoveType.Ice);
                //playerHandUnfixed.Add(MoveType.Thunder);


                enemyMoves = new MoveType[] {
                    MoveType.MoveDown,
                    MoveType.MoveLeft,
                    MoveType.MoveUp,
                    MoveType.MoveRight,
                    MoveType.Fire,
                    MoveType.Ice,
                    MoveType.Thunder,
                };
            }

            //playerHand.Add(MoveType.NormalShot);

        }

        public bool IsVictory()
        {
            return battleResult.result == 1;
        }


        public void Init()
        {
            if (BattleConfig.attackDemonstrationMode == false)
            {
                BattleEntity hero = new BattleEntity();

                hero.pos.Set(1, 1);
                hero.minPos.Set(0, 0);
                hero.maxPos.Set(2, 2);
                hero.Type = EntityType.hero;
                hero.maxLife = 50;
                
                for (int i = 0; i < hero.moves.Length; i++)
                {
                    hero.moves[i] = -1;
                }


                entities.Add(hero);
                ecsInteg.HeroCreated(hero);
            }

            
            ecsInteg.SpawnEnemies();

            {
                //GameEntity pickup = new GameEntity();
                //pickup.Type = EntityType.pickup;
                //pickup.pos.Set(0, 2);
                //pickup.life = 2;
                //pickup.graphic = 4;
                //entities.Add(pickup);
            }
            //{
            //    BattleEntity panelEffect = new BattleEntity();
            //    panelEffect.Type = EntityType.paneleffect;
            //    panelEffect.pos.Set(0, 2);
            //    panelEffect.life = 5;
            //    panelEffect.graphic = 5;
            //    panelEffect.randomPosition = true;
            //    panelEffect.drawLife = false;
            //    panelEffect.drawTurn = false;
            //    RandomPosition(panelEffect);
            //    entities.Add(panelEffect);
            //}

            //{
            //    BattleEntity panelEffect = new BattleEntity();
            //    panelEffect.Type = EntityType.paneleffect;
            //    panelEffect.pos.Set(0, 2);
            //    panelEffect.life = 5;
            //    panelEffect.graphic = 5;
            //    panelEffect.randomPosition = true;
            //    panelEffect.drawLife = false;
            //    panelEffect.drawTurn = false;
            //    RandomPosition(panelEffect);
            //    entities.Add(panelEffect);
            //}

            Reset();
            ExecutePhase();
        }

        public BattleEntity NewBattleEntity()
        {
            BattleEntity battleEntity = new BattleEntity();
            entities.Add(battleEntity);
            return battleEntity;
        }

        internal void Reset()
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].life = entities[i].maxLife;
            }
            ChangePhase(BattlePhase.EnemyMoveChoice);
            battleState.turn.Val = 0;
            battleState.totalTurns = 0;
            battleState.actingEntity = 0;
            battleState.moveTick_Now.Val = 0;
            battleState.moveTick_Total = 1;
            battleResult.result = 0;
        }

        internal bool IsOver()
        {
            return battleResult.result != 0;
        }

        public void Tick()
        {
            FinishPreviousTick();
            bool heroAlive = false;
            bool enemyAlive = false;
            bool pickupObligatoryExist = false;
            foreach (var item in entities)
            {
                if (item.Type == EntityType.enemy)
                {
                    if (item.life > 0)
                        enemyAlive = true;
                }
                if (item.Type == EntityType.hero)
                {
                    if (item.life > 0)
                        heroAlive = true;
                }
            }
            for (int i = 0; i < this.pickupAccessor.Length; i++)
            {
                var pickup = pickupAccessor.Comp2(i);
                if (pickup.necessaryForVictory && pickupAccessor.Comp1(i).Alive)
                {
                    pickupObligatoryExist = true;
                }
            }
            if (battleState.BattleEndEnable && !BattleConfig.attackDemonstrationMode)
            {
                if (!heroAlive)
                {
                    battleResult.result = 2;
                }
                else if ((!enemyAlive || !BattleConfig.needKillAllEnemies) && !pickupObligatoryExist)
                {
                    battleResult.result = 1;
                    trackBattle.BattleEndVictory();
                }
            }

            if (battleResult.result == 0)
            {
                happManager.Tick();
                timeStamp.Advance(1);
                ExecutePhase();
            }

        }

        public void Update(float delta)
        {
            if (timeToChoose > 0 && battleState.phase == BattlePhase.PickHands)
            {
                timeToChoose -= delta;
                if (timeToChoose <= 0)
                {
                    Tick();
                }
            }


        }

        internal void FinishPreviousTick()
        {
            BattlePhase previousPhase = battleState.phase;
            switch (previousPhase)
            {
                case BattlePhase.EnemyMoveChoice:
                    ChangePhase(BattlePhase.HandRecharge);
                    //if (BattleConfig.attackDemonstrationMode == false)
                    //    ChangePhase(BattlePhase.HandRecharge);
                    //else
                    //    ChangePhase(BattlePhase.PickHands);
                    break;
                case BattlePhase.HandRecharge:
                    ChangePhase(BattlePhase.PickHands);
                    break;
                case BattlePhase.PickHands:
                    ChangePhase(BattlePhase.ExecuteMove);
                    break;
                case BattlePhase.ExecuteMove:
                    if (battleState.moveTick_Now >= battleState.moveTick_Total - 1)
                    {
                        battleState.moveTick_Now.Val = 0;
                        battleState.moveTick_Total = 1;
                        bool noMoreUnitsToActThisTurn = true;
                        int i_initial = battleState.actingEntity + 1;
                        if (i_initial < entities.Count)
                        {
                            for (int i = i_initial; i < entities.Count; i++)
                            {
                                if (entities[i].Alive)
                                {
                                    battleState.actingEntity = i;
                                    noMoreUnitsToActThisTurn = false;
                                    break;
                                }
                            }
                        }


                        if (noMoreUnitsToActThisTurn)
                        {
                            if (battleState.turn >= battleState.turnsPerPhase - 1)
                            {
                                ChangePhase(BattlePhase.EnemyMoveChoice);
                                foreach (var e in entities)
                                {
                                    if (e.randomPosition)
                                    {
                                        //Console.WriteLine("RANDOM POS!!");
                                        RandomPosition(e);
                                    }
                                }
                            }
                            else
                            {
                                battleState.actingEntity = 0;
                                battleState.turn = battleState.turn + 1;
                                battleState.totalTurns += 1;
                            }
                        }
                    }
                    else
                    {
                        battleState.moveTick_Now.Val += 1;
                    }
                    break;
                default:
                    break;
            }
        }

        private static void RandomPosition(BattleEntity e)
        {
            e.pos.X = RandomSupplier.Range(0, 5);
            e.pos.Y = RandomSupplier.Range(0, 2);
        }

        private void ChangePhase(BattlePhase phase)
        {
            BattlePhase previousPhase = battleState.phase;
            if (phase == previousPhase) return;

            if (phase == BattlePhase.PickHands)
            {
                trackBattle.Round();
                if (previousPhase != BattlePhase.ConfirmInput)
                {
                    if (BattleConfig.heroPresent)
                    {
                        deckManager.Refresh();
                        playerHandPool.Clear();
                        var moves = deckManager.selection;
                        for (int i = 0; i < moves.Count; i++)
                        {
                            playerHandPool.Add((MoveType)moves[i]);
                        }

                        //playerHandPool.Shuffle();
                        playerHandUnfixed.Clear();
                        int commandsToAdd = moves.Count;
                        if (commandsToAdd > playerHandPool.Count)
                        {
                            commandsToAdd = playerHandPool.Count;
                        }
                        for (int i = 0; i < commandsToAdd; i++)
                        {
                            playerHandUnfixed.Add(playerHandPool[i]);
                        }


                        timeToChoose = timeToChooseMax;
                    }
                    
                }

            }
            if (previousPhase == BattlePhase.ExecuteMove)
            {
                battleState.turn.Val = 0;
                battleState.actingEntity = 0;
                battleState.moveTick_Now.Val = 0;
                battleState.moveTick_Total = 1;
                foreach (var e in entities)
                {
                    for (int i = 0; i < e.moves.Length; i++)
                    {
                        e.moves[i] = -1;
                    }
                }
            }
            battleState.phase = phase;
        }

        internal void ConfirmInputStart()
        {
            ChangePhase(BattlePhase.ConfirmInput);
            inputs.Clear();
            inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.Confirm), InputTags.MISC);
            inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.Cancel), InputTags.MISC);
        }

        private void ExecutePhase()
        {
            var phase = battleState.phase;
            switch (phase)
            {
                case BattlePhase.EnemyMoveChoice:
                    ecsInteg.SpawnEnemies();
                    EnemyGenerateMoves();
                    break;
                case BattlePhase.HandRecharge:
                    break;
                case BattlePhase.PickHands:
                    PickHandInput();
                    break;
                case BattlePhase.ExecuteMove:
                    ecsInteg.SpawnEnemies();
                    ExecuteMoves();
                    break;
                default:
                    break;
            }
        }

        private void PickHandInput()
        {
            //deckManager.
            inputs.Clear();
            inputs.inputForConfirmation = new InputUnit(InputType.None, -1);

            if (!BattleConfig.attackDemonstrationMode)
            {
                foreach (var hi in playerHandFixed)
                {
                    inputs.Add(new Turnbased.InputUnit(InputType.Move, (int)hi), InputTags.MOVEFIX);
                }
                foreach (var hi in playerHandUnfixed)
                {
                    if (hi != MoveType.DoNothing)
                        inputs.Add(new Turnbased.InputUnit(InputType.Move, (int)hi), InputTags.MOVEUNFIX);
                }
                inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.Redo), InputTags.MISC);
                inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.Done), InputTags.MISC);
                inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.Help), InputTags.MISC);
            }
            else
            {
                inputs.Add(new Turnbased.InputUnit(InputType.MiscBattle, MiscBattleInput.LeaveAttackDemonstration), InputTags.MISC);
            }
            
            
            bool enemyExist = false;
            foreach (var item in entities)
            {
                if (item.Type == EntityType.enemy)
                {
                    enemyExist = true;
                }
            }
            //if (enemyExist)
            //    inputs.Add(new Turnbased.Input(InputType.MiscBattle, MiscBattleInput.Preview), InputTags.MISC);
        }

        public void InputDone(InputUnit input)
        {



            if (battleState.phase == BattlePhase.ConfirmInput)
            {
                ProcessInput(input);
            }
            else
            {
                inputs.inputForConfirmation = input;
            }

        }

        public void InputConfirmed()
        {
            ChangePhase(BattlePhase.PickHands);
            InputUnit input = inputs.inputForConfirmation;
            inputs.inputForConfirmation = new InputUnit(InputType.None, -1);
            ProcessInput(input);
        }

        private void ProcessInput(InputUnit input)
        {
            //Console.Write("INPUT");
            if (input.type == InputType.Move)
            {
                MoveType arg1 = (MoveType)input.arg1;
                //Console.Write("INPUTTED1");
                if (playerHandFixed.Contains(arg1) || playerHandUnfixed.Contains(arg1))
                {
                    deckManager.InputUsed((int)arg1);
                    //Console.Write("INPUTTED2");
                    MoveChosen(arg1);
                }

            }

            if (input.type == InputType.MiscBattle)
            {
                MiscBattleInput misc = (MiscBattleInput)input.arg1;
                //Console.Write("INPUT"+misc);
                if (misc == MiscBattleInput.Redo)
                {
                    foreach (var e in entities)
                    {
                        if (e.Type == EntityType.hero)
                        {
                            for (int i = 0; i < e.moves.Length; i++)
                            {
                                if (e.moves[i] == null)
                                {
                                    e.moves[i] = -1;
                                }
                                int value = e.moves[i];

                                if (value == -1 || i == battleState.turnsPerPhase)
                                {
                                    if (i > 0)
                                    {
                                        int lostMove = e.moves[i - 1];
                                        deckManager.Recover(lostMove);
                                        e.moves[i - 1] = -1;
                                    }
                                }
                            }
                        }
                    }
                }
                if (misc == MiscBattleInput.Done)
                {
                    Tick();
                }
                if (misc == MiscBattleInput.Confirm)
                {
                    InputConfirmed();
                    PickHandInput();
                }
                if (misc == MiscBattleInput.Cancel)
                {

                }
                if (misc == MiscBattleInput.LeaveAttackDemonstration)
                {
                    battleResult.result = 3; //wanna leave
                    //write this
                }
            }
        }

        internal bool BattleDecided()
        {
            int heroes = 0;
            int enemies = 0;
            foreach (var e in entities)
            {
                if (e.Type == EntityType.hero)
                {
                    if (e.Alive)
                        heroes++;
                }
                if (e.Type == EntityType.enemy)
                {
                    if (e.Alive)
                        enemies++;
                }
            }
            return heroes == 0 || enemies == 0;
        }

        public void MoveChosen(MoveType moveType)
        {
            foreach (var e in entities)
            {
                if (e.Type == EntityType.hero)
                {
                    for (int i = 0; i < e.moves.Length; i++)
                    {

                        int value = e.moves[i];

                        if (value == -1)
                        {

                            e.moves[i] = (int)moveType;
                            break;
                        }
                    }
                }
            }
        }


        public void ExecuteMoves()
        {

            //Console.Write("bla" + battleState.turn.Val);
            //Console.Read();
            BattleEntity attacker = entities[battleState.actingEntity];
            int turn = battleState.turn;
            ExecuteMove(attacker, turn);
        }

        public void ExecuteMove(BattleEntity actor, int turn)
        {
            MoveDataExecuter.ExecuteMove(actor, turn);

        }

        public float CalculateAttackMultiplier(BattleEntity actor)
        {
            var baseD = actor.damageMultiplier;
            foreach (var e in entities)
            {
                if (e != actor)
                {
                    if (e.pos == actor.pos)
                    {
                        if (e.Type == EntityType.paneleffect)
                        {
                            baseD *= 3;
                        }
                    }
                }
            }
            return baseD;
        }


        public float CalculateDefenderMultiplier(BattleEntity actor)
        {
            var baseD = 1;
            foreach (var e in entities)
            {
                if (e != actor)
                {
                    if (e.pos == actor.pos)
                    {
                        if (e.Type == EntityType.paneleffect)
                        {
                            baseD *= 3;
                        }
                    }
                }
            }
            return baseD;
        }

        public class BattleState
        {
            public Value turn = new Value();
            public int totalTurns;
            public Value turnsPerPhase = new Value();
            public Value moveTick_Now = new Value();
            public int moveTick_Total = 0;
            public int actingEntity = 0;
            public BattlePhase phase;
            public bool BattleEndEnable = true;
        }

        public class BattleEntity
        {
            public int life;
            public Vector2D pos = new Vector2D();
            public Vector2D minPos = new Vector2D();
            public Vector2D maxPos = new Vector2D();
            public int[] moves = new int[10];
            public int graphic;
            public int graphicRepeatedIndex;
            public float damageMultiplier = 1;
            public bool drawLife = true;
            public bool drawTurn = true;
            public bool randomPosition = false;
            public Element element = Element.Nothingness;
            public int maxLife = 3;

            public EntityType Type = EntityType.hero;
            public Vector2D PositionV2D { get { return new BaseUtils.Vector2D(pos.X, pos.Y); } }

            public bool Dead { get { return life <= 0; } }

            public bool Alive { get { return !this.Dead; } }

        }

        public enum MoveType
        {
            DoNothing,
            MoveUp,
            MoveLeft,
            MoveDown,
            MoveRight,
            NormalShot,
            Fire,
            Ice,
            Thunder,
            IceBomb,
            ThunderBomb,
            SummonEntity,
            DownFire,
            TripleFire,
        }

        public enum HappTag
        {
            AttackHit,
            AttackMiss,
            DamageTaken,
            MovementFail
        }

        public enum BattlePhase
        {
            EnemyMoveChoice,
            HandRecharge,
            PickHands,
            ConfirmInput,
            ExecuteMove,

        }

        public enum EntityType
        {
            hero, enemy, neutral, pickup, paneleffect
        }

        public enum Element
        {
            Fire, Ice, Thunder,
            Nothingness
        }
    }



    public class Value
    {
        public float Val { get; set; }

        public Enum valAsEnum { set { Val = Convert.ToSingle(value); } }

        internal void Set(int v)
        {
            Val = v;
        }

        public static Value operator +(Value c1, float c2)
        {
            c1.Val += c2;
            return c1;
        }

        public static float operator -(Value c1, float c2)
        {
            return c1.Val - c2;
        }

        public static bool operator ==(Value c1, Value c2)
        {
            bool c2null = object.ReferenceEquals(c2, null);
            bool c1null = object.ReferenceEquals(c1, null);
            if (c2null && c1null)
                return true;
            if (c1null || c2null)
            {
                return false;
            }
            return c1.Val == c2.Val;
        }

        public static bool operator !=(Value c1, Value c2)
        {
            bool c2null = object.ReferenceEquals(c2, null);
            bool c1null = object.ReferenceEquals(c1, null);
            if (c2null && c1null)
                return false;
            if (c1null || c2null)
            {
                return true;
            }
            return c1.Val != c2.Val;
        }

        public static implicit operator float(Value d)
        {
            return d.Val;
        }

        public static implicit operator int(Value d)
        {
            return (int)d.Val;
        }


    }


    public class BattleResult
    {
        public int result;
    }

    public struct InputUnit
    {
        public readonly InputType type;
        public readonly int arg1;



        public InputUnit(InputType type, int arg1)
        {
            this.type = type;
            this.arg1 = arg1;
        }

        public InputUnit(InputType type, Enum arg1)
        {
            this.type = type;
            this.arg1 = Convert.ToInt32(arg1);
        }
    }

    public enum InputType
    {
        None, Move, MiscBattle
    }

    public enum MiscBattleInput
    {
        Done,
        Redo,
        Preview,
        Confirm,
        Cancel,
        Help,
        LeaveAttackDemonstration
    }
}
