﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class EnemyAI
    {
        public readonly List<object> moves = new List<object>();
    }

    public class EnemyAIState
    {
        public int progress;
    }

    public class MoveUse
    {
        public readonly int move;

        public MoveUse()
        {
        }

        public MoveUse(int move)
        {
            this.move = move;
        }
    }


}
