﻿using Pidroh.ConsoleApp.Turnbased.DebugExtra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased.Happs
{

    //legacy happs
    //better system on happhandling
    //still used in some situations though...
    public class HappManager
    {
        public int CurrentTime { get; private set; }
        List<Happ> Happs = new List<Happ>();
        List<HappHandler> handlers = new List<HappHandler>();
        int latestHandled = -1;

        public void AddHandler(HappHandler hh)
        {
            handlers.Add(hh);
        }

        public void TryHandle()
        {
            if(latestHandled != CurrentTime)
                Handle();
        }

        private void Handle()
        {
            latestHandled = CurrentTime;
            foreach (var h in handlers)
            {
                for (int i = Happs.Count - 1; i >= 0; i--)
                {
                    //this check assumes happs are ordered by time stamp
                    //which they should be automatically
                    if (Happs[i].TimeStamp != CurrentTime)
                    {
                        DebugEx.Log("Happening not equal to current time");
                        break;
                    }
                    bool hasTags = true;
                    foreach (var tagsNeeded in h.necessaryTags)
                    {
                        if (!Happs[i].HasTag(tagsNeeded))
                        {
                            hasTags = false;
                            break;
                        }
                    }
                    if (hasTags)
                    {
                        DebugEx.Log("Happening handled");
                        h.Handle(Happs[i]);
                    }
                    else
                    {
                        DebugEx.Log("Happening tag is different");
                    }
                }
            }
        }

        public Happ Add(Happ h)
        {
            h.TimeStamp = CurrentTime;
            Happs.Add(h);
            return h;
        }

        public void Tick()
        {
            CurrentTime++;
        }



    }

    public class Happ
    {
        //public string MainTag;
        public List<int> tags = new List<int>();
        public int TimeStamp;
        List<Attribute> attrs = new List<Attribute>();

        //public Happ(IConvertible c)
        //{
        //    tags.Add(Convert.ToInt32(c));
        //}

        public Happ(object mainTag)
        {
            //MainTag = mainTag.ToString();
            tags.Add(Convert.ToInt32(mainTag));
        }

        public class Attribute
        {
            public float Value { get; private set; }
            public Attribute SetValue(float f)
            {
                Value = f;
                return this;
            }
            public TagHolder tags = new TagHolder();
        }

        public Happ AddAttribute(Attribute a)
        {
            attrs.Add(a);
            return this;
        }

        internal int GetAttribute_Int(int index)
        {
            return (int)attrs[index].Value;
        }

        internal bool HasTag(int tagsNeeded)
        {
            return tags.Contains(tagsNeeded);
        }
    }

    public class HappHandler
    {
        public List<int> necessaryTags = new List<int>();
        public Action<Happ> Handle;

        public HappHandler(object mainTag, Action<Happ> handle)
        {
            this.necessaryTags.Add(Convert.ToInt32(mainTag));
            Handle = handle;
        }
    }

    public class TagHolder
    {
        public List<object> Tags { get; } = new List<object>();

        public bool HasTag(object t)
        {
            return Tags.Contains(t);
        }

        internal void Add(object v)
        {
            Tags.Add(v);
        }
    }


}
