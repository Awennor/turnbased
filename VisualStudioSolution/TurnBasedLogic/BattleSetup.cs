﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class BattleSetup
    {
        public ECSManager ecs;
        public BattleMain battleMain;
        public MoveCreatorProg moveCreator;
        public TimeStamp timeStamp;

        public BattleSetup(int mode, ECSManager ecs, EnemyDataCreator enemyDataCreator, MoveCreatorProg mv)
        {
            this.ecs = ecs;
            timeStamp = new TimeStamp();
            ecs.CreateEntityWithComponent(timeStamp);
            battleMain = new BattleMain(mode, ecs, timeStamp);

            moveCreator = mv;

            battleMain.MoveDataExecuter = new MoveDataExecuter(battleMain, moveCreator.moveDatas, ecs, timeStamp);

            var enemyDatas = enemyDataCreator.enemyDatas;
            var enemyFactory = new SpawnEntityFactory(ecs, enemyDatas, battleMain);
            battleMain.ecsInteg = new ECSIntegration(enemyFactory, ecs);
            var battleState = battleMain.battleState;

            { //AI handling code
                var enemyAis = ecs.QuickAccessor2<EnemyAI, BattleMain.BattleEntity>();
                var enemyAiStateless = ecs.CreateAccessor(necessary: new Type[] { typeof(EnemyAI) }, not: new Type[] { typeof(EnemyAIState) });
                List<int> possibleMoves = new List<int>();
                var moveDatas = moveCreator.moveDatas;

                battleMain.EnemyGenerateMoves = () =>
                {
                    while (enemyAiStateless.Length > 0)
                    {
                        enemyAiStateless.Get(0).AddComponent<EnemyAIState>();

                    }

                    for (int i = 0; i < enemyAis.Length; i++)
                    {

                        var ai = enemyAis.Comp1(i);
                        var battler = enemyAis.Comp2(i);
                        var aiState = enemyAis.Entity(i).GetComponent<EnemyAIState>();
                        var moves = ai.moves;
                        var posS = battler.pos;
                        for (int j = 0; j < battleState.turnsPerPhase; j++)
                        {
                            int aiPro = (j + aiState.progress) % moves.Count;
                            var move = moves[aiPro];
                            int moveId = -1;
                            if (move is MoveUse)
                            {

                                moveId = (move as MoveUse).move;
                            }

                            if (move is SpecialEnemyMoves)
                            {

                                var m = (SpecialEnemyMoves)move;
                                if (m == SpecialEnemyMoves.SmartMove)
                                {

                                    possibleMoves.Clear();
                                    for (int i2 = 0; i2 < moveDatas.Count; i2++) //code to add movement moves
                                    {
                                        var tags = moveDatas[i2].tags;
                                        if (tags.Contains((int)MoveDataTags.Movement))
                                        {
                                            bool validMove = true;
                                            var us = moveDatas[i2].units;

                                            foreach (var item in us)
                                            {
                                                var things = item.thingsToHappen;
                                                foreach (var thing in things)
                                                {
                                                    if (thing is MoveAction)
                                                    {
                                                        var ma = thing as MoveAction;
                                                        var dis = ma.distance;
                                                        var testPos = posS + dis;
                                                        if (testPos.X < 3)
                                                        {
                                                            validMove = false;
                                                        }
                                                        if (testPos.X > 5)
                                                        {
                                                            validMove = false;
                                                        }
                                                        if (testPos.Y < 0)
                                                        {
                                                            validMove = false;
                                                        }
                                                        if (testPos.Y > 2)
                                                        {
                                                            validMove = false;
                                                        }
                                                    }
                                                }
                                            }
                                            if (validMove)
                                                possibleMoves.Add(i2);
                                        }
                                    }
                                    moveId = possibleMoves.RandomElement();

                                    //Console.WriteLine(moveId);

                                }

                            }

                            if (moveId >= 0)
                            {
                                battler.moves[j] = moveId;
                                var md = moveCreator.moveDatas[moveId];
                                var us = md.units;
                                foreach (var item in us)
                                {
                                    var things = item.thingsToHappen;
                                    foreach (var thing in things)
                                    {
                                        if (thing is MoveAction)
                                        {
                                            var ma = thing as MoveAction;
                                            var dis = ma.distance;
                                            posS += dis;

                                        }
                                    }
                                }
                            }
                            //be.moves[j] = ;
                        }
                        aiState.progress += battleState.turnsPerPhase;
                    }
                };
            }

            var trackBattle = new TrackBattle();
            battleMain.MoveDataExecuter.trackBattle = trackBattle;
            battleMain.trackBattle = trackBattle;

        }

        internal void AttackPreviewInitialization(MoveData moveDataChosenPreview, int enemyTutorialId, int moveDataIndex)
        {
            battleMain.BattleConfigure(new BattleConfig(false, true, false, false));
            var testEnemySpawn = new SpawnData(enemyTutorialId+moveDataIndex, new Vector2D(4, 1),(int)BattleMain.EntityType.enemy);
            ecs.CreateEntityWithComponent(testEnemySpawn);


        }

        public void StageInitialization(Entity stageEntity, ECSManager ecs)
        {
            var fixedAttack = stageEntity.GetComponent<FixedAttackStage>();
            var playerHandPool = battleMain.playerHandPool;
            if (fixedAttack != null)
            {

                foreach (var item in fixedAttack.moves)
                {
                    playerHandPool.Add((BattleMain.MoveType)item);
                }
            }
            else
            {
                playerHandPool.Add(BattleMain.MoveType.Fire);
                playerHandPool.Add(BattleMain.MoveType.Ice);
                playerHandPool.Add(BattleMain.MoveType.Thunder);
            }

            var deckManager = new DeckManager(5);
            battleMain.deckManager = deckManager;

            if (fixedAttack != null)
            {

                foreach (var item in fixedAttack.moves)
                {
                    int deckId = 0;
                    switch (moveCreator.moveRenders[item].element)
                    {
                        case BattleMain.Element.Fire:
                            deckManager.SetDefaultAttack(0, (int)BattleMain.MoveType.Fire);
                            break;
                        case BattleMain.Element.Ice:

                            deckManager.SetDefaultAttack(1, (int)BattleMain.MoveType.Ice);
                            deckId = 1;
                            break;
                        case BattleMain.Element.Thunder:
                            deckId = 2;

                            deckManager.SetDefaultAttack(2, (int)BattleMain.MoveType.Thunder);
                            break;
                        case BattleMain.Element.Nothingness:
                            break;
                        default:
                            break;
                    }
                    if (item != (int)BattleMain.MoveType.Fire)
                        if (item != (int)BattleMain.MoveType.Ice)
                            if (item != (int)BattleMain.MoveType.Thunder)
                                deckManager.Add(deckId, item);
                }
            }
            else
            {
                deckManager.SetDefaultAttack(0, (int)BattleMain.MoveType.Fire);
                deckManager.SetDefaultAttack(1, (int)BattleMain.MoveType.Ice);
                deckManager.SetDefaultAttack(2, (int)BattleMain.MoveType.Thunder);

                //deckManager.Add(0, (int)BattleMain.MoveType.Fire);



                //deckManager.Add(1, (int)BattleMain.MoveType.Ice);   
                //deckManager.Add(2, (int)BattleMain.MoveType.Thunder);

                //deckManager.Add(0, (int)BattleMain.MoveType.DownFire);
                //deckManager.Add(1, (int)BattleMain.MoveType.IceBomb);
                //deckManager.Add(2, (int)BattleMain.MoveType.ThunderBomb);
            }

            var stage = stageEntity.GetComponent<StageData>();
            var enmys = stage.spawnDatas;

            foreach (var enemy in enmys)
            {
                ecs.CreateEntityWithComponent(enemy);
            }

            
            var trackInfo = stageEntity.GetComponent<TrackInfoStage>();
            battleMain.trackBattle.stageInfo = trackInfo;

            battleMain.BattleConfigure(stage.battleConfig);
        }

        internal void SetDefaultAttack(int index, int attack)
        {
            battleMain.deckManager.SetDefaultAttack(index, attack);
        }
    }


    /// <summary>
    /// data that will be a part of stagedata so each stage can have it's config
    /// It will also be contained in battlemain.
    /// Should be static, once created.
    /// </summary>
    public class BattleConfig
    {
        public readonly List<int> enemiesToSummon = new List<int>();
        public readonly bool needKillAllEnemies = true;
        public readonly bool attackDemonstrationMode = false;
        public readonly bool heroPresent = true;
        public bool intermissionScreen = true;



        public BattleConfig()
        {
        }

        public BattleConfig(int[] enemiesToSummon)
        {
            this.enemiesToSummon.AddRange(enemiesToSummon);
        }

        public BattleConfig(bool needKillAllEnemies)
        {
            this.needKillAllEnemies = needKillAllEnemies;
        }

        public BattleConfig(bool needKillAllEnemies, bool previewMode)
        {
            this.needKillAllEnemies = needKillAllEnemies;
            this.attackDemonstrationMode = previewMode;
        }

        public BattleConfig(bool needKillAllEnemies, bool attackDemonstrationMode, bool heroPresent, bool intermission) : this(needKillAllEnemies, attackDemonstrationMode)
        {
            this.intermissionScreen = intermission;
            this.heroPresent = heroPresent;
        }

        public BattleConfig NoIntermission()
        {
            this.intermissionScreen = false;
            return this;
        }
        
    }
}
