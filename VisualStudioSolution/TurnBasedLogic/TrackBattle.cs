﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class TrackBattle
    {


        internal TrackInfoStage stageInfo;

        public StageBattleMetrics metrics = new StageBattleMetrics(true);

        internal void HeroDamage(int damage)
        {
            if (damage > 0)
            {
                metrics.damage += damage;
                metrics.damageTimes++;
            }
        }

        public void Preview()
        {
            metrics.previewTimes++;
        }

        internal void Round()
        {
            metrics.round++;
        }

        internal void BattleEndVictory()
        {
            metrics.rank = CalculateRank();

        }

        /*
         * Rank formula
         * Goes from 1 to 100, where 100 is S. Can go above S.
         * good turns, no damage and no preview give you S
         */

        internal int CalculateRank()
        {
            if (stageInfo == null) return -1;
            int rank = 100;
            int diff = stageInfo.targetRounds - metrics.round;
            if (diff > 0)
            {
                rank += diff * (diff) * 3;
            }
            else
            {
                rank -= diff * (diff) * 3;
            }

            if (metrics.damageTimes > 3)
            {
                rank -= 10;
            }
            if (metrics.damageTimes > 0)
            {
                rank -= 10;
            }
            //if (metrics.previewTimes > 1)
            //{
            //    rank -= 5;
            //}
            //if (metrics.previewTimes > 3)
            //{
            //    rank -= 5;
            //}
            return rank;
        }
    }
    public class StageBattleMetrics
    {
        public int damage = -1;
        public int damageTimes = -1;
        public int previewTimes = -1;
        public int round = -1;
        public int rank = -1;
        

        public StageBattleMetrics()
        {
           
        }

        public StageBattleMetrics(bool initValues)
        {
            this.damage = 0;
            this.damageTimes = 0;
            this.previewTimes = 0;
            this.rank = 0;
            this.round = 0;
        }

        internal void CopyValues(StageBattleMetrics metrics)
        {
            this.damage = metrics.damage;
            this.damageTimes = metrics.damageTimes;
            this.previewTimes = metrics.previewTimes;
            this.round = metrics.round;
            this.rank = metrics.rank;
        }
    }

    public class StageProgressPersistence
    {
        public int stageId;
        public StageBattleMetrics bestBattleMetrics = new StageBattleMetrics();
        public bool available = false;

        public StageProgressPersistence()
        {
        }

        public StageProgressPersistence(int id)
        {
            stageId = id;
        }
    }
}
