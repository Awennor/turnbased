﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased.ContentCreator
{
    public class StageDataProcedural_A
    {
        StageDataCreator stageDataCreator;
        int[] enemyIds = new int[] { 0, 1, 2, 8, 5, };
        int[] bossIds = new int[] { 3, 9, 10 };
        List<int> attackPrizeIds = new List<int>();
        List<Point2D> positionPool = new List<Point2D>();

        public StageDataProcedural_A(StageDataCreator sdc)
        {
            
            this.stageDataCreator = sdc;
            //dsadsad
            for (int i = 14; i < 38; i++)
            {
                attackPrizeIds.Add(i);
            }
            var master = new StageDataGroup();
            sdc.AddGroup(master);
            master.childIds.Add(sdc.TutorialGroup());

            //{
            //    var debug = new StageDataGroup();
            //    var eDebug = sdc.AddGroup(debug);
            //    master.childIds.Add(eDebug);

            //    sdc.currentGroup = debug;

            //    sdc.Add(new StageData(
            //        new BattleConfig(new int[] { 0 }),
            //                sdc.Enemy(3, new BaseUtils.Vector2D(5, 1))
            //                ));
            //}

            for (int i = 0; i < 2; i++)
            {
                var campaign = new StageDataGroup();
                campaign.level = 1;
                var eCamp = sdc.AddGroup(campaign);
                master.childIds.Add(eCamp);
                for (int j = 0; j < 4; j++)
                {

                    var group = new StageDataGroup();
                    group.level = 0;
                    var eGroup = sdc.AddGroup(group);
                    campaign.childIds.Add(eGroup);
                    sdc.currentGroup = group;

                    int attackPrize = 0;
                    bool normalStage = j < 3;
                    int numberOfStages = 3;
                    for (int l = 0; l < numberOfStages; l++)
                    {
                        for (int x = 0; x < 3; x++)
                        {
                            for (int y = 0; y < 3; y++)
                            {
                                positionPool.Add(new Point2D(x+3, y));
                            }
                        }

                        bool finalStage = l == numberOfStages - 1;
                        {
                            int attackIndex = RandomSupplier.Range(0, attackPrizeIds.Count - 1);
                            attackPrize = attackPrizeIds[attackIndex];
                            attackPrizeIds.RemoveAt(attackIndex);
                        }
                        var sd = new StageData();
                        int nEnemies = RandomSupplier.Range(1, 3);
                        int currency = 30;
                        bool boss = finalStage && !normalStage;
                        if (boss)
                        {
                            currency = 80;
                            nEnemies = 1;
                        }
                        
                        for (int enemyI = 0; enemyI < nEnemies; enemyI++)
                        {
                            var posI = RandomSupplier.Range(0, positionPool.Count-1);
                            var pos = positionPool[posI];
                            positionPool.RemoveAt(posI);
                            int enemyId = RandomSupplier.RandomElement(enemyIds);
                            if (boss)
                            {
                                enemyId = RandomSupplier.RandomElement(bossIds);
                            }
                            sd.spawnDatas.Add(sdc.Enemy(enemyId, new Vector2D(pos.X, pos.Y)));
                        }
                        sdc.AddStage(sd, 
                            //new AttackPrize(attackPrize), 
                            new CurrencyPrize(currency));
                    }

                    //{
                    //    int attackIndex = RandomSupplier.Range(0, attackPrizeIds.Count - 1);
                    //    attackPrize = attackPrizeIds[attackIndex];
                    //    attackPrizeIds.RemoveAt(attackIndex);
                    //}
                    //sdc.AddStage(new StageData(
                    //   sdc.Enemy(RandomSupplier.RandomElement(enemyIds), new BaseUtils.Vector2D(3, 2)),
                    //   sdc.Enemy(RandomSupplier.RandomElement(enemyIds), new BaseUtils.Vector2D(5, 1))
                    //   ), new AttackPrize(attackPrize));

                    //{
                    //    int attackIndex = RandomSupplier.Range(0, attackPrizeIds.Count - 1);
                    //    attackPrize = attackPrizeIds[attackIndex];
                    //    attackPrizeIds.RemoveAt(attackIndex);
                    //}

                    //sdc.AddStage(
                    //    new StageData(
                    //        new BattleConfig(new int[] { RandomSupplier.RandomElement(enemyIds) }),
                    //    sdc.Enemy(RandomSupplier.RandomElement(bossIds), new BaseUtils.Vector2D(4, 1))),
                    //    new AttackPrize(attackPrize)
                    //);

                }
            }



        }
    }
}
