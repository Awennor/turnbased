﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class DeckManager
    {
        List<int>[] decks = new List<int>[3];
        List<int>[] decksStatic = new List<int>[3];
        List<int>[] discards = new List<int>[3];
        bool[] decksOk = new bool[3];
        int[] defaultAttack = new int[4];
        public List<int> selection = new List<int>();
        List<int> decksNotEmpty = new List<int>();
        bool defaultAttackAlwaysPresentAtStart = true;
        public List<int> movesToRemove = new List<int>();

        public DeckManager(int handSize)
        {
            for (int i = 0; i < handSize; i++)
            {
                selection.Add(0);
            }
            for (int i = 0; i < decks.Length; i++)
            {
                decks[i] = new List<int>();
                decksStatic[i] = new List<int>();
            }
        }

        public void Refresh()
        {
            //removes used moves from hand
            foreach (var item in movesToRemove)
            {
                int index = selection.IndexOf(item);
                if (index != -1)
                {
                    selection[index] = 0;
                }
            }
            movesToRemove.Clear();
            if (defaultAttackAlwaysPresentAtStart)
            {
                for (int i = 0; i < defaultAttack.Length; i++)
                {
                    selection[i] = defaultAttack[i];
                }
            }
            RefreshDeckEmptyStatus();
            for (int i = 0; i < decks.Length; i++)
            {
                decksOk[i] = false;
                if (selection.Contains(defaultAttack[i]))
                {
                    decksOk[i] = true;
                }
                for (int j = 0; j < decksStatic[i].Count; j++)
                {

                    if (selection.Contains(decksStatic[i][j]))
                    {
                        decksOk[i] = true;
                        break;
                    }
                }



            }
            for (int i = 0; i < selection.Count; i++)
            {
                if (selection[i] > 0) continue;
                bool added = false;
                for (int j = 0; j < decksOk.Length; j++)
                {
                    if (!decksOk[j])
                    {
                        decksOk[j] = true;
                        added = true;
                        int move = defaultAttack[j];
                        if (decks[j].Count != 0)
                        {
                            move = Draw(j);
                        }

                        selection[i] = move;
                        RefreshDeckEmptyStatus();
                        break;
                    }
                }
                if (!added)
                {
                    if (decksNotEmpty.Count > 0)
                    {
                        int deck = decksNotEmpty.RandomElement();
                        int move = Draw(deck);
                        selection[i] = move;
                        RefreshDeckEmptyStatus();
                    }
                }
            }
        }

        internal void SetDefaultAttack(int deckId, int move)
        {
            defaultAttack[deckId] = move;
        }

        private int Draw(int deckId)
        {
            var move = decks[deckId].RandomElement();
            decks[deckId].Remove(move);
            return move;
        }

        internal void Add(int deckId, int item)
        {
            decks[deckId].Add(item);
            decksStatic[deckId].Add(item);
        }

        private void RefreshDeckEmptyStatus()
        {
            decksNotEmpty.Clear();
            for (int i = 0; i < decks.Length; i++)
            {
                if (decks[i].Count > 0)
                {
                    decksNotEmpty.Add(i);
                }
            }
        }

        internal void InputUsed(int move)
        {
            movesToRemove.Add(move);
            //int index = selection.IndexOf(move);
            //if (index != -1)
            //{
            //    selection[index] = 0;
            //}

        }

        internal void Recover(int lostMove)
        {
            movesToRemove.Remove(lostMove);
        }
    }
}
