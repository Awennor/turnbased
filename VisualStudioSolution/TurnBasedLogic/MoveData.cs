﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;
using static Pidroh.ConsoleApp.Turnbased.BattleMain;

namespace Pidroh.ConsoleApp.Turnbased
{

    public class MoveData
    {
        string label;
        internal List<Tick> units = new List<Tick>();
        internal List<int> tags = new List<int>();

        public MoveData(string label)
        {
            this.label = label;
        }

        public MoveData()
        {
        }

        public static int FindByLabel(List<MoveData> moveDatas, string label)
        {
            for (int i = 0; i < moveDatas.Count; i++)
            {
                if (moveDatas[i] != null)
                    if (moveDatas[i].label == label) return i;
            }
            return -1;
        }

        internal bool HasTag(int tag)
        {
            return tags.Contains(tag);
        }

        
    }

    public class Tick
    {
        internal Condition condition;
        internal List<object> thingsToHappen = new List<object>();

        public Tick(object action)
        {
            thingsToHappen.Add(action);
        }

        public Tick(object[] actions)
        {
            thingsToHappen.AddRange(actions);
        }

        public Tick()
        {
        }
    }
    public class Condition
    {
        internal readonly ConditionType type;
        internal readonly Target target;
        internal readonly BaseUtils.Vector2D vector;

        public Condition(ConditionType type, Target target, BaseUtils.Vector2D vector)
        {
            this.type = type;
            this.target = target;
            this.vector = vector;
        }
    }

    public enum ConditionType
    {
        CanMove
    }

    public class SummonEntity
    {
        public readonly int enemyWhich;
        public readonly Vector2D preferentialRowColumn;

        public SummonEntity(int enemyWhich, Vector2D preferentialRowColumn)
        {
            this.enemyWhich = enemyWhich;
            this.preferentialRowColumn = preferentialRowColumn;
        }

        internal static SummonEntity Enemy(int v, Vector2D vector2D)
        {
            return new SummonEntity(v, vector2D);
        }
    }


    public class Animation
    {
        public readonly Area area = null;
        public readonly Target target = Target.None;
        public readonly Element element = Element.Nothingness;
        public readonly Vector2D userRefOffset;

        public Animation(Target target)
        {
            this.target = target;
        }

        public Animation(Target target, Element element)
        {
            this.element = element;
            this.target = target;
        }

        public Animation(Area area, Element element, Target target)
        {
            this.area = area;
            this.element = element;
            this.target = target;
        }

        public Animation(Area area, Element element)
        {
            this.area = area;
            this.element = element;
        }

        public Animation(Target target, Element element, Vector2D userRefOffset) : this(target, element)
        {
            this.userRefOffset = userRefOffset;
        }
    }

    public class MoveAction
    {
        public readonly Target target;
        public readonly BaseUtils.Vector2D distance;

        public MoveAction(Target target, BaseUtils.Vector2D amount)
        {
            this.target = target;
            this.distance = amount;
        }
    }

    public class DealDamageAction
    {
        public readonly Area area;
        public readonly Target target = Target.None;
        public readonly int damage;
        public readonly Element element;
        public readonly Vector2D userRefOffset;

        public DealDamageAction(Area area, int damage, Element element)
        {
            this.area = area;
            this.damage = damage;
            this.element = element;
            target = Target.Area;
        }

        public DealDamageAction(Target target, int damage, Element element)
        {
            this.target = target;
            this.damage = damage;
            this.element = element;
        }

        public DealDamageAction(Target target, int damage, Element element, Vector2D userRefOffset) : this(target, damage, element)
        {
            this.userRefOffset = userRefOffset;
        }
    }

    public class Area
    {
        //public readonly Area area;
        public readonly Target target;
        public readonly List<Vector2D> points = new List<Vector2D>();

        public Area(Target target)
        {
            this.target = target;
        }
    }

    public enum Target
    {
        None, Self, ClosestTarget, ClosestTargetX, Area
    }



}
