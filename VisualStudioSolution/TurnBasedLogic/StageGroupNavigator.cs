﻿using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class StageGroupNavigator
    {
        public List<Entity> backTrackPile = new List<Entity>();
        public Entity currentGroupE;
        public StageDataGroup currentSDG;

        public StageGroupNavigator(ECSManager ecs)
        {
            var sdgs = ecs.QuickAccessor1<StageDataGroup>();
            var firstGroup = sdgs.Comp1(0);
            Entity newCurrent = sdgs.Entity(0);
            ChangeCurrent(newCurrent);
        }

        private void ChangeCurrent(Entity newCurrent)
        {
            currentGroupE = newCurrent;
            currentSDG = currentGroupE.GetComponent<StageDataGroup>();

        }

        public void Enter(int childId)
        {
            backTrackPile.Add(currentGroupE);
            ChangeCurrent(currentGroupE.GetComponent<StageDataGroup>().childIds[childId]);
        }

        public void Backtrack()
        {
            int lastElement = backTrackPile.Count - 1;
            if (lastElement < 0)
            {
                return;
            }
            ChangeCurrent(backTrackPile[lastElement]);
            backTrackPile.RemoveAt(lastElement);
        }
    }
}
