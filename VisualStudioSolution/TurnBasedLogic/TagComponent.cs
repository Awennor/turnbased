﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class TagComponent
    {
        List<string> tags = new List<string>();

        public TagComponent()
        {
        }

        public TagComponent(string v)
        {
            tags.Add(v);
        }

        internal bool Contains(string v)
        {
            return tags.Contains(v);
        }
    }
}
