﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class MoveCreatorProg
    {
        internal List<MoveData> moveDatas = new List<MoveData>();
        public List<MoveMetaData> moveRenders = new List<MoveMetaData>();
        AreaCreationUtils areaUtils = new AreaCreationUtils();
        private readonly ECSManager ecs;

        BattleMain.Element[] elements = new BattleMain.Element[]
        {
            BattleMain.Element.Fire, BattleMain.Element.Ice, BattleMain.Element.Thunder
        };
        string[] elementLabels = new string[]
        {
            "Fire", "Ice", "Bolt"
        };
        string[] elementLetter = new string[]
        {
            "F", "I", "B"
        };

        public MoveCreatorProg(ECSManager ecs)
        {
            this.ecs = ecs;
            MoveData item = new MoveData("");
            moveDatas.Add(item); //do nothing
            moveRenders.Add(new MoveMetaData("", "", BattleMain.Element.Nothingness));
            ecs.CreateEntityWithComponent(item);
            BaseUtils.Vector2D[] directions = new BaseUtils.Vector2D[] {

                new BaseUtils.Vector2D(0, 1),
                new BaseUtils.Vector2D(-1, 0),
                new BaseUtils.Vector2D(0, -1),
                new BaseUtils.Vector2D(1, 0),
            };
            string[] moveLabels = new string[] {
                "Move Up",
                "Move Left",
                "Move Down",
                "Move Right",
            };
            string[] moveAbrev = new string[] {
                "^",
                "<",
                "v",
                ">",
            };
            for (int i = 0; i < directions.Length; i++)
            {
                NewMoveData(label: moveLabels[i], condition: new Condition(ConditionType.CanMove, Target.Self, directions[i]), action: new MoveAction(Target.Self, directions[i]), tags: TagArray(MoveDataTags.Movement, MoveDataTags.HeroInitial));
                NewMoveTextRenderData(name: moveLabels[i], abrev: moveAbrev[i], description: moveLabels[i], damage: 0);
            }

            int baseDamage = 10;

            NewMoveData("Gun", ticks: OneTickPerAction(new Animation(Target.ClosestTargetX), new DealDamageAction(Target.ClosestTargetX, baseDamage, BattleMain.Element.Nothingness)), tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("Gun", "G", "Shoots forward",baseDamage);


            NewMoveData("Firegun", ticks: OneTickPerAction(new Animation(Target.ClosestTargetX, BattleMain.Element.Fire), new DealDamageAction(Target.ClosestTargetX, baseDamage, BattleMain.Element.Fire)), tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("Firegun", "FG", "Shoots forward", baseDamage, BattleMain.Element.Fire);

            NewMoveData("Icegun", ticks: OneTickPerAction(new Animation(Target.ClosestTargetX, BattleMain.Element.Ice), new DealDamageAction(Target.ClosestTargetX, baseDamage, BattleMain.Element.Ice)), tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("Icegun", "IG", "Shoots forward", baseDamage, BattleMain.Element.Ice);

            NewMoveData("Boltgun", ticks: OneTickPerAction(new Animation(Target.ClosestTargetX, BattleMain.Element.Thunder), new DealDamageAction(Target.ClosestTargetX, baseDamage, BattleMain.Element.Thunder)), tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("Boltgun", "BG","Shoots forward", baseDamage, BattleMain.Element.Thunder);

            Area areaBomb = AreaUser().RowForward(width: 1, XDis: 3);
            NewMoveData("Icebomb", ticks: OneTickPerAction(new Animation(areaBomb, BattleMain.Element.Ice), new DealDamageAction(areaBomb, baseDamage, BattleMain.Element.Ice)), tags: TagArray(MoveDataTags.Bomb));
            NewMoveTextRenderData("Icebomb", "IB", "Attacks row three squares forward", baseDamage, BattleMain.Element.Ice);

            NewMoveData("Boltbomb", ticks: OneTickPerAction(new Animation(areaBomb, BattleMain.Element.Thunder), new DealDamageAction(areaBomb, baseDamage, BattleMain.Element.Thunder)), tags: TagArray(MoveDataTags.Bomb));
            NewMoveTextRenderData("Boltbomb", "BB","Attacks row three squares forward",baseDamage, BattleMain.Element.Thunder);

            NewMoveData("Summon", ticks: OneTickPerAction(SummonEntity.Enemy(0, new Vector2D(5, 0))), tags: TagArray(MoveDataTags.Summon));
            NewMoveTextRenderData("Summon","SU", "Summons an enemy", 0);

            Vector2D directionMove = new Vector2D(0, -1);
            int damageLow2 = (int)(baseDamage * 0.75f);
            int damageHigh = (int)(baseDamage * 3f);
            NewMoveData("Downfire",
                ticks: TickArray(
                    TickUnit(new Animation(Target.ClosestTargetX, BattleMain.Element.Fire)),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow2, BattleMain.Element.Fire)),
                    TickUnitCondition(
                        new Condition(ConditionType.CanMove, Target.Self, directionMove),
                        new MoveAction(Target.Self, directionMove)),
                    TickUnit(new Animation(Target.ClosestTargetX, BattleMain.Element.Fire)),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, 1, BattleMain.Element.Fire))),
                tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("Downfire", "DF", "Shoot, down, shoot",baseDamage, BattleMain.Element.Fire);

            NewMoveData("TripleFire", ticks:
                TickArray(
                    TickUnit(
                        new Animation(Target.ClosestTargetX, BattleMain.Element.Fire, new Vector2D(0, -1)),
                        new Animation(Target.ClosestTargetX, BattleMain.Element.Fire),
                        new Animation(Target.ClosestTargetX, BattleMain.Element.Fire, new Vector2D(0, 1))
                        ),
                    TickUnit(

                        new DealDamageAction(Target.ClosestTargetX, 1, BattleMain.Element.Fire, new Vector2D(0, -1)),

                        new DealDamageAction(Target.ClosestTargetX, 1, BattleMain.Element.Fire),

                        new DealDamageAction(Target.ClosestTargetX, 1, BattleMain.Element.Fire, new Vector2D(0, 1))
                        )

                    )
                , tags: TagArray(MoveDataTags.Shoot));
            NewMoveTextRenderData("TripleFire", "TF","Shoots in the three rows", baseDamage,BattleMain.Element.Fire);


            Area areaPunch = AreaUser().SquareForward(XDis: 1);
            Area areaSlash = AreaUser().RowForward(width: 1, XDis: 1);

            for (int i = 0; i < elements.Length; i++)
            {

                NewMoveData(elementLabels[i] + "bomb", ticks: OneTickPerAction(new Animation(areaBomb, elements[i]), new DealDamageAction(areaBomb, (int)(baseDamage * 1f), elements[i])), tags: TagArray(MoveDataTags.Bomb));
                NewMoveTextRenderData(elementLabels[i] + "bomb", elementLetter[i] + "B", "Attacks row three squares forward",baseDamage, elements[i]);
            }
           
            for (int i = 0; i < elements.Length; i++)
            {

                
                NewMoveData(elementLabels[i] + "punch", ticks: OneTickPerAction(new Animation(areaPunch, elements[i]), new DealDamageAction(areaPunch, damageHigh, elements[i])), tags: TagArray(MoveDataTags.Bomb));
                NewMoveTextRenderData(elementLabels[i] + "punch", elementLetter[i] + "P", "Attacks enemy forward",damageHigh, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {

                int damageHigh2 = (int)(baseDamage * 2f);
                NewMoveData(elementLabels[i] + "slash", ticks: OneTickPerAction(new Animation(areaSlash, elements[i]), new DealDamageAction(areaSlash, damageHigh2, elements[i])), tags: TagArray(MoveDataTags.Bomb));
                NewMoveTextRenderData(elementLabels[i] + "slash", elementLetter[i] + "S", "Attacks a row forward",damageHigh2, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {

                Vector2D userRefOffset = new Vector2D(0, 1);
                NewMoveData("Upside"+elementLabels[i], ticks:
                TickArray(
                    TickUnit(
                        new Animation(Target.ClosestTargetX, elements[i], userRefOffset)
                        ),
                    TickUnit(
                        new DealDamageAction(Target.ClosestTargetX, (int)(baseDamage * 1f), elements[i], userRefOffset)
                        )

                    )
                , tags: TagArray(MoveDataTags.Shoot));
                NewMoveTextRenderData("Upside" + elementLabels[i], "U"+elementLetter[i], "Shoots from Above",baseDamage, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {

                Vector2D userRefOffset = new Vector2D(0, -1);
                NewMoveData("Downside" + elementLabels[i], ticks:
                TickArray(
                    TickUnit(
                        new Animation(Target.ClosestTargetX, elements[i], userRefOffset)
                        ),
                    TickUnit(
                        new DealDamageAction(Target.ClosestTargetX, (int)(baseDamage * 1f), elements[i], userRefOffset)
                        )

                    )
                , tags: TagArray(MoveDataTags.Shoot));
                NewMoveTextRenderData("Downside" + elementLabels[i], "D" + elementLetter[i], "Shoots from below",baseDamage, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {

                Vector2D userRefOffset = new Vector2D(0, -1);
                Vector2D userRefOffset2 = new Vector2D(0, 1);
                NewMoveData("Sides" + elementLabels[i], ticks:
                TickArray(
                    TickUnit(
                        new Animation(Target.ClosestTargetX, elements[i], userRefOffset),
                        new Animation(Target.ClosestTargetX, elements[i], userRefOffset2)
                        ),
                    TickUnit(
                        new DealDamageAction(Target.ClosestTargetX, damageLow2, elements[i], userRefOffset),
                        new DealDamageAction(Target.ClosestTargetX, damageLow2, elements[i], userRefOffset2)
                        )

                    )
                , tags: TagArray(MoveDataTags.Shoot));
                NewMoveTextRenderData("Sides" + elementLabels[i], "S" + elementLetter[i], "Attacks from the sides",damageLow2, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {
                int damageLow3 = (int)(baseDamage * 0.4f);
                NewMoveData("Double" + elementLabels[i],
                ticks: TickArray(
                    TickUnit(new Animation(Target.ClosestTargetX, elements[i])),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow3, elements[i])),
                    TickUnit(new Animation(Target.ClosestTargetX, elements[i])),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow3, elements[i]))),
                tags: TagArray(MoveDataTags.Shoot));
                NewMoveTextRenderData("Double" + elementLabels[i], "D"+elementLetter[i],"Shoots two times",damageLow3, elements[i]);
            }
            for (int i = 0; i < elements.Length; i++)
            {
                int damageLow4 = (int)(baseDamage * 0.2f);
                NewMoveData("Triple" + elementLabels[i],
                ticks: TickArray(
                    TickUnit(new Animation(Target.ClosestTargetX, elements[i])),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow4, elements[i])),
                    TickUnit(new Animation(Target.ClosestTargetX, elements[i])),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow4, elements[i])),
                    TickUnit(new Animation(Target.ClosestTargetX, elements[i])),

                    TickUnit(new DealDamageAction(Target.ClosestTargetX, damageLow4, elements[i]))),
                tags: TagArray(MoveDataTags.Shoot));
                NewMoveTextRenderData("Triple" + elementLabels[i], "T" + elementLetter[i],"Shoots three times",damageLow4, elements[i]);
            }

        }



        internal int GetMoveId(string v)
        {
            return MoveData.FindByLabel(moveDatas, v);
        }

        private AreaCreationUtils AreaUser()
        {
            areaUtils.target = Target.Self;
            return areaUtils;
        }

        public class AreaCreationUtils
        {
            public Target target;
            int height = 3;

            internal Area RowForward(int width, int XDis)
            {
                var ra = new Area(target);
                int offsetY = (int)Math.Floor((float)height / 2f);
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {

                        ra.points.Add(new Vector2D(i + XDis, j - offsetY));
                    }
                }
                return ra;
            }

            internal Area SquareForward(int XDis)
            {
                var ra = new Area(target);
                ra.points.Add(new Vector2D(XDis, 0));
                return ra;
            }
        }


        private void NewMoveTextRenderData(string name, string abrev, string description, int damage, BattleMain.Element e = BattleMain.Element.Nothingness)
        {
            MoveMetaData item = new MoveMetaData(name, abrev, e);
            item.Description = description;
            item.damage = damage;
            moveRenders.Add(item);
        }

        private MoveData NewMoveData(string label, Tick[] ticks, object[] tags)
        {
            var mv = new MoveData(label);
            mv.units.AddRange(ticks);
            foreach (var item in tags)
            {
                mv.tags.Add(Convert.ToInt32(item));
            }
            ecs.CreateEntityWithComponent(mv);
            moveDatas.Add(mv);
            return mv;
        }

        private MoveData NewMoveData(string label, Condition condition, object action, object[] tags)
        {
            var mv = new MoveData(label);
            Tick tick = new Tick();
            tick.condition = condition;
            tick.thingsToHappen.Add(action);
            mv.units.Add(tick);
            foreach (var item in tags)
            {
                mv.tags.Add(Convert.ToInt32(item));
            }
            ecs.CreateEntityWithComponent(mv);
            moveDatas.Add(mv);
            return mv;
        }

        private Tick[] OneTickPerAction(params object[] actions)
        {
            Tick[] ticks = new Tick[actions.Length];
            for (int i = 0; i < ticks.Length; i++)
            {
                ticks[i] = new Tick(actions[i]);
            }
            return ticks;
        }

        private Tick TickUnitCondition(Condition condition, params Object[] actions)
        {
            Tick tick = new Tick(actions);
            tick.condition = condition;
            return tick;
        }


        private Tick TickUnit(params Object[] actions)
        {
            return new Tick(actions);
        }

        private Tick[] TickArray(params Tick[] ticks)
        {
            return ticks;
        }

        private object[] TagArray(params object[] args)
        {
            return args;
        }
    }

    public class MoveMetaData
    {
        public string Label;
        public string Abrev;
        public readonly BattleMain.Element element;
        public int damage;

        public MoveMetaData(string label, string abrev, BattleMain.Element element)
        {
            Label = label;
            Abrev = abrev;
            this.element = element;
        }

        //public MoveMetaData(string label, string abrev)
        //{
        //    this.Label = label;
        //    this.Abrev = abrev;
        //}

        public string Description { get; internal set; }
    }


}
