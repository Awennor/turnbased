﻿using Pidroh.BaseUtils;
using Pidroh.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class SpawnEntityFactory
    {

        ECSManager ecs;
        List<EnemyData> enemyDatas;
        private readonly BattleMain battleMain;
        private QuickAccessorOne<SpawnData> spawns;

        public SpawnEntityFactory(ECSManager ecs, List<EnemyData> enemyDatas, BattleMain battleMain)
        {
            this.ecs = ecs;
            //ecs.QuickAccessor1<EnemyData>();
            spawns = ecs.QuickAccessor1<SpawnData>();
            this.enemyDatas = enemyDatas;
            this.battleMain = battleMain;
        }

        public void Spawn()
        {
            int spawned = 0;
            //for (int i = 0; i < spawns.Count; i++)
            while (spawns.Count > 0)
            {
                SpawnData spawn = spawns.Comp1(0);
                spawns.Entity(0).RemoveComponent(spawn);
                var id = spawn.id;
                BattleMain.EntityType entType = (BattleMain.EntityType)spawn.entityType;
                if(entType == BattleMain.EntityType.pickup)
                {
                    var be = battleMain.NewBattleEntity();
                    be.Type = entType;
                    PickupInfo pickup = new PickupInfo(true);
                    var pickupE = ecs.CreateEntityWithComponent(pickup);
                    pickupE.AddComponent(be);
                    be.pos = spawn.position;
                    be.life = 1;
                    be.maxLife = 1;
                    be.drawLife = false;
                    be.drawTurn = false;
                    be.graphic = 4;
                    
                }
                if (entType == BattleMain.EntityType.neutral)
                {
                    var be = battleMain.NewBattleEntity();
                    be.Type = entType;

                    be.pos = spawn.position;
                    be.life = 30;
                    be.maxLife = 30;
                    be.drawLife = false;
                    be.drawTurn = false;
                    be.graphic = 5;

                }
                if (entType == BattleMain.EntityType.enemy)
                {
                    var enemyAI = enemyDatas[id].enemyAI;
                    var enemy = ecs.CreateEntityWithComponent(enemyAI);
                    var be = battleMain.NewBattleEntity();
                    be.pos = spawn.position;
                    be.life = enemyDatas[id].hp;
                    be.maxLife = be.life;
                    be.graphic = enemyDatas[id].render;
                    var entities = battleMain.entities;
                    foreach (var item in entities)
                    {
                        if (item != be && item.graphic == be.graphic)
                        {
                            be.graphicRepeatedIndex++;
                        }
                    }
                    be.minPos = new Vector2D(3, 0);
                    be.maxPos = new Vector2D(5, 2);
                    be.Type = BattleMain.EntityType.enemy;
                    enemy.AddComponent(be);
                    EnemyAIState enemyAiState = new EnemyAIState();
                    enemyAiState.progress = spawned;
                    enemy.AddComponent(enemyAiState);
                    //Console.Write("SPAWN");
                    spawned++;
                }
                
            }
            
        }
    }

    public class PickupInfo
    {
        public bool necessaryForVictory;

        public PickupInfo(bool necessaryForVictory)
        {
            this.necessaryForVictory = necessaryForVictory;
        }

        public PickupInfo()
        {
        }
    }

    public class EnemyData
    {
        public readonly EnemyAI enemyAI;
        public readonly int hp;
        public readonly int render;

        public EnemyData(EnemyAI enemyAI, int hp, int render)
        {
            this.enemyAI = enemyAI;
            this.hp = hp;
            this.render = render;
        }
    }
}
