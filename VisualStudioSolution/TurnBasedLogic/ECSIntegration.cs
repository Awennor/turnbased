﻿using Pidroh.ECS;
using System;

namespace Pidroh.ConsoleApp.Turnbased
{
    internal class ECSIntegration
    {

        SpawnEntityFactory enemyFactory;
        ECSManager ecs;

        public ECSIntegration(SpawnEntityFactory enemyFactory, ECSManager ecs)
        {
            this.enemyFactory = enemyFactory;
            this.ecs = ecs;
        }

        internal void HeroCreated(BattleMain.BattleEntity hero)
        {
            ecs.CreateEntityWithComponent(hero);
        }

        internal void SpawnEnemies()
        {
            enemyFactory.Spawn();
        }
    }
}