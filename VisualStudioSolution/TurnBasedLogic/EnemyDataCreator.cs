﻿using System;
using System.Collections.Generic;
using static Pidroh.ConsoleApp.Turnbased.BattleMain;

namespace Pidroh.ConsoleApp.Turnbased
{
    public class EnemyDataCreator
    {
        List<string> renderTexts;
        public List<EnemyData> enemyDatas = new List<EnemyData>();
        MoveCreatorProg moveCreatorProg;
        public int startOfTutorialEnemy;

        public EnemyDataCreator(List<string> renderTexts, MoveCreatorProg moveCreatorProg)
        {
            renderTexts.Add("@");
            this.renderTexts = renderTexts;
            this.moveCreatorProg = moveCreatorProg;

            //comment
            //AddEnemy(ai: Actions(
            //    SpecialEnemyMoves.SmartMove, MoveType.MoveLeft, MoveType.MoveDown
            //    ), hp: 2, renderText: "%");

            AddEnemy(ai: Actions(
                Moves(MoveType.MoveLeft,MoveType.MoveDown, MoveType.Fire, MoveType.MoveRight, MoveType.MoveUp, MoveType.Thunder)
                ), hp:12, renderText:"%");
            AddEnemy(ai: Actions(
                Moves(MoveType.IceBomb, MoveType.DoNothing, MoveType.DoNothing)
                ), hp: 15, renderText: "#");
            AddEnemy(ai: Actions(
               Moves(
                   MoveType.IceBomb,
                   MoveType.MoveLeft,
                   MoveType.ThunderBomb,
                   MoveType.MoveRight
                   )
               ), hp: 17, renderText: "&");
            AddEnemy(ai: Actions(
                   
                   MoveType.IceBomb,
                   "Summon",
                   MoveType.MoveLeft,
                   MoveType.ThunderBomb,
                   MoveType.MoveRight,
                   MoveType.Fire


               ), hp: 450, renderText: "$");
            AddEnemy(ai: Actions(

                   MoveType.MoveUp,
                   MoveType.MoveLeft,
                   MoveType.MoveRight,
                   MoveType.MoveDown,
                   MoveType.MoveLeft,
                   MoveType.MoveDown,
                   MoveType.MoveUp


               ), hp: 28, renderText: "H");
            AddEnemy(ai: Actions(

                MoveType.Ice,
                   MoveType.DoNothing,
                   MoveType.DoNothing



               ), hp: 30, renderText: "J");
            AddEnemy(ai: Actions(
                   MoveType.DoNothing,
                   MoveType.DoNothing



               ), hp: 30, renderText: "L");
            AddEnemy(ai: Actions(

                MoveType.Fire,
                   MoveType.DoNothing,
                   MoveType.DoNothing



               ), hp: 30, renderText: "K");
            AddEnemy(ai: Actions(

                MoveType.DownFire,
                SpecialEnemyMoves.SmartMove,
                SpecialEnemyMoves.SmartMove,
                SpecialEnemyMoves.SmartMove


               ), hp: 30, renderText: "K");
            AddEnemy(ai: Actions(

                   MoveType.IceBomb,
                   SpecialEnemyMoves.SmartMove,
                   MoveType.ThunderBomb,
                   SpecialEnemyMoves.SmartMove,
                   SpecialEnemyMoves.SmartMove,
                   MoveType.ThunderBomb,
                   SpecialEnemyMoves.SmartMove


               ), hp: 250, renderText: "$");
            AddEnemy(ai: Actions(

                   MoveType.Fire,
                   SpecialEnemyMoves.SmartMove,
                   MoveType.Thunder,
                   SpecialEnemyMoves.SmartMove,
                   SpecialEnemyMoves.SmartMove,
                   MoveType.Thunder,
                   SpecialEnemyMoves.SmartMove,
                   SpecialEnemyMoves.SmartMove,
                   SpecialEnemyMoves.SmartMove,
                   MoveType.TripleFire,
                   MoveType.Thunder

               ), hp: 250, renderText: "$");

            startOfTutorialEnemy = enemyDatas.Count;
            var moves = (BattleMain.MoveType[]) Enum.GetValues(typeof(BattleMain.MoveType));
            for (int i = 0; i < moves.Length; i++)
            {
                AddEnemy(ai: Actions(

                   MoveType.DoNothing,
                   moves[i],
                   MoveType.DoNothing

               ), hp: 5, renderText: "@");
            }
            //AddEnemy(ai: Actions(), hp: 3, renderText: "$");
            //AddEnemy(ai: Actions(), hp: 5, renderText: "#");

        }

        private EnemyAI Actions(params object[] obs)
        {
            var ai = new EnemyAI();
            
            foreach (var o in obs)
            {
                if (o is MoveType)
                {
                    ai.moves.Add(new MoveUse((int)o));
                    continue;
                }
                if (o is string)
                {   
                    ai.moves.Add(new MoveUse(moveCreatorProg.GetMoveId(o as string)));
                    continue;
                }
                if (o is MoveType[])
                {
                    foreach (var item in o as MoveType[])
                    {
                        ai.moves.Add(new MoveUse((int)item));
                    }
                    continue;
                }
                ai.moves.Add(o);
            }
            return ai;
        }

        private MoveType[] Moves(params MoveType[] moves)
        {
            return moves;
        }

        private void AddEnemy(EnemyAI ai, int hp, string renderText)
        {
            int render = renderTexts.Count;
            renderTexts.Add(renderText);
            enemyDatas.Add(new EnemyData(ai, hp, render));
        }

       
    }

    public class SpecialEnemyMoves
    {
        public static SpecialEnemyMoves SmartMove = new SpecialEnemyMoves(0);
        private int number;

        public SpecialEnemyMoves(int v)
        {
            this.number = v;
        }
    }
}
