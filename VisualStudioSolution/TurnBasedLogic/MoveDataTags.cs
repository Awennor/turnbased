﻿namespace Pidroh.ConsoleApp.Turnbased
{
    
    public enum MoveDataTags
    {
        HeroInitial,
        Movement,
        Shoot,
        Bomb,
        Summon,

    }
}