﻿using System;
using System.Text;
//using System.Drawing;
using System.Globalization;

namespace Pidroh.TextRendering
{
    [Serializable]
    public struct Vector2D : IEquatable<Vector2D>
    {
        #region Private Fields

        private static Vector2D zeroVector = new Vector2D(0f, 0f);
        private static Vector2D unitVector = new Vector2D(1f, 1f);
        private static Vector2D unitXVector = new Vector2D(1f, 0f);
        private static Vector2D unitYVector = new Vector2D(0f, 1f);

        #endregion Private Fields


        #region Public Fields

        public float X;
        public float Y;

        #endregion Public Fields

        # region Public Properties

        public int XInt { get { return (int)X; } }
        public int YInt { get { return (int) Y; } }

        #endregion Public Properties

        #region Constants
        #endregion


        #region Properties

        public static Vector2D Zero
        {
            get { return zeroVector; }
        }

        public static Vector2D One
        {
            get { return unitVector; }
        }

        public static Vector2D UnitX
        {
            get { return unitXVector; }
        }

        public static Vector2D UnitY
        {
            get { return unitYVector; }
        }

        #endregion Properties


        #region Constructors

        public Vector2D(float x, float y)
        {
            this.X = x;
            this.Y = y;
        }

        public Vector2D(float value)
        {
            this.X = value;
            this.Y = value;
        }

        public static Vector2D InterpolateRounded(Vector2D startPosition, Vector2D endPosition, float ratio)
        {
            return (startPosition * (1 - ratio) + endPosition * ratio).Round();
        }

        private Vector2D Round()
        {
            return new Vector2D((float)Math.Round(X), (float)Math.Round(Y));
        }

        #endregion Constructors


        #region Public Methods

        public static Vector2D Add(Vector2D value1, Vector2D value2)
        {
            value1.X += value2.X;
            value1.Y += value2.Y;
            return value1;
        }

        public static void Add(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X + value2.X;
            result.Y = value1.Y + value2.Y;
        }





        public static float Distance(Vector2D value1, Vector2D value2)
        {
            float v1 = value1.X - value2.X, v2 = value1.Y - value2.Y;
            return (float)Math.Sqrt((v1 * v1) + (v2 * v2));
        }

        public static void Distance(ref Vector2D value1, ref Vector2D value2, out float result)
        {
            float v1 = value1.X - value2.X, v2 = value1.Y - value2.Y;
            result = (float)Math.Sqrt((v1 * v1) + (v2 * v2));
        }

        public static float DistanceSquared(Vector2D value1, Vector2D value2)
        {
            float v1 = value1.X - value2.X, v2 = value1.Y - value2.Y;
            return (v1 * v1) + (v2 * v2);
        }

        public static void DistanceSquared(ref Vector2D value1, ref Vector2D value2, out float result)
        {
            float v1 = value1.X - value2.X, v2 = value1.Y - value2.Y;
            result = (v1 * v1) + (v2 * v2);
        }

        public static Vector2D Divide(Vector2D value1, Vector2D value2)
        {
            value1.X /= value2.X;
            value1.Y /= value2.Y;
            return value1;
        }

        public static void Divide(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X / value2.X;
            result.Y = value1.Y / value2.Y;
        }

        public static Vector2D Divide(Vector2D value1, float divider)
        {
            float factor = 1 / divider;
            value1.X *= factor;
            value1.Y *= factor;
            return value1;
        }

        public static void Divide(ref Vector2D value1, float divider, out Vector2D result)
        {
            float factor = 1 / divider;
            result.X = value1.X * factor;
            result.Y = value1.Y * factor;
        }

        public static float Dot(Vector2D value1, Vector2D value2)
        {
            return (value1.X * value2.X) + (value1.Y * value2.Y);
        }

        public static void Dot(ref Vector2D value1, ref Vector2D value2, out float result)
        {
            result = (value1.X * value2.X) + (value1.Y * value2.Y);
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2D)
            {
                return Equals((Vector2D)this);
            }

            return false;
        }

        public bool Equals(Vector2D other)
        {
            return (X == other.X) && (Y == other.Y);
        }

        public static Vector2D Reflect(Vector2D vector, Vector2D normal)
        {
            Vector2D result;
            float val = 2.0f * ((vector.X * normal.X) + (vector.Y * normal.Y));
            result.X = vector.X - (normal.X * val);
            result.Y = vector.Y - (normal.Y * val);
            return result;
        }

        public static void Reflect(ref Vector2D vector, ref Vector2D normal, out Vector2D result)
        {
            float val = 2.0f * ((vector.X * normal.X) + (vector.Y * normal.Y));
            result.X = vector.X - (normal.X * val);
            result.Y = vector.Y - (normal.Y * val);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }


        public float Length()
        {
            return (float)Math.Sqrt((X * X) + (Y * Y));
        }

        public float LengthSquared()
        {
            return (X * X) + (Y * Y);
        }
        
        public static Vector2D Max(Vector2D value1, Vector2D value2)
        {
            return new Vector2D(value1.X > value2.X ? value1.X : value2.X,
                               value1.Y > value2.Y ? value1.Y : value2.Y);
        }

        public static void Max(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X > value2.X ? value1.X : value2.X;
            result.Y = value1.Y > value2.Y ? value1.Y : value2.Y;
        }

        public static Vector2D Min(Vector2D value1, Vector2D value2)
        {
            return new Vector2D(value1.X < value2.X ? value1.X : value2.X,
                               value1.Y < value2.Y ? value1.Y : value2.Y);
        }

        public static void Min(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X < value2.X ? value1.X : value2.X;
            result.Y = value1.Y < value2.Y ? value1.Y : value2.Y;
        }

        public static Vector2D Multiply(Vector2D value1, Vector2D value2)
        {
            value1.X *= value2.X;
            value1.Y *= value2.Y;
            return value1;
        }

        public static Vector2D Multiply(Vector2D value1, float scaleFactor)
        {
            value1.X *= scaleFactor;
            value1.Y *= scaleFactor;
            return value1;
        }

        public static void Multiply(ref Vector2D value1, float scaleFactor, out Vector2D result)
        {
            result.X = value1.X * scaleFactor;
            result.Y = value1.Y * scaleFactor;
        }

        public static void Multiply(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X * value2.X;
            result.Y = value1.Y * value2.Y;
        }

        public static Vector2D Negate(Vector2D value)
        {
            value.X = -value.X;
            value.Y = -value.Y;
            return value;
        }

        public static void Negate(ref Vector2D value, out Vector2D result)
        {
            result.X = -value.X;
            result.Y = -value.Y;
        }

        public void Normalize()
        {
            float val = 1.0f / (float)Math.Sqrt((X * X) + (Y * Y));
            X *= val;
            Y *= val;
        }

        public static Vector2D Normalize(Vector2D value)
        {
            float val = 1.0f / (float)Math.Sqrt((value.X * value.X) + (value.Y * value.Y));
            value.X *= val;
            value.Y *= val;
            return value;
        }

        public static void Normalize(ref Vector2D value, out Vector2D result)
        {
            float val = 1.0f / (float)Math.Sqrt((value.X * value.X) + (value.Y * value.Y));
            result.X = value.X * val;
            result.Y = value.Y * val;
        }



        public static Vector2D Subtract(Vector2D value1, Vector2D value2)
        {
            value1.X -= value2.X;
            value1.Y -= value2.Y;
            return value1;
        }

        public static void Subtract(ref Vector2D value1, ref Vector2D value2, out Vector2D result)
        {
            result.X = value1.X - value2.X;
            result.Y = value1.Y - value2.Y;
        }




        public override string ToString()
        {
            CultureInfo currentCulture = CultureInfo.CurrentCulture;
            return string.Format(currentCulture, "{{X:{0} Y:{1}}}", new object[] {
                this.X.ToString(currentCulture), this.Y.ToString(currentCulture) });
        }

        #endregion Public Methods


        #region Operators

        public static Vector2D operator -(Vector2D value)
        {
            value.X = -value.X;
            value.Y = -value.Y;
            return value;
        }


        public static bool operator ==(Vector2D value1, Vector2D value2)
        {
            return value1.X == value2.X && value1.Y == value2.Y;
        }


        public static bool operator !=(Vector2D value1, Vector2D value2)
        {
            return value1.X != value2.X || value1.Y != value2.Y;
        }


        public static Vector2D operator +(Vector2D value1, Vector2D value2)
        {
            value1.X += value2.X;
            value1.Y += value2.Y;
            return value1;
        }


        public static Vector2D operator -(Vector2D value1, Vector2D value2)
        {
            value1.X -= value2.X;
            value1.Y -= value2.Y;
            return value1;
        }


        public static Vector2D operator *(Vector2D value1, Vector2D value2)
        {
            value1.X *= value2.X;
            value1.Y *= value2.Y;
            return value1;
        }


        public static Vector2D operator *(Vector2D value, float scaleFactor)
        {
            value.X *= scaleFactor;
            value.Y *= scaleFactor;
            return value;
        }


        public static Vector2D operator *(float scaleFactor, Vector2D value)
        {
            value.X *= scaleFactor;
            value.Y *= scaleFactor;
            return value;
        }


        public static Vector2D operator /(Vector2D value1, Vector2D value2)
        {
            value1.X /= value2.X;
            value1.Y /= value2.Y;
            return value1;
        }


        public static Vector2D operator /(Vector2D value1, float divider)
        {
            float factor = 1 / divider;
            value1.X *= factor;
            value1.Y *= factor;
            return value1;
        }

        #endregion Operators
    }
}