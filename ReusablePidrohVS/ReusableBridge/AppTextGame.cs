﻿using Bridge;
using Bridge.Html5;
using Newtonsoft.Json;

using Pidroh.TextRendering;

//using Pidroh.ConsoleApp.Turnbased;
using System;
using System.Text;

namespace BridgeBuild
{
    public class AppTextGame
    {
        private int buffer;
        private bool bufferOn;
        private bool bufferMouse;
        private ITextGame game;
        private TextBoard TextBoard;
        private string[] colors;
        private double lastUpdate;

        public AppTextGame(ITextGame game)
        {
            this.game = game;
        }

        public void Start(int W, int H)
        {

            //novelMain = new NovelMain().Init(50, 20);
            game.Init(W, H);
            //novelMain = new TextRenderTests().Init(W, H);
            
            Script.Call("setDisplaySize", W, H);

            //Console.WriteLine("Game Start");
            //colors = new string[20];

            //for (int i = 0; i < colors.Length; i++)
            //{
            //    //colors[i] = "#1f2026";

            //}
            //colors[1] = "#ffffff";

            var style = new HTMLStyleElement();
            style.InnerHTML = "html,body {font-family: Courier; background-color:#1f2526; height: 100%;}" + "\n #canvas-container {width: 100%; height: 100%; text-align:center; vertical-align: middle; } ";
            Document.Head.AppendChild(style);
            buffer = 9;
            bufferOn = false;

            bufferMouse = false;
            Document.OnKeyPress += (KeyboardEvent a) => {
                
                int code = a.KeyCode;
                if (code == 0) code = a.CharCode;
                buffer = code;
                bufferOn = true;
            };

            Document.OnMouseDown += (a) =>
            {
                bufferMouse = true;
            };
            
            UpdateGame();

            // After building (Ctrl + Shift + B) this project, 
            // browse to the /bin/Debug or /bin/Release folder.

            // A new bridge/ folder has been created and
            // contains your projects JavaScript files. 

            // Open the bridge/index.html file in a browser by
            // Right-Click > Open With..., then choose a
            // web browser from the list

            // This application will then run in a browser.
        }

        private void UpdateGame()
        {
            var now = Date.Now();
            var dt = now - lastUpdate;
            lastUpdate = now;


            float delta = (float)dt/1000f;
            //delta *= 200;
            

            var screen = game.ScreenHolder.Screen;
            colors = game.GetPalette().HtmlColors;
            
            game.Update(delta);
            TextBoard = screen.GetBoard();
            screen.Update(delta);
            //gr.Draw(0.033f);

            //screen.Update(delta);
            if (bufferOn)
            {
                game.ScreenHolder.Key.InputUnicode = buffer;

                bufferOn = false;
            }
            else
            {
                game.ScreenHolder.Key.InputUnicode = -1;

            }
            if (bufferMouse)
            {
                if (game.ScreenHolder.Mouse != null)
                    game.ScreenHolder.Mouse.MouseEvent(MouseEvents.MouseDown, -1, -1);
                bufferMouse = false;
            }
            else
            {
                if (game.ScreenHolder.Mouse != null)
                    game.ScreenHolder.Mouse.MouseEvent(MouseEvents.None, -1, -1);
                //Console.Write("Mouse NONE");
            }
            Script.Call("clear");
            DrawTextBoard();

            Window.SetTimeout(UpdateGame, 0);
        }

        private void DrawTextBoard()
        {
            for (int j = 0; j < TextBoard.Height; j++)
            {
                for (int i = 0; i < TextBoard.Width; i++)
                {

                    int tc = TextBoard.TextColor[i, j];
                    int tb = TextBoard.BackColor[i, j];
                    if (tc < 0) tc = 0;
                    if (tb < 0) tb = 0;
                    string color1 = colors[tc];
                    
                    string colorBack = colors[tb];
                    Script.Call("draw", i, j, color1, colorBack, "" + TextBoard.CharAt(i, j));

                }
            }
        }
    }
}