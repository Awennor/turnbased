﻿// MIT License - Copyright (C) The Mono.Xna Team
// This file is subject to the terms and conditions defined in
// file 'LICENSE.txt', which is part of this source code package.

using System;
using System.Diagnostics;
using System.Text;
using System.Runtime.Serialization;

namespace Pidroh.BaseUtils
{

    
    public struct Vector3D : IEquatable<Vector3D>
    {
        #region Private Fields

        private static Vector3D zero = new Vector3D(0f, 0f, 0f);
        private static Vector3D one = new Vector3D(1f, 1f, 1f);
        private static Vector3D unitX = new Vector3D(1f, 0f, 0f);
        private static Vector3D unitY = new Vector3D(0f, 1f, 0f);
        private static Vector3D unitZ = new Vector3D(0f, 0f, 1f);
        private static Vector3D up = new Vector3D(0f, 1f, 0f);
        private static Vector3D down = new Vector3D(0f, -1f, 0f);
        private static Vector3D right = new Vector3D(1f, 0f, 0f);
        private static Vector3D left = new Vector3D(-1f, 0f, 0f);
        private static Vector3D forward = new Vector3D(0f, 0f, -1f);
        private static Vector3D backward = new Vector3D(0f, 0f, 1f);

        #endregion Private Fields


        #region Public Fields

        
        public float X;

        
        public float Y;

        
        public float Z;

        #endregion Public Fields


        #region Properties

        /// <summary>
        /// Returns a <see>Vector3</see> with components 0, 0, 0.
        /// </summary>
        public static Vector3D Zero
        {
            get { return zero; }
        }

        /// <summary>
        /// Returns a <see>Vector3</see> with components 1, 1, 1.
        /// </summary>
        public static Vector3D One
        {
            get { return one; }
        }

        /// <summary>
        /// Returns a <see>Vector3</see> with components 1, 0, 0.
        /// </summary>
        public static Vector3D UnitX
        {
            get { return unitX; }
        }

        /// <summary>
        /// Returns a <see>Vector3</see> with components 0, 1, 0.
        /// </summary>
        public static Vector3D UnitY
        {
            get { return unitY; }
        }

        /// <summary>
        /// Returns a <see>Vector3</see> with components 0, 0, 1.
        /// </summary>
        public static Vector3D UnitZ
        {
            get { return unitZ; }
        }

        public static Vector3D Up
        {
            get { return up; }
        }

        public static Vector3D Down
        {
            get { return down; }
        }

        public static Vector3D Right
        {
            get { return right; }
        }

        public static Vector3D Left
        {
            get { return left; }
        }

        public static Vector3D Forward
        {
            get { return forward; }
        }

        public static Vector3D Backward
        {
            get { return backward; }
        }

        #endregion Properties


        #region Constructors

        public Vector3D(float x, float y, float z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }


        public Vector3D(float value)
        {
            this.X = value;
            this.Y = value;
            this.Z = value;
        }


        public Vector3D(Vector2D value, float z)
        {
            this.X = value.X;
            this.Y = value.Y;
            this.Z = z;
        }


        #endregion Constructors


        #region Public Methods

        /// <summary>
        /// Performs vector addition on <paramref name="value1"/> and <paramref name="value2"/>.
        /// </summary>
        /// <param name="value1">The first vector to add.</param>
        /// <param name="value2">The second vector to add.</param>
        /// <returns>The result of the vector addition.</returns>
        public static Vector3D Add(Vector3D value1, Vector3D value2)
        {
            value1.X += value2.X;
            value1.Y += value2.Y;
            value1.Z += value2.Z;
            return value1;
        }

        /// <summary>
        /// Performs vector addition on <paramref name="value1"/> and
        /// <paramref name="value2"/>, storing the result of the
        /// addition in <paramref name="result"/>.
        /// </summary>
        /// <param name="value1">The first vector to add.</param>
        /// <param name="value2">The second vector to add.</param>
        /// <param name="result">The result of the vector addition.</param>
        public static void Add(ref Vector3D value1, ref Vector3D value2, out Vector3D result)
        {
            result.X = value1.X + value2.X;
            result.Y = value1.Y + value2.Y;
            result.Z = value1.Z + value2.Z;
        }


        public static Vector3D Cross(Vector3D vector1, Vector3D vector2)
        {
            Cross(ref vector1, ref vector2, out vector1);
            return vector1;
        }

        public static void Cross(ref Vector3D vector1, ref Vector3D vector2, out Vector3D result)
        {
            var x = vector1.Y * vector2.Z - vector2.Y * vector1.Z;
            var y = -(vector1.X * vector2.Z - vector2.X * vector1.Z);
            var z = vector1.X * vector2.Y - vector2.X * vector1.Y;
            result.X = x;
            result.Y = y;
            result.Z = z;
        }

        public static float Distance(Vector3D vector1, Vector3D vector2)
        {
            float result;
            DistanceSquared(ref vector1, ref vector2, out result);
            return (float)Math.Sqrt(result);
        }

        public static void Distance(ref Vector3D value1, ref Vector3D value2, out float result)
        {
            DistanceSquared(ref value1, ref value2, out result);
            result = (float)Math.Sqrt(result);
        }

        public static float DistanceSquared(Vector3D value1, Vector3D value2)
        {
            float result;
            DistanceSquared(ref value1, ref value2, out result);
            return result;
        }

        public static void DistanceSquared(ref Vector3D value1, ref Vector3D value2, out float result)
        {
            result = (value1.X - value2.X) * (value1.X - value2.X) +
                     (value1.Y - value2.Y) * (value1.Y - value2.Y) +
                     (value1.Z - value2.Z) * (value1.Z - value2.Z);
        }

        public static Vector3D Divide(Vector3D value1, Vector3D value2)
        {
            value1.X /= value2.X;
            value1.Y /= value2.Y;
            value1.Z /= value2.Z;
            return value1;
        }

        public static Vector3D Divide(Vector3D value1, float value2)
        {
            float factor = 1 / value2;
            value1.X *= factor;
            value1.Y *= factor;
            value1.Z *= factor;
            return value1;
        }

        public static void Divide(ref Vector3D value1, float divisor, out Vector3D result)
        {
            float factor = 1 / divisor;
            result.X = value1.X * factor;
            result.Y = value1.Y * factor;
            result.Z = value1.Z * factor;
        }

        public static void Divide(ref Vector3D value1, ref Vector3D value2, out Vector3D result)
        {
            result.X = value1.X / value2.X;
            result.Y = value1.Y / value2.Y;
            result.Z = value1.Z / value2.Z;
        }

        public static float Dot(Vector3D vector1, Vector3D vector2)
        {
            return vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }

        public static void Dot(ref Vector3D vector1, ref Vector3D vector2, out float result)
        {
            result = vector1.X * vector2.X + vector1.Y * vector2.Y + vector1.Z * vector2.Z;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3D))
                return false;

            var other = (Vector3D)obj;
            return X == other.X &&
                    Y == other.Y &&
                    Z == other.Z;
        }

        public bool Equals(Vector3D other)
        {
            return X == other.X &&
                    Y == other.Y &&
                    Z == other.Z;
        }

        public override int GetHashCode()
        {
            return (int)(this.X + this.Y + this.Z);
        }


        public float Length()
        {
            float result;
            DistanceSquared(ref this, ref zero, out result);
            return (float)Math.Sqrt(result);
        }

        public float LengthSquared()
        {
            float result;
            DistanceSquared(ref this, ref zero, out result);
            return result;
        }




        public static Vector3D Multiply(Vector3D value1, Vector3D value2)
        {
            value1.X *= value2.X;
            value1.Y *= value2.Y;
            value1.Z *= value2.Z;
            return value1;
        }

        public static Vector3D Multiply(Vector3D value1, float scaleFactor)
        {
            value1.X *= scaleFactor;
            value1.Y *= scaleFactor;
            value1.Z *= scaleFactor;
            return value1;
        }

        public static void Multiply(ref Vector3D value1, float scaleFactor, out Vector3D result)
        {
            result.X = value1.X * scaleFactor;
            result.Y = value1.Y * scaleFactor;
            result.Z = value1.Z * scaleFactor;
        }

        public static void Multiply(ref Vector3D value1, ref Vector3D value2, out Vector3D result)
        {
            result.X = value1.X * value2.X;
            result.Y = value1.Y * value2.Y;
            result.Z = value1.Z * value2.Z;
        }

        /// <summary>
        /// Returns a <see>Vector3</see> pointing in the opposite
        /// direction of <paramref name="value"/>.
        /// </summary>
        /// <param name="value">The vector to negate.</param>
        /// <returns>The vector negation of <paramref name="value"/>.</returns>
        public static Vector3D Negate(Vector3D value)
        {
            value = new Vector3D(-value.X, -value.Y, -value.Z);
            return value;
        }

        /// <summary>
        /// Stores a <see>Vector3</see> pointing in the opposite
        /// direction of <paramref name="value"/> in <paramref name="result"/>.
        /// </summary>
        /// <param name="value">The vector to negate.</param>
        /// <param name="result">The vector that the negation of <paramref name="value"/> will be stored in.</param>
        public static void Negate(ref Vector3D value, out Vector3D result)
        {
            result.X = -value.X;
            result.Y = -value.Y;
            result.Z = -value.Z;
        }

        public void Normalize()
        {
            Normalize(ref this, out this);
        }

        public static Vector3D Normalize(Vector3D vector)
        {
            Normalize(ref vector, out vector);
            return vector;
        }

        public static void Normalize(ref Vector3D value, out Vector3D result)
        {
            float factor;
            Distance(ref value, ref zero, out factor);
            factor = 1f / factor;
            result.X = value.X * factor;
            result.Y = value.Y * factor;
            result.Z = value.Z * factor;
        }

        public static Vector3D Reflect(Vector3D vector, Vector3D normal)
        {
            // I is the original array
            // N is the normal of the incident plane
            // R = I - (2 * N * ( DotProduct[ I,N] ))
            Vector3D reflectedVector;
            // inline the dotProduct here instead of calling method
            float dotProduct = ((vector.X * normal.X) + (vector.Y * normal.Y)) + (vector.Z * normal.Z);
            reflectedVector.X = vector.X - (2.0f * normal.X) * dotProduct;
            reflectedVector.Y = vector.Y - (2.0f * normal.Y) * dotProduct;
            reflectedVector.Z = vector.Z - (2.0f * normal.Z) * dotProduct;

            return reflectedVector;
        }

        public static void Reflect(ref Vector3D vector, ref Vector3D normal, out Vector3D result)
        {
            // I is the original array
            // N is the normal of the incident plane
            // R = I - (2 * N * ( DotProduct[ I,N] ))

            // inline the dotProduct here instead of calling method
            float dotProduct = ((vector.X * normal.X) + (vector.Y * normal.Y)) + (vector.Z * normal.Z);
            result.X = vector.X - (2.0f * normal.X) * dotProduct;
            result.Y = vector.Y - (2.0f * normal.Y) * dotProduct;
            result.Z = vector.Z - (2.0f * normal.Z) * dotProduct;
        }

        /// <summary>
        /// Performs vector subtraction on <paramref name="value1"/> and <paramref name="value2"/>.
        /// </summary>
        /// <param name="value1">The vector to be subtracted from.</param>
        /// <param name="value2">The vector to be subtracted from <paramref name="value1"/>.</param>
        /// <returns>The result of the vector subtraction.</returns>
        public static Vector3D Subtract(Vector3D value1, Vector3D value2)
        {
            value1.X -= value2.X;
            value1.Y -= value2.Y;
            value1.Z -= value2.Z;
            return value1;
        }

        /// <summary>
        /// Performs vector subtraction on <paramref name="value1"/> and <paramref name="value2"/>.
        /// </summary>
        /// <param name="value1">The vector to be subtracted from.</param>
        /// <param name="value2">The vector to be subtracted from <paramref name="value1"/>.</param>
        /// <param name="result">The result of the vector subtraction.</param>
        public static void Subtract(ref Vector3D value1, ref Vector3D value2, out Vector3D result)
        {
            result.X = value1.X - value2.X;
            result.Y = value1.Y - value2.Y;
            result.Z = value1.Z - value2.Z;
        }

        internal string DebugDisplayString
        {
            get
            {
                return string.Concat(
                    this.X.ToString(), "  ",
                    this.Y.ToString(), "  ",
                    this.Z.ToString()
                );
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(32);
            sb.Append("{X:");
            sb.Append(this.X);
            sb.Append(" Y:");
            sb.Append(this.Y);
            sb.Append(" Z:");
            sb.Append(this.Z);
            sb.Append("}");
            return sb.ToString();
        }



        ///// <summary>
        ///// Transforms a vector by a quaternion rotation.
        ///// </summary>
        ///// <param name="vec">The vector to transform.</param>
        ///// <param name="quat">The quaternion to rotate the vector by.</param>
        ///// <param name="result">The result of the operation.</param>
        //        public static void Transform(ref Vector3 vec, ref Quaternion quat, out Vector3 result)
        //        {
        //		// Taken from the OpentTK implementation of Vector3
        //            // Since vec.W == 0, we can optimize quat * vec * quat^-1 as follows:
        //            // vec + 2.0 * cross(quat.xyz, cross(quat.xyz, vec) + quat.w * vec)
        //            Vector3 xyz = quat.Xyz, temp, temp2;
        //            Vector3.Cross(ref xyz, ref vec, out temp);
        //            Vector3.Multiply(ref vec, quat.W, out temp2);
        //            Vector3.Add(ref temp, ref temp2, out temp);
        //            Vector3.Cross(ref xyz, ref temp, out temp);
        //            Vector3.Multiply(ref temp, 2, out temp);
        //            Vector3.Add(ref vec, ref temp, out result);
        //        }




        #endregion Public methods


        #region Operators

        public static bool operator ==(Vector3D value1, Vector3D value2)
        {
            return value1.X == value2.X
                && value1.Y == value2.Y
                && value1.Z == value2.Z;
        }

        public static bool operator !=(Vector3D value1, Vector3D value2)
        {
            return !(value1 == value2);
        }

        public static Vector3D operator +(Vector3D value1, Vector3D value2)
        {
            value1.X += value2.X;
            value1.Y += value2.Y;
            value1.Z += value2.Z;
            return value1;
        }

        public static Vector3D operator -(Vector3D value)
        {
            value = new Vector3D(-value.X, -value.Y, -value.Z);
            return value;
        }

        public static Vector3D operator -(Vector3D value1, Vector3D value2)
        {
            value1.X -= value2.X;
            value1.Y -= value2.Y;
            value1.Z -= value2.Z;
            return value1;
        }

        public static Vector3D operator *(Vector3D value1, Vector3D value2)
        {
            value1.X *= value2.X;
            value1.Y *= value2.Y;
            value1.Z *= value2.Z;
            return value1;
        }

        public static Vector3D operator *(Vector3D value, float scaleFactor)
        {
            value.X *= scaleFactor;
            value.Y *= scaleFactor;
            value.Z *= scaleFactor;
            return value;
        }

        public static Vector3D operator *(float scaleFactor, Vector3D value)
        {
            value.X *= scaleFactor;
            value.Y *= scaleFactor;
            value.Z *= scaleFactor;
            return value;
        }

        public static Vector3D operator /(Vector3D value1, Vector3D value2)
        {
            value1.X /= value2.X;
            value1.Y /= value2.Y;
            value1.Z /= value2.Z;
            return value1;
        }

        public static Vector3D operator /(Vector3D value, float divider)
        {
            float factor = 1 / divider;
            value.X *= factor;
            value.Y *= factor;
            value.Z *= factor;
            return value;
        }

        #endregion
    }
}