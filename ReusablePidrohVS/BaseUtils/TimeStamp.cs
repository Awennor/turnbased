﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.BaseUtils
{
    public class TimeStamp
    {
        public float CurrentSnap;

        public TimeStampSnap GetSnap()
        {
            return new TimeStampSnap(CurrentSnap);
        }
        
        public void Advance(float delta)
        {
            CurrentSnap += delta;
        }
    }

    public struct TimeStampSnap
    {
        public readonly float TimeSnap;

        public TimeStampSnap(float snap)
        {
            TimeSnap = snap;
        }
    }
}
