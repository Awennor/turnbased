﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Pidroh.BaseUtils
{
    public class Debugger
    {
        bool debugging;
        int ident;
        StringBuilder stringBuilder = new StringBuilder();

        public static bool ActiveStatic { get; set; } = true;

        public Debugger(bool debugging)
        {
            if(ActiveStatic)
                this.debugging = debugging;
        }

        public void Print(string s)
        {
            if (!debugging) return;
            for (int i = 0; i < ident; i++)
            {
                Console.Write(' ');
            }
            Console.WriteLine(s);
        }

        internal void Deident()
        {
            ident = ident - 2; ;
        }

        internal void Ident()
        {
            ident = ident+2;
        }

        internal void Active()
        {
            if (!ActiveStatic) return;
            debugging = true;
        }

        internal void Active(bool v)
        {
            if (!ActiveStatic) return;
            debugging = v;
        }

        public void Print(Object obj)
        {
            if (!debugging) return;
            stringBuilder.Length = 0;
            
            var type = obj.GetType();
            stringBuilder.Append("Type: ");
            stringBuilder.Append(type.Name);
            stringBuilder.AppendLine();
            var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var f in fields)
            {
                stringBuilder.Append(' ');
                stringBuilder.Append(' ');
                stringBuilder.Append(f.GetValue(obj));
                stringBuilder.Append(' ');
                stringBuilder.Append(' ');
                stringBuilder.Append(f.Name);
                stringBuilder.AppendLine();
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
