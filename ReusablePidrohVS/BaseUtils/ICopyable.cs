﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.BaseUtils
{
    public interface ICopyable<T>
    {
        void CopyTo(T target);
    }


}
