﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.BaseUtils
{
    static public class RandomSupplier
    {
        public static Func<float> Generate{ get; set; }

        public static int Range(int min, int max) {
            return (int) ((Generate() * (max-min)+min)+0.5f);
        }

        public static T RandomElement<T>(T[] array)
        {
            return array[Range(0, array.Length-1)];
        }

        internal static int Next(int count)
        {
            return Range(0, count);
        }
    }
}
