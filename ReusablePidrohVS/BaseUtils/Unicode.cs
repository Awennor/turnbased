﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.BaseUtils
{
    public static class Unicode
    {
        public const int Space = 32;
        public const int Enter = 13;
        public const int Backspace = 8;

        public const int keyDown = 40;
        public const int keyLeft = 37;
        public const int keyUp = 38;
        public const int keyRight = 39;

        public const char Uparrow2 = (char)24;
        public const char Downarrow2 = (char)25;
        public const char Rightarrow2 = (char)26;
        public const char Leftarrow2 = (char)27;
        public const char Uparrow = (char)30;
        public const char Downarrow = (char)31;
        public const char Leftarrow = (char)17;
        public const char Rightarrow = (char)16;
        public const char AsciiGridHor = (char)196;
        public const char AsciiGridVer = (char)179;

        public static readonly char[] grids = new char[] {
            AsciiGridHor,
            AsciiGridVer
        };
        public static readonly char AsciiGridUpLeft = (char)217;
        public static readonly char AsciiGridUpRight = (char) 192;
        public static readonly char AsciiGridUpRightLeft = (char)193;
        public static readonly char AsciiGridDownLeft = (char)191;
        public static readonly char AsciiGridDownRight = (char)218;
        public static readonly char AsciiGridDownRightLeft = (char)194;
        public static readonly int Escape = (char)27;
    }
}
