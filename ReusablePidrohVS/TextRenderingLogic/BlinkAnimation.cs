﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.TextRendering
{
    public class BlinkAnim : TextAnimation<BlinkAnim.BlinkData>
    {
        public override void Modify(TextEntity entity, BlinkData mainData, float progress, float length)
        {
            base.Modify(entity, mainData, progress, length);
            float aux = progress;
            bool blink = true;
            while (true)
            {
                if (blink)
                {
                    aux -= mainData.blinkActiveTime;
                }
                else
                {
                    aux -= mainData.blinkInactive;
                }
                if (aux < 0)
                {
                    break;
                }
                else
                {
                    blink = !blink;
                }
            }
            if (!blink)
            {
                if (mainData.changeInvisible)
                {
                    entity.Animation.SetAll(mainData.text, mainData.textColor, mainData.backColor);
                }
                else {
                    entity.Animation.SetAllIfVisible(mainData.text, mainData.textColor, mainData.backColor);
                }
                
            }
        }


        public struct BlinkData
        {
            public readonly char text;
            public readonly int backColor, textColor;
            public readonly float blinkActiveTime;
            public readonly float blinkInactive;
            public readonly bool changeInvisible;

            public BlinkData(char text, int backColor, int textColor, float blinkActiveTime, float blinkInactive, bool changeNoChangeColor = true)
            {
                this.text = text;
                this.backColor = backColor;
                this.textColor = textColor;
                this.blinkActiveTime = blinkActiveTime;
                this.blinkInactive = blinkInactive;
                this.changeInvisible = true;
            }

            public static BlinkData BackColor(int backColor, float blinkDuration, bool changeNoChangeColor = true)
            {
                return new BlinkData(TextBoard.NOCHANGECHAR, backColor, TextBoard.NOCHANGECOLOR, blinkDuration, blinkDuration, changeNoChangeColor);
            }

            public static BlinkData FrontColor(int frontColor, float blinkDuration)
            {
                return new BlinkData(TextBoard.NOCHANGECHAR, TextBoard.NOCHANGECOLOR, frontColor,  blinkDuration, blinkDuration);
            }

            public static BlinkData Char(char c, float blinkDuration)
            {
                return new BlinkData(c, TextBoard.NOCHANGECOLOR, TextBoard.NOCHANGECOLOR, blinkDuration, blinkDuration);
            }
        }
    }
}
