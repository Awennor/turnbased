﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering
{
    public class Palette
    {
        public string[] HtmlColors;


        public Palette(params string[] colors)
        {
            HtmlColors = colors;
        }
    }

    public class DefaultPalettes
    {
        public static Palette C4KiroKaze = new Palette("#332c50", "#46878f", "#94e344", "#e2f3e4");
        public static Palette C4Reader = new Palette("#262626", "#8b8cba", "#8bba91", "#649f8d");
        public static Palette C4Novel = new Palette("#262626", "#342d41", "#b8b8b8", "#8b8cba");
        public const int C4Black = 0;
        public const int C4BlackNeutral = 1;
        public const int C4WhiteNeutral = 2;
        public const int C4White = 3;

    }
}
