﻿namespace Pidroh.TextRendering
{
    public class CharByCharAnimation : TextAnimation<CharByCharAnimation.CharByCharData>
    {
        public override void Modify(TextEntity entity, CharByCharData mainData, float progress, float length)
        {
            base.Modify(entity, mainData, progress, length);
            float ratio = progress / length;
            float lengthText = mainData.charEnd - mainData.charStart;
            int lineBreaks = 0;
            int offsetedPerm = 0;
            for (int i = mainData.charStart; i < mainData.charEnd; i++)
            {
                int offseted = i + offsetedPerm;
                int line = 0;
                var tb = entity.Animation;
                

                while (offseted >= tb.Width)
                {
                    line++;
                    offseted -= tb.Width;
                }
                if (entity.Origin.CharAt(offseted, line + lineBreaks) == '\n')
                {
                    lineBreaks++;
                    offsetedPerm -= offseted;
                    offseted = 0;
                }
                if (i > ((lengthText * ratio) + mainData.charStart))
                {
                    tb.DrawChar(' ', offseted, line + lineBreaks);
                    //tb.Draw("" + i, 6, 5, 1);

                }
            }
        }


        public class CharByCharData
        {
            internal int charStart;
            internal int charEnd;

            public CharByCharData(int charStart, int charEnd)
            {
                this.charStart = charStart;
                this.charEnd = charEnd;
            }
        }
    }
}
