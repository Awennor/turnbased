﻿using Pidroh.BaseUtils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.TextRendering
{
    public class TextWorld
    {
        public Palette palette = DefaultPalettes.C4KiroKaze;
        List<TextEntity> activeAgents = new List<TextEntity>();
        List<TextEntity> freeBoards = new List<TextEntity>();
        List<TextAnimation> animations = new List<TextAnimation>();
        public TextBoard mainBoard;
        int latestId = -1;

        public T AddAnimation<T>(T ta) where T : TextAnimation
        {
            animations.Add(ta);
            ta.RegisterLists();
            return ta;
        }

        public void Init(int width, int height)
        {
            mainBoard = new TextBoard(width, height);

        }

        public void Draw()
        {
            mainBoard.Reset();
            DrawChildren();
        }

        public void DrawChildren()
        {
            for (int i = 0; i < activeAgents.Count; i++)
            {
                activeAgents[i].ResetAnimation();
                foreach (var anim in animations)
                {
                    anim.Modify(activeAgents[i]);
                }
                if (activeAgents[i].freeIfIdle && !activeAgents[i].animating)
                {
                    freeBoards.Add(activeAgents[i]);
                    activeAgents.Remove(activeAgents[i]);
                    i--;
                }
                else
                {
                    mainBoard.Insert(activeAgents[i].Animation);
                }

            }
        }

        public TextEntity GetFreeEntity(int w, int h)
        {
            TextEntity te;
            if (freeBoards.Count > 0)
            {
                te = freeBoards[freeBoards.Count - 1];
                freeBoards.RemoveAt(freeBoards.Count - 1);
            }
            else
            {
                te = new TextEntity();
                te.id = ++latestId;

            }

            activeAgents.Add(te);
            te.freeIfIdle = false;
            te.SetSize(w, h);
            te.ResetFull();
            return te;
        }

        public TextEntity GetTempEntity(int w, int h)
        {
            var te = GetFreeEntity(w, h);
            te.freeIfIdle = true;
            return te;
        }

        public void AdvanceTime(float v)
        {
            foreach (var anim in animations)
            {
                anim.Update(v);
            }
        }

        public void TryEndAnimations()
        {
            foreach (var anim in animations)
            {
                anim.TryEnd();
            }
        }

        internal void Free(List<TextEntity> entities)
        {
            
            freeBoards.AddRange(entities);
            foreach (var item in entities)
            {
                activeAgents.Remove(item);
            }
        }

        public bool IsDone()
        {
            foreach (var anim in animations)
            {
                if (!anim.IsDone()) return false;
            }
            return true;
        }
    }

    public class TextEntity
    {
        public int id;
        public TextBoard Origin;
        public TextBoard Animation;
        public bool freeIfIdle = false;
        internal bool animating;

        public int Height { get { return Origin.Height; } }
        public int Width { get { return Origin.Width; } }

        public TextAnimation.BaseData AnimBase(float length)
        {
            return new TextAnimation.BaseData(length, 0, id);
        }

        internal Vector2D GetPosition()
        {
            return Origin.Position;
        }

        internal void ResetAnimation()
        {
            animating = false;
            Animation.Set(Origin);
        }

        internal void ResetFull()
        {
            Origin.ResetInvisible();
        }

        internal void SetPosition(int x, int y)
        {
            Origin.Position = new Vector2D(x,y);
        }

        internal void SetPosition(Vector2D vector2D)
        {
            Origin.Position = vector2D;
        }

        internal void SetPosition(Point2D point)
        {
            Origin.Position = new Vector2D(point.X, point.Y);
        }

        internal void SetSize(int w, int h)
        {
            if (Origin == null)
            {
                Origin = new TextBoard(w, h);
                Animation = new TextBoard(w, h);
            }
            Origin.Resize(w, h);
            Animation.Resize(w, h);

        }
    }

    public class DelaysAnimation : TextAnimation
    {
        protected override void RequestRegisterLists()
        {
            
        }

        internal void Delay(float v)
        {
            Add(new BaseData(v, 0, -1));
        }
    }


    public class PositionAnimation : TextAnimation<PositionAnimation.PositionData>
    {

        public override void Modify(TextEntity entity, PositionData mainData, float progress, float length)
        {
            base.Modify(entity, mainData, progress, length);
            TextBoard target = entity.Animation;
            if (mainData.permanent)
                target = entity.Origin;
            target.Position = Vector2D.InterpolateRounded(mainData.startPosition, mainData.endPosition, progress / length);

        }

        public struct PositionData
        {
            public readonly bool permanent;
            public readonly Vector2D startPosition, endPosition;

            public PositionData(Vector2D startPosition, Vector2D endPosition, bool perm = false)
            {
                this.startPosition = startPosition;
                this.endPosition = endPosition;
                this.permanent = perm;
            }
        }
    }

    public abstract class TextAnimation<T> : TextAnimation
    {
        protected List<T> mainData = new List<T>();
        protected override void RequestRegisterLists()
        {
            base.RegisterList(mainData);
        }

        public void Add(BaseData baseData, T mainD)
        {
            base.Add(baseData);
            mainData.Add(mainD);
        }

        public override void Modify(TextEntity entity, int index, float progress, float length)
        {
            Modify(entity, mainData[index], progress, length);
        }

        public virtual void Modify(TextEntity entity, T mainData, float progress, float length)
        {
        }

        //internal override void Execute(int index, BaseData baseData)
        //{
        //    this.Execute(mainData[index], baseData);
        //}

        //public abstract void Execute(T mainData, BaseData baseData);
    }

    public abstract class TextAnimation
    {

        public struct BaseData
        {
            public readonly float length;
            public readonly float progress;
            public readonly int target;

            public BaseData(float length, float progress, int target)
            {
                this.length = length;
                this.progress = progress;
                this.target = target;
            }
        }
        List<float> length = new List<float>();
        List<float> progress = new List<float>();
        List<int> targets = new List<int>();
        List<IList> lists = new List<IList>();

        public void RegisterLists()
        {
            lists.Add(length);
            lists.Add(progress);
            lists.Add(targets);
            RequestRegisterLists();
        }

        protected abstract void RequestRegisterLists();

        public void Update(float delta)
        {
            for (int i = 0; i < progress.Count; i++)
            {
                progress[i] += delta;
                if (progress[i] >= length[i])
                {
                    progress[i] = length[i];
                }
                else
                {
                    //Execute(i, new BaseData(length[i],progress[i], targets[i]));
                }
            }
        }

        //internal abstract void Execute(int index, BaseData baseData);

        internal void Add(BaseData bd)
        {
            progress.Add(bd.progress);
            targets.Add(bd.target);
            length.Add(bd.length);
        }

        public bool IsDone()
        {
            foreach (var item in lists)
            {
                if (item.Count != progress.Count)
                {
                    string s = null;
                    s.Trim();
                }
            }
            return progress.Count == 0;
        }

        internal void EndTask(int i)
        {
            foreach (var l in lists)
            {

                l.RemoveAt(i);
            }
        }

        internal void RegisterList(IList mainData)
        {
            lists.Add(mainData);
        }

        internal void Modify(TextEntity a)
        {
            for (int i = 0; i < progress.Count; i++)
            {
                if (a.id == targets[i])
                {
                    Modify(a, i, progress[i], length[i]);
                    a.animating = true;
                }
            }
        }

        public virtual void Modify(TextEntity entity, int index, float progress, float length)
        {
        }

        internal void TryEnd()
        {
            for (int i = 0; i < progress.Count; i++)
            {
                if (progress[i] >= length[i])
                {
                    EndTask(i);
                }
            }
            
        }
    }
}
