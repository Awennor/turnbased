﻿using Pidroh.TextRendering;
using Pidroh.TextRendering.GameScreen;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering
{
    public class TextScreenN : ITextScreen, IMouseInput, IKeyboardInput
    {
        public TextWorld TextWorld;

        public virtual void Update(float f) { }

        public TextScreenN()
        {
        }

        public TextScreenN(TextWorld textWorld)
        {
            TextWorld = textWorld;
        }

        public  void Init(int w, int h)
        {
            TextWorld = new TextWorld();
            TextWorld.Init(w, h);

        }

        public TextBoard GetBoard()
        {
            return TextWorld.mainBoard;
        }



        public void MouseEvent(MouseEvents mouseDown, int v1, int v2)
        {
            
        }

        public int InputUnicode { set; get; }
        public int InputAsNumber
        {
            get
            {
                return InputUnicode - 48;
            }
        }

        public MouseIO mouseIO { get; } = new MouseIO();
    }

    public interface ITextScreen
    {
        
        TextBoard GetBoard();
        
        void Update(float f);
        
    }

    public interface IMouseInput
    {
        void MouseEvent(MouseEvents eventType, int v1, int v2);
        MouseIO mouseIO { get; }
    }

    public interface IKeyboardInput
    {
        int InputUnicode { set; }
    }

    public enum MouseEvents
    { 
        MouseDown,
        None
    }

    public class TextScreenHolder
    {
        public ITextScreen Screen { get; set; }
        public IMouseInput Mouse { get; set; }
        public IKeyboardInput Key { get; set; }

        internal void SetAll(object dns)
        {
            Screen = dns as ITextScreen;
            Mouse = dns as IMouseInput;
            Key = dns as IKeyboardInput;
        }
    }
}
