﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering.GameScreen
{
    public class UnicodeRemap
    {

        Dictionary<int, int> remaps = new Dictionary<int, int>();

        public UnicodeRemap()
        {
            remaps.Add(Unicode.keyUp, 'w');
            remaps.Add(Unicode.keyDown, 's');
            remaps.Add(Unicode.keyLeft, 'a');
            remaps.Add(Unicode.keyRight, 'd');

            remaps.Add(Unicode.Enter, Unicode.Space);

            remaps.Add('i', '1');

        }

        public int Remap(int unicode)
        {
            int result;
            if (remaps.TryGetValue(unicode, out result))
            {
                return result;
            }
            return unicode;
        }
    }
}
