﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering.GameScreen
{
    public struct MouseHover
    {
        public Rect rect;
        public int type;
        public int id;

        public MouseHover(Rect rect, int type, int id)
        {
            this.rect = rect;
            this.type = type;
            this.id = id;
        }
    }

    public class MouseHoverManager
    {
        public List<MouseHover> mouseHovers = new List<MouseHover>();
        public List<MouseHover> mouseHoversActive = new List<MouseHover>();
        public List<MouseHover> mouseHoversClickDown = new List<MouseHover>();
        public MouseIO mouseIO;

        public MouseHoverManager(MouseIO mouseIO)
        {
            this.mouseIO = mouseIO;
        }

        public void Update()
        {
            mouseHoversActive.Clear();
            mouseHoversClickDown.Clear();
            foreach (var item in mouseHovers)
            {
                if (item.rect.Contains(mouseIO.pos))
                {
                    mouseHoversActive.Add(item);
                    if (mouseIO.mouseClickHappen)
                    {
                        mouseHoversClickDown.Add(item);
                    }
                }
            }
        }
    }


}
