﻿using Pidroh.TextRendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering
{
    public interface ITextGame
    {
        TextScreenHolder ScreenHolder { get; }

        void Init(int w, int h);
        void Update(float delta);
        Palette GetPalette();
    }
}
