﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pidroh.TextRendering
{
    public class TestGame : ITextGame
    {
        public TextScreenHolder ScreenHolder { get; } = new TextScreenHolder();
        public Palette palette = DefaultPalettes.C4Novel;

        public Palette GetPalette()
        {
            return palette;
        }

        public void Init(int w, int h)
        {
            TextScreenN screen = new TestScreen();
            ScreenHolder.SetAll(screen);
            screen.Init(w, h);
            screen.GetBoard().Draw("Test", 0,0, 2);
        }

        public void Update(float delta)
        {
            
        }
    }

    public class TestScreen : TextScreenN
    {
        public override void Update(float f)
        {
            
        }
    }
}
