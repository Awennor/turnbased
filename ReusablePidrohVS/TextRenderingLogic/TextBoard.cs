﻿using Pidroh.BaseUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pidroh.TextRendering
{
    public class TextBoard
    {
        public const char NOCHANGECHAR = (char)1;
        public const char INVISIBLECHAR = (char)2;
        public const int NOCHANGECOLOR = -2;
        public const int INVISIBLECOLOR = -1;
        char[,] chars;
        public int[,] TextColor { get; private set; }
        public int[,] BackColor { get; private set; }
        //StringBuilder stringBuilder = new StringBuilder();
        int cursorX = 0;
        int cursorY = 0;
        public Vector2D Position { get; set; }


        public TextBoard(int width, int height)
        {
            //SetMaxSize(width, height);
            Resize(width, height);
        }

        public void DrawOnCenter(string message, int color, int xOff = 0, int yOff = 0, bool alignString = true)
        {
            int x = (Width) / 2;
            if (alignString) x -= message.Length / 2;
            int y = Height / 2;
            Draw(message, x + xOff, y + yOff, color);
        }

        public void DrawOnCenterHorizontal(string message, int color, int xOff = 0, int y = 0, bool alignString = true)
        {
            int x = (Width) / 2;
            if (alignString) x -= message.Length / 2;
            Draw(message, x + xOff, y, color);
        }


        private void SetMaxSize(int width, int height)
        {
            chars = new char[width, height];
            TextColor = new int[width, height];
            BackColor = new int[width, height];
        }

        public void Reset()
        {
            DrawRepeated(' ', 0, 0, Width, Height, 0, 0);
        }

        public void ResetInvisible()
        {
            DrawRepeated(INVISIBLECHAR, 0, 0, Width, Height, INVISIBLECOLOR, INVISIBLECOLOR);
        }


        public int Width { get; private set; }

        public void Insert(TextBoard secondBoard)
        {
            for (int i = 0; i < secondBoard.Width; i++)
            {
                for (int j = 0; j < secondBoard.Height; j++)
                {
                    int x = (int)secondBoard.Position.X + i;
                    int y = (int)secondBoard.Position.Y + j;
                    if (x < 0 || y < 0) continue;
                    if (x >= Width || y >= Height) continue;
                    if (secondBoard.chars[i, j] != INVISIBLECHAR)
                        chars[x, y] = secondBoard.chars[i, j];
                    if (secondBoard.TextColor[i, j] != INVISIBLECOLOR)
                        TextColor[x, y] = secondBoard.TextColor[i, j];
                    if (secondBoard.BackColor[i, j] != INVISIBLECOLOR)
                        BackColor[x, y] = secondBoard.BackColor[i, j];
                }
            }
        }

        internal void DrawHorizontalLine(char c, int y, int color)
        {
            DrawRepeated(c, 0, y, Width, 1, color);
        }

        internal void DrawRect(char c, int x, int y, int w, int h, int textColor, int backColor)
        {
            DrawRepeated(c, x,      y,   1, h, textColor, backColor);
            DrawRepeated(c, x+w-1,  y,   1, h, textColor, backColor);
            DrawRepeated(c, x,      y,   w, 1, textColor, backColor);
            DrawRepeated(c, x,      y+h-1, w, 1, textColor, backColor);
        }

        public int Height { get; private set; }
        public int CursorX
        {
            get { return cursorX; }
            set
            {
                cursorX = value;
            }
        }
        public int CursorY { get { return cursorY; }
            set
            {
                cursorY = value;
            }
        }

        public void DrawOneDigit(int i, int x, int y, int color = NOCHANGECOLOR, int background = NOCHANGECOLOR)
        {
            char c = (char)(i + '0');
            DrawChar(c, x, y, color, background);
        }

        public void DrawTwoDigits(int i, int x, int y, int color = NOCHANGECOLOR, int background = NOCHANGECOLOR)
        {
            DrawOneDigit(i/10,x,y,color,background);
            DrawOneDigit(i %10, x+1, y, color, background);
        }

        internal bool SameAs(TextBoard textBoard, int x, int y)
        {
            return this.chars[x, y] == textBoard.chars[x, y]
                && this.BackColor[x,y] == textBoard.BackColor[x,y]
                && this.TextColor[x,y] == textBoard.TextColor[x,y];
        }

        internal void Copy(TextBoard textBoard, int x, int y)
        {
            this.chars[x, y] = textBoard.chars[x, y];
            this.TextColor[x, y] = textBoard.TextColor[x, y];
            this.BackColor[x, y] = textBoard.BackColor[x, y];
        }

        internal void Draw_Cursor_UnicodeLabel(int v, int color)
        {
            int len = DrawUnicodeLabel(v, cursorX, cursorY, color);
            for (int i = 0; i < len; i++)
            {
                AdvanceCursor();
            }
            
        }

        internal int DrawUnicodeLabel(int unicode, int x, int y, int color)
        {
            if (unicode >= 'a' && unicode <= 'z') {
                DrawChar((char)(unicode + 'A' - 'a'), x, y, color);
                return 1;
            }
            if (unicode >= '0' && unicode <= '9')
            {
                DrawChar((char)(unicode), x, y, color);
                return 1;
            }
            string label = "";
            if (unicode == 32)
            {
                label = "SPACE";
            }
            if (unicode == Unicode.Escape)
            {
                label = "ESCAPE";
            }
            Draw(label, x, y, color);
            return label.Length;
        }

        internal void Set(TextBoard origin)
        {
            this.Position = origin.Position;
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    this.chars[i, j] = origin.chars[i, j];
                    this.BackColor[i, j] = origin.BackColor[i, j];
                    this.TextColor[i, j] = origin.TextColor[i, j];
                }
            }
        }

        internal void Resize(int w, int h)
        {
            if (chars == null || w > chars.GetLength(0) || h > chars.GetLength(1))
            {
                SetMaxSize(w, h);
            }
            Width = w;
            Height = h;

        }

        public char CharAt(int i, int j)
        {
            return chars[i, j];
        }

        public void SetCursorAt(int x, int y)
        {
            cursorX = x;
            cursorY = y;
        }

        public void Draw_Cursor(string v)
        {
            foreach (var c in v)
            {
                Draw_Cursor(c);
            }
        }

        public void Draw_Cursor(string v, int color, int backColor = NOCHANGECOLOR)
        {
            foreach (var c in v)
            {
                Draw_Cursor(c, color, backColor);
            }
        }

        internal bool CanDraw_Cursor_SmartLineBreak(string v)
        {
            int currentX = cursorX;
            int currentY = cursorY;

            for (int i = 0; i < v.Length; i++)
            {
                bool lineBreak = false;
                bool shouldCheckForLineBreaks = (i == 0 || v[i] == ' ') && i != v.Length - 1;
                if (shouldCheckForLineBreaks)
                {
                    for (int j = 1; j < v.Length - i; j++)
                    {
                        if (j + currentX >= Width) //reach end of the line without ending the word, should line break
                        {
                            if (v[i] == ' ')
                            {
                                i++; //skip through the space if it's a new line
                            }
                            lineBreak = true;
                            break;
                        }
                        if (v[i + j] == ' ') //new word begins so no need to line break
                        {
                            break;
                        }
                    }
                }
                if (lineBreak)
                {
                    currentY++;
                    currentX = 0;
                }
                currentX++;
                if (currentX >= Width)
                {
                    currentY++;
                    currentX = 0;
                }
                if (currentX >= Width || currentY >= Height) return false;
                
                
            }
            return true;
        }

        public DrawCursorResult Draw_Cursor_SmartLineBreak(string v, int color)
        {
            int offStart = 0;
            int offEnd = v.Length - 1;
            return Draw_Cursor_SmartLineBreak(v, color, offStart, offEnd);
        }

        public DrawCursorResult Draw_Cursor_SmartLineBreak(string v, int color, int offStart, int offEnd, int xNewline = 0)
        {
            
            Vector2D start = new Vector2D(CursorX, CursorY);
            int endIndex = offEnd + 1;
            for (int i = offStart; i < endIndex; i++)
            {
                int originX = cursorX;
                bool lineBreak = false;
                bool shouldCheckForLineBreaks = (i == 0 || v[i] == ' ') && i != endIndex - 1;
                if (shouldCheckForLineBreaks)
                {
                    for (int j = 1; j < endIndex - i; j++)
                    {
                        if (j + originX >= Width) //reach end of the line without ending the word, should line break
                        {
                            if (v[i] == ' ')
                            {
                                i++; //skip through the space if it's a new line
                            }
                            lineBreak = true;
                            break;
                        }
                        if (v[i + j] == ' ') //new word begins so no need to line break
                        {
                            break;
                        }
                    }
                }
                if (lineBreak)
                {
                    CursorNewLine(xNewline);
                }
                Draw_Cursor(v[i], color);
            }
            Vector2D end = new Vector2D(CursorX, CursorY);
            return new DrawCursorResult(PositionToIndex(start), PositionToIndex(end), start, end);
        }

        internal void AutoFixGridding()
        {
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    if (IsGrid(i, j))
                    {
                        int mask = 0;
                        if (IsGrid(i - 1, j))
                        {
                            mask += 1;
                        }
                        if (IsGrid(i + 1, j))
                        {
                            mask += 2;
                        }
                        if (IsGrid(i, j - 1))
                        {
                            mask += 4;
                        }
                        if (IsGrid(i, j + 1))
                        {
                            mask += 8;
                        }
                        switch (mask)
                        {
                            case 1:
                            case 2:
                            case 3:
                                chars[i, j] = Unicode.AsciiGridHor;
                                break;
                            case 4:
                            case 8:
                            case 12:
                                chars[i, j] = Unicode.AsciiGridVer;
                                break;
                            case 5:
                                chars[i, j] = Unicode.AsciiGridUpLeft;
                                break;
                            case 6:
                                chars[i, j] = Unicode.AsciiGridUpRight;
                                break;
                            case 7:
                                chars[i, j] = Unicode.AsciiGridUpRightLeft;
                                break;
                            case 9:
                                chars[i, j] = Unicode.AsciiGridDownLeft;
                                break;
                            case 10:
                                chars[i, j] = Unicode.AsciiGridDownRight;
                                break;
                            case 11:
                                chars[i, j] = Unicode.AsciiGridDownRightLeft;
                                break;
                            default:
                                break;
                        }
                    }
                    

                }
            }
        }

        private bool IsGrid(int x, int y)
        {
            if(x <0 || y <0 || x>= Width || y>= Height){
                return false;
            }
            char c = chars[x, y];
            foreach (var item in Unicode.grids)
            {
                if (c == item) return true;
            }
            return false;
        }

        internal void DrawLines(int hero, params Vector2D[] points)
        {
            for (int i = 0; i < points.Length-1; i++)
            {
                DrawLine(points[i], points[i+1], hero);
            }
            
        }

        private void DrawLine(Vector2D pos1, Vector2D pos2, int color)
        {
            char c = Unicode.AsciiGridHor;
            if (pos1.Y != pos2.Y) c = Unicode.AsciiGridVer;
            int height = pos2.YInt - pos1.YInt;
            //if (height <= 0) height = 1;
            int width = pos2.XInt - pos1.XInt;
            //if (width <= 0) width = 1;
            DrawRepeated(c, pos1.XInt, pos1.YInt, width+1, height+1, color);
        }

        private int PositionToIndex(Vector2D start)
        {
            return (int)(start.X + start.Y * Width);
        }

        public void DrawOneDigit_Cursor(int i)
        {
            Draw_Cursor((char)(i + '0'));
        }

        public void Draw_Cursor(char c)
        {

            DrawChar(c, cursorX, cursorY);
            AdvanceCursor();
        }

        public void Draw_Cursor(char c, int color, int backColor= NOCHANGECOLOR)
        {

            DrawChar(c, cursorX, cursorY, color, backColor);
            AdvanceCursor();
        }


        public void AdvanceCursor()
        {
            cursorX++;
            if (cursorX >= Width)
            {
                cursorX = 0;
                cursorY++;
            }
        }

        public void CursorNewLine(int x)
        {
            cursorY++;
            cursorX = x;
        }

        public void DrawChar(char v, int x, int y)
        {
            
            if (v != NOCHANGECHAR) {
                chars[x, y] = v;

            }
                
        }

        public void DrawChar(char v, int x, int y, int color, int backColor = NOCHANGECOLOR)
        {

            DrawChar(v, x, y);
            SetColor(color, x, y);
            SetBackColor(backColor, x, y);
        }

        internal void SetAll(char text, int textColor = NOCHANGECOLOR, int backColor= NOCHANGECOLOR)
        {
            DrawRepeated(text, 0, 0, Width, Height, textColor, backColor);
        }

        internal void SetAllIfVisible(char text, int textColor = NOCHANGECOLOR, int backColor = NOCHANGECOLOR)
        {
            DrawRepeatedIfVisible(text, 0, 0, Width, Height, textColor, backColor);
        }



        public void DrawWithGrid(string text, int x, int y, int gridColor, int textColor)
        {
            int width = text.Length;
            DrawGrid(x, y, width + 2, 3, gridColor);
            Draw(text, x + 1, y + 1, textColor);
        }

        public void Draw(string v, int x, int y, int color, int backColor = NOCHANGECOLOR)
        {
            for (int i = 0; i < v.Length; i++)
            {
                int x2 = x + i;
                int y2 = y;
                if(x2 >= Width)
                {
                    x2 -= Width;
                    y2++;
                }
                DrawChar(v[i], x2, y2, color, backColor);
            }
        }

        public void DrawWithLinebreaks(string v, int x, int y, int newlineX, int color, int backColor = NOCHANGECOLOR)
        {
            int linebreaks = 0;
            int xOffsetnewlines = 0;
            for (int i = 0; i < v.Length; i++)
            {
                int x2 = x + i+ xOffsetnewlines;
                int y2 = y;
                
                while (x2 >= Width)
                {
                    x2 = x2-Width+newlineX;
                    y2++;
                }

                //
                if (v[i] == '\n')
                {
                    linebreaks++;
                    xOffsetnewlines += newlineX - x2-1;
                }
                else{
                    DrawChar(v[i], x2, y2 + linebreaks, color, backColor);
                }
            }
        }


        public void Draw(IEnumerable<char> v, int x, int y, int color, int backColor = NOCHANGECOLOR)
        {
            for (int i = 0; i < v.Count(); i++)
            {
                DrawChar(v.ElementAt(i), x + i, y, color, backColor);
            }
        }

        public void DrawGrid(int x, int y, int width, int height, int color)
        {

            DrawRepeated(Unicode.AsciiGridVer, x, y, 1, height, color);
            DrawRepeated(Unicode.AsciiGridVer, x + width - 1, y, 1, height, color);
            DrawRepeated(Unicode.AsciiGridHor, x, y, width, 1, color);
            DrawRepeated(Unicode.AsciiGridHor, x, y + height - 1, width, 1, color);

            DrawRepeated((char)218, x, y, 1, 1, color);
            DrawRepeated((char)192, x,              y+height-1, 1, 1, color);
            DrawRepeated((char)217, x+width-1,      y+ height - 1, 1, 1, color);
            DrawRepeated((char)191, x + width - 1,  y, 1, 1, color);
        }

        public void DrawRepeated(char c, int x, int y, int width, int height, int color, int backColor = NOCHANGECOLOR)
        {
            for (int i = x; i < x + width; i++)
            {
                for (int j = y; j < y + height; j++)
                {
                    DrawChar(c, i, j, color);

                    SetBackColor(backColor, i, j);
                }
            }
        }

        public void DrawRepeatedIfVisible(char c, int x, int y, int width, int height, int color, int backColor = NOCHANGECOLOR)
        {
            for (int i = x; i < x + width; i++)
            {
                for (int j = y; j < y + height; j++)
                {
                    if (chars[i, j] != TextBoard.INVISIBLECHAR || TextColor[i,j] != INVISIBLECOLOR)
                        DrawChar(c, i, j, color);
                    if(BackColor[i,j] != TextBoard.INVISIBLECOLOR)
                        SetBackColor(backColor, i, j);
                }
            }
        }

        public void SetColor(int color, int x, int y)
        {
            if (color != NOCHANGECOLOR)
                TextColor[x, y] = color;
        }

        public void SetBackColor(int color, int x, int y)
        {
            if (color != NOCHANGECOLOR)
            {
                BackColor[x, y] = color;
            }
        }

        public void Draw(string v, int x2, int y2, object input)
        {
            throw new NotImplementedException();
        }

        public void DrawGrid(int v1, int v2, int v3, int v4, object board)
        {
            throw new NotImplementedException();
        }

        public struct DrawCursorResult
        {
            public int StartIndex;
            public int EndIndex;
            public Vector2D StartPosition;
            public Vector2D EndPosition;

            public DrawCursorResult(int startIndex, int endIndex, Vector2D startPosition, Vector2D endPosition)
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
                StartPosition = startPosition;
                EndPosition = endPosition;
            }
        }
    }
}
