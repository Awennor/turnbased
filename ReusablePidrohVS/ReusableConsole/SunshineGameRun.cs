﻿using OpenTK.Graphics;
using SunshineConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Input;
using System.Drawing;
using Pidroh.TextRendering;

namespace Pidroh.ReusableSunshine
{
    public class SunshineGameRun
    {
        private static DateTime last;
        ITextGame gr;

        public SunshineGameRun(ITextGame game)
        {
            this.gr = game;
            SunshineMain();
        }

        private void SunshineMain()
        {

            TextBoard TextBoard;
            //SetupGame(out gr, out TextBoard);

            //Dictionary<Key[], InputKey> keyBinds = new Dictionary<Key[], InputKey>() {
            //    {new Key[]{ Key.W}, InputKey.UP },
            //    {new Key[]{ Key.S}, InputKey.DOWN },
            //    {new Key[]{ Key.A}, InputKey.LEFT },
            //    {new Key[]{ Key.D}, InputKey.RIGHT },
            //    {new Key[]{ Key.G}, InputKey.NORMALSHOT },
            //    {new Key[]{ Key.F}, InputKey.FIRE },
            //    {new Key[]{ Key.T}, InputKey.THUNDER },
            //    {new Key[]{ Key.I}, InputKey.ICE },
            //    {new Key[]{ Key.R}, InputKey.REDO },
            //    {new Key[]{ Key.Space}, InputKey.DONE },
            //};

            var p = gr.GetPalette();
            var htmlCols = p.HtmlColors;
            Color4[] colors = new Color4[htmlCols.Length];
            
            for (int i = 0; i < colors.Length; i++)
            {
                
                colors[i] = new Color4(ColorTranslator.FromHtml(htmlCols[i]));

            }


            

            ConsoleWindow console = new ConsoleWindow(40, 120, "Sunshine Console Hello World");
            
            console.MouseDown += (b, m) =>
            {
                var pos = m.Position;
                Console.WriteLine(pos.X);
                Console.WriteLine(pos.Y);
                Console.WriteLine(pos.X * console.Cols / console.Width);
                Console.WriteLine(pos.Y * console.Rows / console.Height);

                //Console.WriteLine(pos.Y/40);
            };


            // Write to the window at row 6, column 14:
            console.Write(6, 14, "Hello World!", Color4.Lime);
            
            console.KeyDown += (o, e) =>
            {
                
                Console.WriteLine("KEY PRESS " + e.Key);
                Console.WriteLine("KEY PRESS " + e.ScanCode);
                gr.ScreenHolder.Key.InputUnicode = (int) e.ScanCode;
            };
            while (console.WindowUpdate())
            {
                float deltaSec = UpdateGame();
                gr.ScreenHolder.Key.InputUnicode = -1;
                TextBoard = gr.ScreenHolder.Screen.GetBoard();


                //draw board
                for (int i = 0; i < TextBoard.Width; i++)
                {
                    for (int j = 0; j < TextBoard.Height; j++)
                    {
                        console.Write(j, i, TextBoard.CharAt(i, j), colors[TextBoard.TextColor[i, j]], colors[TextBoard.BackColor[i, j]]);
                        //Console.SetCursorPosition(i, j);
                        //Console.Write(TextBoard.CharAt(i, j));
                    }
                }

                /* WindowUpdate() does a few very important things:
                ** It renders the console to the screen;
                ** It checks for input events from the OS, such as keypresses, so that they
                **   can reach the program;
                ** It returns false if the console has been closed, so that the program
                **   can be properly ended. */
            }
        }


        private float UpdateGame()
        {
            //gr.Input = ConsoleSuper.GetChar_NonBlocking();
            int milliseconds = (DateTime.Now - last).Milliseconds;
            float deltaSec = milliseconds / 1000f;
            //deltaSec = 0.001f;
            last = DateTime.Now;
            gr.Update(deltaSec);
            return deltaSec;
        }




    }
}
